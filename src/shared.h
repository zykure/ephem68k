/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	shared.h
 * Desc:	Some functions used both in ephem68k and aphem68k.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef _SHARED_H
#define _SHARED_H

/* Flags for the 'flags' item (16-bit-field).
 * The 'flags' field is used to set various options of
 * the program, e.g. which objects to display. */
#define F_MERCURY		1			// bit  0
#define F_VENUS			2			// bit  1
#define F_MARS			4			// bit  2
#define F_JUPITER		8			// bit  3
#define F_SATURN		16		// bit  4
#define F_URANUS		32		// bit  5
#define F_NEPTUNE		64		// bit  6
#define F_SUN				128		// bit  7
#define F_MOON			256		// bit  8
#define F_OBJ1			512		// bit  9
#define F_OBJ2			1024	// bit 10
#define F_OBJ3			2048	// bit 11
#define F_OBJ4			4096	// bit 12
#define F_OBJECTS		8191	// all bits 0-12 for 'object masking'
#define F_TWILIGHT	8192	// bit 13 - Compute twilight
#define F_ADPHZN		16384	// bit 14 - Use adaptive horizon
													/* This takes temperature and pressure into account.
													 * Otherwise, nominal (standard) horizon will be used. */
#define F_CIVILTWI	32768	// bit 15 - Use civil twilight, not astronomical
													/* Sun's offset below horizon is 6 degrees for civil,
													 * and 18 degrees for astronmical twilight.
													 * While astronomical twilight some stars can't be seen,
													 * while civil twilight you can read a nespaper. */

/* Key codes for user interface */
#define K_QUIT			KEY_ESC
#define K_MAIN			KEY_F1
#define K_DATA1			KEY_F2
#define K_DATA2			KEY_F3
#define K_RISET			KEY_F4
#define K_SKYDOME		KEY_F5
#define K_HORIZON		KEY_F6
#define K_INFO			KEY_F7
#define K_HELP			KEY_F8
#define K_PREVITEM		KEY_UP
#define K_NEXTITEM		KEY_DOWN
#define K_ROTL			KEY_LEFT
#define K_ROTR			KEY_RIGHT
#define K_TIMEDOWN		'-'
#define K_TIMEUP		'+'
#define K_UPDATE		' '
#define K_TOGGLE		KEY_MODE
#define K_SETTINGS		KEY_APPS
#define K_EDITITEM		KEY_CLEAR


#endif
