/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	eq_ecl.c
 * Desc:	Routines to convert between equatorial (ha/dec) and ecliptical
 *			(lon/lat) coordinates.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 * This file was taken from original ephem source code
 * Copyright (c) 1990,1991,1992 by Elwood Charles Downey
 * (Code cleanup by Jonathan Woithe, March 2000)
 *
 * It has been modified to run properly on TI calculators.
 */


#ifdef __TIGCC_ENV__
#include <timath.h>
#else
#include <math.h>
#endif
#include "astro.h"
#include "circum.h"
#include "ephem.h"

#define	EQtoECL	1
#define	ECLtoEQ	(-1)


#ifndef TI
/* given the modified Julian date, mjd, and an equitorial ra and dec, each in
 * radians, find the corresponding geocentric ecliptic latitude, *lat, and
 * longititude, *lng, also each in radians.
 * correction for the effect on the angle of the obliquity due to nutation is
 * included.
 */
void eq_ecl (Now *np, double ra, double dec, double *lat, double *lng)
{
	ecleq_aux (EQtoECL, np, ra, dec, lng, lat);
}

/* given the modified Julian date, mjd, and a geocentric ecliptic latitude,
 * *lat, and longititude, *lng, each in radians, find the corresponding
 * equitorial ra and dec, also each in radians.
 * correction for the effect on the angle of the obliquity due to nutation is
 * included.
 */
void ecl_eq (Now *np, double lat, double lng, double *ra, double *dec)
{
	ecleq_aux (ECLtoEQ, np, lng, lat, ra, dec);
}
#endif

void ecleq_aux (short sw, Now *np, double x, double y, double *p, double *q)
{
	static double lasteps = -1000.;
	static double seps = -100., ceps = -100.; /* sin and cos of mean obliquity */
  	double sx,cx,sy,cy, ty;
	double eps = np->n_const.c_eps + np->n_const.c_deps;

	if (eps != lasteps) {		/* Caching produces errors on TI - why??? */
    lasteps = eps;
#ifdef _TI_OPTIMIZED
    sincos(eps, 0, &seps, &ceps);
#else
   	seps = sin(eps);
    ceps = cos(eps);
#endif
	}

#ifdef _TI_OPTIMIZED
  sincos(x, 0, &sx, &cx);
  sincos(y, 0, &sy, &cy);
#else
 	sx = sin(x);
  cx = cos(x);
 	sy = sin(y);
  cy = cos(y);
#endif

  if (fabs(cy) < 1.e-20) cy = 1.e-20;		/* insure > 0 */
  ty = sy/cy;
  *q = asin((sy*ceps)-(cy*seps*sx*sw));
  *p = atan(((sx*ceps)+(ty*seps*sw))/cx);
  if (cx < 0.) *p += PI;		/* account for atan quad ambiguity */
	range(*p, TWOPI);
}