/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	obliq.c
 * Desc:	Compute current obliquity of the ecliptic.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 * This file was taken from original ephem source code
 * Copyright (c) 1990,1991,1992 by Elwood Charles Downey
 * (Code cleanup by Jonathan Woithe, March 2000)
 *
 * It has been modified to run properly on TI calculators.
 */


#include "astro.h"
#include "ephem.h"


/* given the modified Julian date, mjd, find the obliquity of the
 * ecliptic, *eps, in radians.
 */
void obliquity (double mjd, double *eps)
{
	static double lastmjd = -10000., lasteps = -100.;

	if (mjd != lastmjd) {
	    double t;
	    t = mjd/36525.;
	    lasteps = degrad(2.345229444E1
			- ((((-1.81E-3*t)+5.9E-3)*t+4.6845E1)*t)/3600.);
	    lastmjd = mjd;
	}

	*eps = lasteps;
}
