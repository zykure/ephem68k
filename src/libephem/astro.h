/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	astro.h
 * Desc:	Some general definitions...
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef _ASTRO_H
#define _ASTRO_H

/* Make sure we can use PI */
#ifndef PI
#define	PI			3.141592653589793
#endif
#define	TWOPI		(2*PI)
#define MJD			2415020L		/* convert between MJD and JD */
#define REARTH		6.379e6			/* mean earth radius (m) */
#define AS			60.0			/* target accuracy for calculations */
#define SKYACC		3600.0			/* target accuracy for sky watch */

/* Conversions among hours (of ra), degrees and radians. */
#define	degrad(x)	((x)*PI/180.)
#define	raddeg(x)	((x)*180./PI)
#define	hrdeg(x)	((x)*15.)
#define	deghr(x)	((x)/15.)
#define	hrrad(x)	degrad(hrdeg(x))
#define	radhr(x)	deghr(raddeg(x))


#define	SIDRATE		.9972695677		/* solar day -> sidereal day */
#define PARSEC		3.2616339		/* lightyears -> parsec */
#define KPARSEC		(1000.0*PARSEC)
#define MPARSEC		(1000.0*KPARSEC)

#define	EPS			(5e-6)			/* math rounding fudge - always the way, eh? */

/* Manifest names for planets.
 * N.B. must cooincide with usage in pelement.c and plans.c.
 */
#define	MERCURY		0
#define	VENUS		1
#define	MARS		2
#define	JUPITER		3
#define	SATURN		4
#define	URANUS		5
#define	NEPTUNE		6
#define	SUN			7
#define	MOON		8
#define	OBJ1		(MOON+1)
#define	OBJ2		(MOON+2)
#define	OBJ3		(MOON+3)
#define	OBJ4		(MOON+4)
#define	NOBJ		(MOON+5)

#endif
