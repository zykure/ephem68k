/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	circum.h
 * Desc:	Header file with all information  needed to compute the
 *			circumstances of a body.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef _CIRCUM_H
#define _CIRCUM_H

#define	SPD			(24.0*3600.0)	/* seconds per day */

#define	EOD			(-9786)			/* special epoch flag: use epoch of date */
#define	RTC			(-1324)			/* special tminc flag: use rt clock */
#define	NOMJD		(-58631.)		/* an unlikely mjd for initing static mjd's */
#define	NOHELIO		(-2314)			/* special s_hlong flag: means s_hlong and
				 					 * s_hlat are undefined
				 					 */

#define	STDHZN		0	/* rise/set times based on nominal conditions */
#define	ADPHZN		1	/* rise/set times based on exact current conditions */
#define	TWILIGHT	2	/* rise/set times for sun 18 degs below hor */
#define	TWILIGHTCIV	3	/* rise/set times for sun 6 degs below hor */


/* Info about our local observing circumstances */
typedef struct {
	double n_mjd;		/* modified Julian date, ie, days since
			 			 * Jan 0.5 1900 (== 12 noon, Dec 30, 1899), utc.
			 			 * enough precision to get well better than 1 second.
			 			 * N.B. if not first member, must move NOMJD inits.
			 			 */
	double n_lat;		/* latitude, >0 north, rads */
	double n_lng;		/* longitude, >0 east, rads */
	double n_tz;		/* time zone, hrs behind UTC */
	double n_temp;		/* atmospheric temp, degrees C */
	double n_pressure; 	/* atmospheric pressure, mBar */
	double n_height;	/* height above sea level, earth radii */
	double n_epoch;		/* desired precession display epoch as an mjd, or EOD */
	/* Pseudo-constants - will be the same for one mjd, so this should boost
	 * performance because these values will be read from the Now structure
	 * instead of computing them again each time they are needed.
	 */
	struct {
		double c_lst;	/* local sidereal time */
		double c_eps;	/* obliquity of the ecliptic */
		double c_lsn;	/* ecliptic longitude of the sun */
		double c_rsn;	/* sun-earth distance */
		double c_deps;	/* nutation in obliquity */
		double c_dpsi;	/* nutation in longitude */
	} n_const;			/* pseudo-constant parameters for this mjd */
} Now;

/* Info about where and how we see something in the sky */
typedef struct {
	double s_ra;		/* ra, rads (precessed to n_epoch) */
	double s_dec;		/* dec, rads (precessed to n_epoch) */
	double s_az;		/* azimuth, >0 e of n, rads */
	double s_alt;		/* altitude above topocentric horizon, rads */
	double s_sdist;		/* dist from object to sun, au */
	double s_edist;		/* dist from object to earth, au */
	double s_elong;		/* angular sep between object and sun, >0 if east */
	double s_hlong;		/* heliocentric longitude, rads */
	double s_hlat;		/* heliocentric latitude, rads */
	double s_size;		/* angular size, arc secs */
	double s_phase;		/* phase, % */
	double s_mag;		/* visual magnitude */
} Sky;

typedef struct {
	double w_az;
	double w_alt;
	double w_ra;
	double w_dec;
	short w_x;
	short w_y;
} Watch;

typedef struct {
	double start;
	double stop;
	double step;
} AutoData;

/* Static initializers */
#define NOW_NULL	{NOMJD,0.,0.,0.,20.,1013.,0.,EOD,{0.,0.,0.,0.,0.,0.}}
#define SKY_NULL	{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.}
#define WATCH_NULL	{0.,0.,0.,0.,0,0}


/* Flags for riset_cir() status */
#define	RS_NORISE		0x001	/* object does not rise as such today */
#define	RS_2RISES		0x002	/* object rises more than once today */
#define	RS_NOSET		0x004	/* object does not set as such today */
#define	RS_2SETS		0x008	/* object sets more than once today */
#define	RS_CIRCUMPOLAR	0x010	/* object stays up all day today */
#define	RS_2TRANS		0x020	/* transits twice in one day */
#define	RS_NEVERUP		0x040	/* object never rises today */
#define	RS_NOTRANS		0x080	/* doesn't transit today */
#define	RS_ERROR		0x100	/* can't figure out times... */


/* Structures to describe objects of various types. */

#define	MAXNM		20		/* longest allowed object name */

/* Magnitude of an object */
typedef struct {
    double m_m1, m_m2;		/* either g/k or H/G, depending on... */
	double m_mag;			/* resulting magnitude */
    short m_whichm;		/* one of MAG_gk or MAG_HG */
} Mag;
/* Fixed object (stars, deep-sky etc.) */
typedef struct {
    double f_ra;			/* ra, rads, at given epoch */
    double f_dec;			/* dec, rads, at given epoch */
    double f_mag;			/* visual magnitude */
    double f_siz;			/* angular size, in arc seconds */
    double f_epoch;		/* the given epoch, as an mjd */
    double f_dist;			/* earth distance, in lightyears */
    char   f_class[16];	/* Additional info (e.g. spectral class, ...) */
    char   f_name[MAXNM+1];/* name */
} ObjF;
/* Object in heliocentric elliptical orbit (e.g. planets) */
typedef struct {
    double e_inc;			/* inclination, degrees */
    double e_Om;			/* longitude of ascending node, degrees */
    double e_om;			/* argument of perihelion, degress */
    double e_a;			/* mean distance, aka, semi-maj axis, in AU */
    double e_n;			/* daily motion, degrees/day */
    double e_e;			/* eccentricity */
    double e_M;			/* mean anomaly, ie, degrees from perihelion at... */
    double e_cepoch;		/* epoch date (M reference), as an mjd */
    double e_epoch;		/* equinox year (inc/Om/om reference), as an mjd */
    Mag    e_mag;			/* magnitude */
    double e_siz;			/* angular size, in arc seconds at 1 AU */
    double e_radius;		/* radius, in km */
    double e_daylen;		/* daylength, in hours */
    char   e_name[MAXNM+1];/* name */
} ObjE;
/* Object in heliocentric hyperbolic trajectory (e.g. comets) */
typedef struct {
    double h_ep;			/* epoch of perihelion, as an mjd */
    double h_inc;			/* inclination, degs */
    double h_Om;			/* longitude of ascending node, degs */
    double h_om;			/* argument of perihelion, degs. */
    double h_e;			/* eccentricity */
    double h_qp;			/* perihelion distance, AU */
    double h_epoch;		/* equinox year (inc/Om/om reference), as an mjd */
    double h_g, h_k;		/* magnitude model coefficients */
    double h_siz;			/* angular size, in arc seconds at 1 AU */
    double h_radius;		/* radius, in km */
    double h_daylen;		/* daylength, in hours */
    char   h_name[MAXNM+1];/* name */
} ObjH;
/* Object in heliocentric parabolic trajectory orbit */
typedef struct {
    double p_ep;			/* epoch of perihelion, as an mjd */
    double p_inc;			/* inclination, degs */
    double p_om;			/* argument of perihelion, degs. */
    double p_qp;			/* perihelion distance, AU */
    double p_Om;			/* longitude of ascending node, degs */
    double p_epoch;		/* reference epoch, as an mjd */
    double p_g, p_k;		/* magnitude model coefficients */
    double p_siz;			/* angular size, in arc seconds at 1 AU */
    double p_radius;		/* radius, in km */
    double p_daylen;		/* daylength, in hours */
    char   p_name[MAXNM+1];/* name */
} ObjP;

/* Combine all info in one structure */
typedef struct {
    short  o_type;			/* current object type; see flags, below */
    char  o_class;			/* current object cass */
    short  o_on;			/* !=0 while current object is active */
    /* We use an union instead of a struct here to save memory */
    union {
		ObjF o_f;			/* the fixed object */
    	ObjE o_e;			/* the elliptical orbit object */
    	ObjH o_h;			/* the hyperbolic orbit object */
    	ObjP o_p;			/* the parabolic orbit object */
	} o_data;
} Obj;

/* Flags: o_type */
#define	OBJ_NONE	0
#define	FIXED		1
#define	ELLIPTICAL	2
#define	HYPERBOLIC	3
#define	PARABOLIC	4

/* Flags: m_whichm */
#define	MAG_HG		0	/* using 0 makes HG the initial default */
#define	MAG_gk		1

#endif
