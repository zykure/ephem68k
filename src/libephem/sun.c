/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	sun.c
 * Desc:	Compute current values of the sun.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 * This file was taken from original ephem source code
 * Copyright (c) 1990,1991,1992 by Elwood Charles Downey
 * (Code cleanup by Jonathan Woithe, March 2000)
 *
 * It has been modified to run properly on TI calculators.
 */


#ifdef __TIGCC_ENV__
#include <timath.h>
#else
#include <math.h>
#endif
#include "astro.h"
#include "ephem.h"


/* given the modified JD, mjd, return the true geocentric ecliptic longitude
 *   of the sun for the mean equinox of the date, *lsn, in radians, and the
 *   sun-earth distance, *rsn, in AU. (the true ecliptic latitude is never more
 *   than 1.2 arc seconds and so may be taken to be a constant 0.)
 * if the APPARENT ecliptic longitude is required, correct the longitude for
 *   nutation to the true equinox of date and for aberration (light travel time,
 *   approximately  -9.27e7/186000/(3600*24*365)*2*pi = -9.93e-5 radians).
 */
void sunpos (double mjd, double *lsn, double *rsn)
{
	static double last_mjd = -10000., last_lsn = -100., last_rsn = -100.;
	double t, t2;
	double ls, ms;    /* mean longitude and mean anomoay */
	double s, nu, ea; /* eccentricity, true anomaly, eccentric anomaly */
	double a, b, sa1,ca1, sb1,cb1, sc1,cc1, sd1,cd1, se1, sh1, dl, dr;

	if (mjd != last_mjd) {
		last_mjd = mjd;

		t = mjd/36525.;
		t2 = t*t;

		a = 100.0021359*t;
		b = 360.*(a-(long)a);
		ls = 279.69668+.0003025*t2+b;

		a = 99.99736042000039*t;
		b = 360.*(a-(long)a);
		ms = 358.47583-(.00015+.0000033*t)*t2+b;

		s = .016751-.0000418*t-1.26e-07*t2;
		anomaly (degrad(ms), s, &nu, &ea);

		a = 62.55209472000015*t;
		b = 360*(a-(long)a);
#ifdef _TI_OPTIMIZED
		sincos(153.23+b, 1, &sa1, &ca1);
#else
		a = degrad(153.23+b);
		sa1 = sin(a);
		ca1 = cos(a);
#endif

		a = 125.1041894*t;
		b = 360*(a-(long)a);
#ifdef _TI_OPTIMIZED
		sincos(216.57+b, 1, &sb1, &cb1);
#else
		a = degrad(216.57+b);
		sb1 = sin(a);
		cb1 = cos(a);
#endif

		a = 91.56766028*t;
		b = 360*(a-(long)a);
#ifdef _TI_OPTIMIZED
		sincos(312.69+b, 1, &sc1, &cc1);
#else
		a = degrad(312.69+b);
		sc1 = sin(a);
		cc1 = cos(a);
#endif

		a = 1236.853095*t;
		b = 360*(a-(long)a);
#ifdef _TI_OPTIMIZED
		sincos(350.74-.00144*t2+b, 1, &sd1, &cd1);
#else
		a = degrad(350.74-.00144*t2+b);
		sd1 = sin(a);
		cd1 = cos(a);
#endif

#ifdef _TI_OPTIMIZED
		sincos (231.19+20.2*t, 1, &se1, &a);
#else
		a = degrad(231.19+20.2*t);
		se1 = sin(a);
#endif

		a = 183.1353208*t;
		b = 360*(a-(long)a);
#ifdef _TI_OPTIMIZED
		sincos (353.4+b, 1, &sh1, &a);
#else
		a = degrad(353.4+b);
		sh1 = sin(a);
#endif

		dl = .00134*ca1+.00154*cb1+.002*cc1+.00179*sd1+.00178*se1;
		dr = 5.43e-06*sa1+1.575e-05*sb1+1.627e-05*sc1+3.076e-05*cd1+9.27e-06*sh1;
		last_lsn = nu+degrad(ls-ms+dl);
		last_rsn = 1.0000002*(1-s*cos(ea))+dr;
		range(last_lsn, TWOPI);
	}

	*lsn = last_lsn;
	*rsn = last_rsn;
}
