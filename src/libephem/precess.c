/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	precess.c
 * Desc:	Compute precession correction.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 * This file was taken from original ephem source code
 * Copyright (c) 1990,1991,1992 by Elwood Charles Downey
 * (Code cleanup by Jonathan Woithe, March 2000)
 *
 * It has been modified to run properly on TI calculators.
 */


#ifdef __TIGCC_ENV__
#include <timath.h>
#else
#include <math.h>
#endif
#include "astro.h"
#include "ephem.h"

#define	DCOS(x)		cos(degrad(x))
#define	DSIN(x)		sin(degrad(x))
#define	DASIN(x)	raddeg(asin(x))
#define	DATAN2(y,x)	raddeg(atan2((y),(x)))


/* corrects ra and dec, both in radians, for precession from epoch 1 to epoch 2.
 * the epochs are given by their modified JDs, mjd1 and mjd2, respectively.
 * N.B. ra and dec are modifed IN PLACE.
 */

/*
 * Copyright (c) 1990 by Craig Counterman. All rights reserved.
 *
 * This software may be redistributed freely, not sold.
 * This copyright notice and disclaimer of warranty must remain
 *    unchanged.
 *
 * No representation is made about the suitability of this
 * software for any purpose.  It is provided "as is" without express or
 * implied warranty, to the extent permitted by applicable law.
 *
 * Rigorous precession. From Astronomical Ephemeris 1989, p. B18
 */

void precess (double mjd1, double mjd2, double *ra, double *dec)
{
	double zeta_A, z_A, theta_A;
	double T, T2, T3;
	double A, B, C;
	double alpha, delta;
	double alpha_in, delta_in;
	double from_equinox, to_equinox;
	double alpha2000, delta2000;
	double sa,ca, sd,cd, st,ct;

	mjd_year (mjd1, &from_equinox);
	mjd_year (mjd2, &to_equinox);
	alpha_in = raddeg(*ra);
	delta_in = raddeg(*dec);

	/* From from_equinox to 2000.0 */
	if (from_equinox != 2000.) {
    T = (from_equinox - 2000.)/100.;
    T2 = T*T;
    T3 = T2*T;
	  zeta_A  = 0.6406161* T + 0.0000839* T2 + 0.0000050* T3;
    z_A     = 0.6406161* T + 0.0003041* T2 + 0.0000051* T3;
    theta_A = 0.5567530* T - 0.0001185* T2 + 0.0000116* T3;

#ifdef _TI_OPTIMIZED
		sincos(alpha_in - z_A, 1, &sa, &ca);
		sincos(delta_in, 1, &sd, &cd);
		sincos(theta_A, 1, &st, &ct);
#else
		sa = DSIN(alpha_in - z_A);
		ca = DCOS(alpha_in - z_A);
		sd = DSIN(delta_in);
		cd = DCOS(delta_in);
		st = DSIN(theta_A);
		ct = DCOS(theta_A);
#endif

    A = sa * cd;
    B = ca * ct * cd + st * sd;
    C = -ca * st * cd + ct * sd;

    alpha2000 = DATAN2(A,B) - zeta_A;
    delta2000 = DASIN(C);
		alpha2000 -= 360.*floor(alpha2000/360.);
	} else {
	  /* should get the same answer, but this could improve accruacy */
	  alpha2000 = alpha_in;
	  delta2000 = delta_in;
	};


	/* From 2000.0 to to_equinox */
	if (to_equinox != 2000.) {
	  T = (to_equinox - 2000.)/100.;
	  T2 = T*T;
	  T3 = T2*T;
	  zeta_A  = 0.6406161* T + 0.0000839* T2 + 0.0000050* T3;
	  z_A     = 0.6406161* T + 0.0003041* T2 + 0.0000051* T3;
	  theta_A = 0.5567530* T - 0.0001185* T2 + 0.0000116* T3;

#ifdef _TI_OPTIMIZED
		sincos(alpha_in - z_A, 1, &sa, &ca);
		sincos(delta_in, 1, &sd, &cd);
		sincos(theta_A, 1, &st, &ct);
#else
		sa = DSIN(alpha2000 + zeta_A);
		ca = DCOS(alpha2000 + zeta_A);
		sd = DSIN(delta2000);
		cd = DCOS(delta2000);
		st = DSIN(theta_A);
		ct = DCOS(theta_A);
#endif

    A = sa * cd;
    B = ca * ct * cd - st * sd;
    C = ca * st * cd + ct * sd;

    alpha = DATAN2(A,B) + z_A;
    delta = DASIN(C);
		alpha -= 360.*floor(alpha/360.);
	} else {
    /* should get the same answer, but this could improve accruacy */
    alpha = alpha2000;
    delta = delta2000;
	};

	*ra = degrad(alpha);
	*dec = degrad(delta);
}
