/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	aa_hadec.c
 * Desc:	Routines to transform between equatorial (ha/dec) and horizontal
 *			(az/alt) coordinates.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 * This file was taken from original ephem source code
 * Copyright (c) 1990,1991,1992 by Elwood Charles Downey
 * (Code cleanup by Jonathan Woithe, March 2000)
 *
 * It has been modified to run properly on TI calculators.
 */


#ifdef __TIGCC_ENV__
#include <timath.h>
#else
#include <math.h>
#endif
#include "ephem.h"
#include "astro.h"

#include <tigcclib.h>


/* the actual formula is the same for both transformation directions so
 * do it here once for each way.
 * N.B. all arguments are in radians.
 */
void aaha_aux (double lat, double x, double y, double *p, double *q)
{
	static double lastlat = -1000.;
	static double sinlastlat = 0., coslastlat = 0.;
	double sy, cy;
	double sx, cx;

	/* latitude doesn't change much, so try to reuse the sin and cos evals.
	 */
	if (lat != lastlat) {
#ifdef _TI_OPTIMIZED
		sincos(lat, 0, &sinlastlat, &coslastlat);
#else
	  sinlastlat = sin(lat);
	  coslastlat = cos(lat);
#endif
	  lastlat = lat;
	}

#ifdef _TI_OPTIMIZED
	sincos(y, 0, &sy, &cy);
	sincos(x, 0, &sx, &cx);
#else
	sy = sin(y);
	cy = cos(y);
	sx = sin(x);
	cx = cos(x);
#endif

	*q = asin((sy*sinlastlat) + (cy*coslastlat*cx));
	*p = atan2(-cy*sx, -cy*cx*sinlastlat + sy*coslastlat);
	range(*p, TWOPI);
}

#ifndef TI
/* given latitude (n+, radians), lat, altitude (up+, radians), alt, and
 * azimuth (angle round to the east from north+, radians),
 * return hour angle (radians), ha, and declination (radians), dec.
 */
void aa_hadec (double lat, double alt, double az, double *ha, double *dec)
{
	aaha_aux (lat, az, alt, ha, dec);
}

/* given latitude (n+, radians), lat, hour angle (radians), ha, and declination
 * (radians), dec,
 * return altitude (up+, radians), alt, and
 * azimuth (angle round to the east from north+, radians),
 */
void hadec_aa (double lat, double ha, double dec, double *alt, double *az)
{
	aaha_aux (lat, ha, dec, az, alt);
}
#endif
