/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	reduce.c
 * Desc:	Routines to convert orbital elements from one epoch to another.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 * This file was taken from original ephem source code
 * Copyright (c) 1990,1991,1992 by Elwood Charles Downey
 * (Code cleanup by Jonathan Woithe, March 2000)
 *
 * It has been modified to run properly on TI calculators.
 */


#ifdef __TIGCC_ENV__
#include <timath.h>
#else
#include <math.h>
#endif
#include "astro.h"
#include "ephem.h"


/* convert those orbital elements that change from epoch mjd0 to epoch mjd.
 * 		mjd0:	initial epoch
 * 		mjd0;	initial epoch
 * 		mjd;	desired epoch
 * 		inc0;	initial inclination, rads
 * 		ap0;	initial argument of perihelion, as an mjd
 * 		om0;	initial long of ascending node, rads
 *  	*inc;	desired inclination, rads
 * 		*ap;	desired epoch of perihelion, as an mjd
 * 		*om;	desired long of ascending node, rads
 */
void reduce_elements (double mjd0, double mjd, double inc0, double ap0, double om0, double *inc, \
		double *ap, double *om)
{
	double t0, t1;
	double tt, tt2, t02, tt3;
	double eta, th, th0;
	double a, b;
	double dap;
	double cinc, sinc;
	double ot, sot, cot, ot1;
	double seta, ceta;

	t0 = mjd0/365250.;
	t1 = mjd/365250.;

	tt = t1-t0;
	tt2 = tt*tt;
  t02 = t0*t0;
	tt3 = tt*tt2;

  eta = (471.07-6.75*t0+.57*t02)*tt+(.57*t0-3.37)*tt2+.05*tt3;
  th0 = 32869.*t0+56.*t02-(8694.+55.*t0)*tt+3*tt2;
  eta = degrad(eta/3600.);
  th0 = degrad((th0/3600.)+173.950833);
  th = (50256.41+222.29*t0+.26*t02)*tt+(111.15+.26*t0)*tt2+.1*tt3;
  th = th0+degrad(th/3600.);

	ot = om0-th0;
#ifdef _TI_OPTIMIZED
	sincos(inc0, 0, &sinc, &cinc);
	sincos(ot, 0, &sot, &cot);
	sincos(eta, 0, &seta, &ceta);
#else
  sinc = sin(inc0);
	cinc = cos(inc0);
	sot = sin(ot);
  cot = cos(ot);
	seta = sin(eta);
  ceta = cos(eta);
#endif
	a = sinc*sot;
  b = ceta*sinc*cot-seta*cinc;
	ot1 = atan(a/b);
  if (b<0) ot1 += PI;
  b = sinc*ceta-cinc*seta*cot;
  a = -1*seta*sot;
	dap = atan(a/b);
  if (b<0) dap += PI;

  *ap = ap0+dap;
	range (*ap, TWOPI);
  *om = ot1+th;
	range (*om, TWOPI);

  if (inc0 < .175)
	  *inc = asin(a/sin(dap));
	else
	  *inc = 1.570796327-asin((cinc*ceta)+(sinc*seta*cot));
}
