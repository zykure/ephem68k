/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	anomaly.c
 * Desc:	Compute true and eccentric anomaly for an object.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 * This file was taken from original ephem source code
 * Copyright (c) 1990,1991,1992 by Elwood Charles Downey
 * (Code cleanup by Jonathan Woithe, March 2000)
 *
 * It has been modified to run properly on TI calculators.
 */


#ifdef __TIGCC_ENV__
#include <timath.h>
#else
#include <math.h>
#endif
#include "astro.h"
#include "ephem.h"

/* given the mean anomaly, ma, and the eccentricity, s, of elliptical motion,
 * find the true anomaly, *nu, and the eccentric anomaly, *ea.
 * all angles in radians.
 */
void anomaly (double ma, double s, double *nu, double *ea)
{
	double m, fea;

	m = ma-TWOPI*(long)(ma/TWOPI);
	if (m > PI) m -= TWOPI;
	if (m < -PI) m += TWOPI;
	fea = m;

	if (s < 1.) {
	  /* elliptical */
	  double dla;
	  while (1) {
			dla = fea-(s*sin(fea))-m;
			if (fabs(dla)<1.e-6)
		  	break;
			dla /= 1-(s*cos(fea));
			fea -= dla;
	  }
	  *nu = 2.*atan(sqrt((1.+s)/(1.-s))*tan(fea/2.));
	} else {
	  /* hyperbolic */
	  double corr = 1.;
	  while (fabs(corr) > 0.000001) {
	    corr = (m - s * sinh(fea) + fea) / (s*cosh(fea) - 1.);
	    fea += corr;
	  }
	  *nu = 2*atan(sqrt((s+1.)/(s-1.))*tanh(fea/2.));
	}
	*ea = fea;
}
