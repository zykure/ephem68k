/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	riset_c.c
 * Desc:	Solve iteratively for rise/set times of an object.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 * This file was taken from original ephem source code
 * Copyright (c) 1990,1991,1992 by Elwood Charles Downey
 * (Code cleanup by Jonathan Woithe, March 2000)
 *
 * It has been modified to run properly on TI calculators.
 */


#ifdef __TIGCC_ENV__
#include <timath.h>
#else
#include <math.h>
#endif
#include "astro.h"
#include "circum.h"
#include "ephem.h"

#define	STDREF		degrad(34./60.)	/* nominal horizon refraction amount */
#define	TWIREF		degrad(18.)		/* astronomical twilight horizon displacement (18 degrees) */
#define	TWIREFCIV	degrad(6.)		/* civil twilight horizon displacement (6 degrees) */
#define	TMACC		(15./3600.)		/* convergence accuracy, hours */


static void iterative_riset (short p, Now *np, Obj *op, short hzn, double *ltr, double *lts, \
	double *ltt, double *azr, double *azs, double *altt, short *status);
static void stationary_riset (short p, double mjd0, Now *np, Obj *op, short hzn, double *lstr, \
	double *lsts, double *lstt, double *azr, double *azs, double *altt, short *status);
static void transit (double r, double d, Now *np, double *lstt, double *altt);


/* find where and when a body, p, will rise and set and
*   it's transit circumstances. all times are local, angles rads e of n.
* return 0 if just returned same stuff as previous call, else 1 if new.
* status is set from the RS_* #defines in circum.h.
* also used to find twilight by calling with hzn TWILIGHT.
*/
short riset_cir (short p, Now *np, Obj *op, short force, short hzn, double *ltr, double *lts, \
	double *ltt, double *azr, double *azs, double *altt, short *status)
{
	typedef struct {
		Now l_now;
		double l_ltr, l_lts, l_ltt, l_azr, l_azs, l_altt;
		int l_hzn;
		int l_status;
	}
	Last;
	/* must be in same order as the astro.h #define's */
	static Last last[NOBJ] = {
		{	NOW_NULL,0,0,0,0,0,0,STDHZN,0	},
		{	NOW_NULL,0,0,0,0,0,0,STDHZN,0	},
		{	NOW_NULL,0,0,0,0,0,0,STDHZN,0	},
		{	NOW_NULL,0,0,0,0,0,0,STDHZN,0	},
		{	NOW_NULL,0,0,0,0,0,0,STDHZN,0	},
		{	NOW_NULL,0,0,0,0,0,0,STDHZN,0	},
		{	NOW_NULL,0,0,0,0,0,0,STDHZN,0	},
		{	NOW_NULL,0,0,0,0,0,0,STDHZN,0	},
		{	NOW_NULL,0,0,0,0,0,0,STDHZN,0	},
		{	NOW_NULL,0,0,0,0,0,0,STDHZN,0	},
		{	NOW_NULL,0,0,0,0,0,0,STDHZN,0	},
		{	NOW_NULL,0,0,0,0,0,0,STDHZN,0	},
		{	NOW_NULL,0,0,0,0,0,0,STDHZN,0	}
	};
	Last *lp;
	short new;

	lp = last + p;
	if (!force && same_cir (np, &lp->l_now) && same_lday (np, &lp->l_now)
		    && lp->l_hzn == hzn) {
		*ltr = lp->l_ltr;
		*lts = lp->l_lts;
		*ltt = lp->l_ltt;
		*azr = lp->l_azr;
		*azs = lp->l_azs;
		*altt = lp->l_altt;
		*status = lp->l_status;
		new = 0;
	} else {
		*status = 0;
		iterative_riset (p, np, op, hzn, ltr, lts, ltt, azr, azs, altt, status);
		lp->l_ltr = *ltr;
		lp->l_lts = *lts;
		lp->l_ltt = *ltt;
		lp->l_azr = *azr;
		lp->l_azs = *azs;
		lp->l_altt = *altt;
		lp->l_status = *status;
		lp->l_hzn = hzn;
		lp->l_now = *np;
		new = 1;
	}

	return (new);
}

static void iterative_riset (short p, Now *np, Obj *op, short hzn, double *ltr, double *lts, \
		double *ltt, double *azr, double *azs, double *altt, short *status)
{
#define	MAXPASSES	8			/* Should be fast and accurate enough */
	double lstr, lsts, lstt; 	/* local sidereal times of rising/setting */
	double mjd0;				/* mjd estimates of rise/set event */
	double lnoon;				/* mjd of local noon */
	double x;					/* discarded tmp value */
	Now n;						/* just used to call now_lst() */
	double lst;					/* lst at local noon */
	double diff, lastdiff;		/* iterative improvement to mjd0 */
	short pass;
	short rss;

	/* first approximation is to find rise/set times of a fixed object
	* in its position at local noon.
	*/
	lnoon = mjd_day(np->n_mjd-np->n_tz/24.) + (12.+np->n_tz)/24.; /*mjd of local noon*/
	n.n_mjd = lnoon;
	n.n_lng = np->n_lng;
	now_lst (&n, &lst);	/* lst at local noon */
	mjd0 = lnoon;
	stationary_riset (p,mjd0,np,op,hzn,&lstr,&lsts,&lstt,&x,&x,&x,&rss);

chkrss:
	switch (rss) {
  	case  0:
	  	break;
  	case  1:
	  	*status = RS_NEVERUP;
		return;
  	case -1:
	  	*status = RS_CIRCUMPOLAR;
		goto transit;
  	default:
	  	*status = RS_ERROR;
		return;
	}

	/* find a better approximation to the rising circumstances based on
	* more passes, each using a "fixed" object at the location at
	* previous approximation of the rise time.
	*/
	lastdiff = 1000.;
	for (pass = 1; pass < MAXPASSES; pass++) {
		diff = (lstr - lst)*SIDRATE; /* next guess at rise time wrt noon */
		if (diff > 12.)
			diff -= 24.*SIDRATE;	/* not tomorrow, today */
		else if (diff < -12.)
			diff += 24.*SIDRATE;	/* not yesterday, today */
		mjd0 = lnoon + diff/24.;	/* next guess at mjd of rise */
		stationary_riset (p,mjd0,np,op,hzn,&lstr,&x,&x,azr,&x,&x,&rss);
		if (rss != 0)
		  goto chkrss;
		if (fabs (diff - lastdiff) < TMACC)
			break;
		lastdiff = diff;
	}
	if (pass == MAXPASSES)
		*status |= RS_NORISE;	/* didn't converge - no rise today */
	else {
		*ltr = 12. + diff;
		if (p != MOON &&
			    (*ltr <= 24.*(1.0-SIDRATE) || *ltr >= 24.*SIDRATE))
			*status |= RS_2RISES;
	}

	/* find a better approximation to the setting circumstances based on
	* more passes, each using a "fixed" object at the location at
	* previous approximation of the set time.
	*/
	lastdiff = 1000.;
	for (pass = 1; pass < MAXPASSES; pass++) {
	diff = (lsts - lst)*SIDRATE; /* next guess at set time wrt noon */
		if (diff > 12.)
			diff -= 24.*SIDRATE;	/* not tomorrow, today */
		else if (diff < -12.)
			diff += 24.*SIDRATE;	/* not yesterday, today */
		mjd0 = lnoon + diff/24.;	/* next guess at mjd of set */
		stationary_riset (p,mjd0,np,op,hzn,&x,&lsts,&x,&x,azs,&x,&rss);
		if (rss != 0)
			goto chkrss;
		if (fabs (diff - lastdiff) < TMACC)
			break;
		lastdiff = diff;
	}
	if (pass == MAXPASSES)
		*status |= RS_NOSET;	/* didn't converge - no set today */
	else {
		*lts = 12. + diff;
		if (p != MOON &&
			    (*lts <= 24.*(1.0-SIDRATE) || *lts >= 24.*SIDRATE))
			*status |= RS_2SETS;
	}

transit:
	/* find a better approximation to the transit circumstances based on
	* more passes, each using a "fixed" object at the location at
	* previous approximation of the transit time.
	*/
	lastdiff = 1000.;
	for (pass = 1; pass < MAXPASSES; pass++) {
		diff = (lstt - lst)*SIDRATE; /*next guess at transit time wrt noon*/
		if (diff > 12.)
			diff -= 24.*SIDRATE;	/* not tomorrow, today */
		else if (diff < -12.)
			diff += 24.*SIDRATE;	/* not yesterday, today */
		mjd0 = lnoon + diff/24.;	/* next guess at mjd of transit */
		stationary_riset (p,mjd0,np,op,hzn,&x,&x,&lstt,&x,&x,altt,&rss);
		if (fabs (diff - lastdiff) < TMACC) break;
		lastdiff = diff;
	}
	if (pass == MAXPASSES)
		*status |= RS_NOTRANS;	/* didn't converge - no transit today */
	else {
		*ltt = 12. + diff;
		if (p != MOON &&
			    (*ltt <= 24.*(1.0-SIDRATE) || *ltt >= 24.*SIDRATE))
			*status |= RS_2TRANS;
	}
}

static void stationary_riset (short p, double mjd0, Now *np, Obj *op, short hzn, double *lstr, \
	double *lsts, double *lstt, double *azr, double *azs, double *altt, short *status)
{
	double dis;
	Now n;
	Sky s;

	/* find object p's topocentric ra/dec at mjd0
	* (this must include parallax)
	*/
	n = *np;
	n.n_mjd = mjd0;
	body_cir (p, 0., &n, op, &s);
	if (n.n_epoch != EOD)
		precess (n.n_epoch, mjd0, (double*) &s.s_ra, (double*) &s.s_dec);
	if (s.s_edist > 0.) {
		/* parallax, if we can */
		double ehp, lst, ha;
		if (p == MOON)
			ehp = asin (6378.14/s.s_edist);
		else
			ehp = (2.*6378./146e6)/s.s_edist;
		now_lst (&n, &lst);
		ha = hrrad(lst) - s.s_ra;
		ta_par (ha, s.s_dec, n.n_lat, n.n_height, ehp, &ha, (double*) &s.s_dec);
		s.s_ra = hrrad(lst) - ha;
		range(s.s_ra, TWOPI);
	}

	switch (hzn) {
  	case STDHZN:
	  	/* nominal atmospheric refraction.
  		 * then add nominal moon or sun semi-diameter, as appropriate.
  		 * other objects assumes to be negligibly small.
   		 */
  		dis = STDREF;
	  	if (p == MOON || p == SUN)
		  	dis += degrad (32./60./2.);
  		break;
	case TWILIGHT:
		if (p != SUN) {
			return;
  		} else
		  	dis = TWIREF;
  		break;
	case TWILIGHTCIV:
		if (p != SUN) {
			return;
  		} else
  			dis = TWIREFCIV;
	  	break;
  	case ADPHZN:
	  	/* adaptive includes actual refraction conditions and also
		   * includes object's semi-diameter.
  		 */
  		unrefractex (n.n_pressure, n.n_temp, 0., &dis);
	  	dis = -dis;
		  dis += degrad(s.s_size/3600./2.);
  		break;
	  }

	riset (s.s_ra, s.s_dec, n.n_lat, dis, lstr, lsts, azr, azs, status);
	transit (s.s_ra, s.s_dec, np, lstt, altt);
}


/* find when and how hi object at (r,d) is when it transits. */
static void transit (double r, double d, Now *np, double *lstt, double *altt)
{
	*lstt = radhr(r);
	*altt = PI/2. - np->n_lat + d;
	if (*altt > PI/2.)
		*altt = PI - *altt;
	refractex (np->n_pressure, np->n_temp, *altt, altt);
}
