/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	sex_dec.c
 * Desc:	Routines to convert between sexagesimal and decimal notation.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 * This file was taken from original ephem source code
 * Copyright (c) 1990,1991,1992 by Elwood Charles Downey
 * (Code cleanup by Jonathan Woithe, March 2000)
 *
 * It has been modified to run properly on TI calculators.
 */


#include "ephem.h"


/* given hours (or degrees), hd, minutes, m, and seconds, s,
 * return decimal hours (or degrees), *d.
 * in the case of hours (angles) < 0, only the first non-zero element should
 *   be negative.
 */
void sex_dec (short hd, short m, short s, double *d)
{
	short sign = 1;

	if (hd < 0) {
	    sign = -1;
	    hd = -hd;
	} else if (m < 0) {
	    sign = -1;
	    m = -m;
	} else if (s < 0) {
	    sign = -1;
	    s = -s;
	}

	*d = (((double)s/60. + (double)m)/60. + (double)hd) * sign;
}

/* given decimal hours (or degrees), d.
 * return nearest hours (or degrees), *hd, minutes, *m, and seconds, *s,
 * each always non-negative; *isneg is set to 1 if d is < 0, else to 0.
 */
void dec_sex (double d, short *hd, short *m, short *s, short *isneg)
{
	double min;

	if (d < 0.) {
	    *isneg = 1;
	    d = -d;
	} else
	    *isneg = 0;

	*hd = (int)d;
	min = (d - *hd)*60.;
	*m = (int)min;
	*s = (int)((min - *m)*60. + 0.5);

	if (*s == 60) {
	    if ((*m += 1) == 60) {
		*hd += 1;
		*m = 0;
	    }
	    *s = 0;
	}
	/* no  negative 0's */
	if (*hd == 0 && *m == 0 && *s == 0)
	    *isneg = 0;
}
