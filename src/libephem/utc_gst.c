/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	utc_gst.c
 * Desc:	Routines to convert between UTC and GST time.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 * This file was taken from original ephem source code
 * Copyright (c) 1990,1991,1992 by Elwood Charles Downey
 * (Code cleanup by Jonathan Woithe, March 2000)
 *
 * It has been modified to run properly on TI calculators.
 */


#ifdef __TIGCC_ENV__
#include <timath.h>
#else
#include <math.h>
#endif
#include "astro.h"
#include "ephem.h"

double tnaught (double);


/* given a modified julian date, mjd, and a universally coordinated time, utc,
 * return greenwich mean siderial time, *gst.
 */
void utc_gst (double mjd, double utc, double *gst)
{
	static double lastmjd = -10000., lastutc = -100., lastgst = -100.;
	static double t0 = -100.;

	if (mjd != lastmjd) {
	  	t0 = tnaught (mjd);
		lastmjd = mjd;
	}

	if (mjd != lastmjd || utc != lastutc) {
		lastutc = utc;
		lastgst = (1./SIDRATE)*lastutc + t0 - 24.*floor(lastgst/24.);
	}

	*gst = lastgst;
}

/* given a modified julian date, mjd, and a greenwich mean siderial time, gst,
 * return universally coordinated time, *utc.
 */
void gst_utc (double mjd, double gst, double *utc)
{
	static double lastmjd = -10000., lastgst = -100., lastutc = -100.;
	static double t0 = 0.;

	if (mjd != lastmjd) {
    	t0 = tnaught (mjd);
		t0 -= 24.*floor(t0/24.);
    	lastmjd = mjd;
    }

	if (mjd != lastmjd || gst != lastgst) {
	    lastgst = gst;
		lastutc = (lastgst - t0 - 24.*floor(lastutc/24.)) * SIDRATE;
	}

	*utc = lastutc;
}

double tnaught (double mjd)
{
	double dmjd;
	short m, y;
	double d, t;

	mjd_cal (mjd, &m, &d, &y);
	cal_mjd (1., 0., y, &dmjd);
	t = dmjd/36525.;
	return (6.57098e-2 * (mjd - dmjd) -
		(24. - (6.6460656 + (5.1262e-2 + (t * 2.581e-5))*t) -
		(2400. * (t - (((double)y - 1900.)/100.)))));
}
