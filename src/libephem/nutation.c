/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	nutation.c
 * Desc:	Compute current nutation in longitude and obliquity.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 * This file was taken from original ephem source code
 * Copyright (c) 1990,1991,1992 by Elwood Charles Downey
 * (Code cleanup by Jonathan Woithe, March 2000)
 *
 * It has been modified to run properly on TI calculators.
 */


#ifdef __TIGCC_ENV__
#include <timath.h>
#else
#include <math.h>
#endif
#include "astro.h"
#include "ephem.h"


/* given the modified JD, mjd, find the nutation in obliquity, *deps, and
 * the nutation in longitude, *dpsi, each in radians.
 */
void nutation (double mjd, double *deps, double *dpsi)
{
	static double lastmjd = -10000., lastdeps = -100., lastdpsi = -100.;
	double ls, ld;	/* sun's mean longitude, moon's mean longitude */
	double ms, md;	/* sun's mean anomaly, moon's mean anomaly */
	double nm;	/* longitude of moon's ascending node */
	double t, t2;	/* number of Julian centuries of 36525 days since
			 * Jan 0.5 1900.
			 */
	double a, b;	/* temps */
	double tnm, tls, tld;
	double snm,cnm, sms, smd, stnm,ctnm, stls,ctls, stld,ctld;

	if (mjd == lastmjd) {
	    *deps = lastdeps;
	    *dpsi = lastdpsi;
	    return;
	}

	t = mjd/36525.;
	t2 = t*t;

	a = 100.0021358*t;
	b = 360.*(a-(long)a);
	ls = 279.697+.000303*t2+b;

	a = 1336.855231*t;
	b = 360.*(a-(long)a);
	ld = 270.434-.001133*t2+b;

	a = 99.99736056000026*t;
	b = 360.*(a-(long)a);
	ms = 358.476-.00015*t2+b;

	a = 13255523.59*t;
	b = 360.*(a-(long)a);
	md = 296.105+.009192*t2+b;

	a = 5.372616667*t;
	b = 360.*(a-(long)a);
	nm = 259.183+.002078*t2-b;

	/* convert to radian forms for use with trig functions.
	 */
	nm = degrad(nm);
	ms = degrad(ms);
	md = degrad(md);
	tnm = 2*nm;
	tls = 2*degrad(ls);
	tld = 2*degrad(ld);
#ifdef _TI_OPTIMIZED
	sincos(nm, 0, &snm, &cnm);
	sincos(ms, 0, &sms, &a);
	sincos(md, 0, &smd, &a);
	sincos(tnm, 0, &stnm, &ctnm);
	sincos(tls, 0, &stls, &ctls);
	sincos(tld, 0, &stld, &ctld);
#else
	snm = sin(nm);
	cnm = cos(nm);
	sms = sin(ms);
	smd = sin(ms);
	stnm = sin(tnm);
	ctnm = cos(tnm);
	stls = sin(tls);
	ctls = cos(tls);
	stld = sin(tld);
	ctld = cos(tld);
#endif

	// find delta psi and eps, in arcseconds.
	lastdpsi =	(-17.2327-.01737*t)*snm			+(-1.2729-.00013*t)*stls
				+.2088*stnm						-.2037*stld
				+(.1261-.00031*t)*sms			+.0675*smd
				-(.0497-.00012*t)*sin(tls+ms)	-.0342*sin(tld-nm)
				-.0261*sin(tld+md)				+.0214*sin(tls-ms)
				-.0149*sin(tls-tld+md)			+.0124*sin(tls-nm)
				+.0114*sin(tld-md);
	lastdeps = 	(9.21+.00091*t)*cnm				+(.5522-.00029*t)*ctls
				-.0904*ctnm						+.0884*ctld
				+.0216*cos(tls+ms)				+.0183*cos(tld-nm)
				+.0113*cos(tld+md)				-.0093*cos(tls-ms)
				-.0066*cos(tls-nm);

	// convert to radians.
	lastdpsi = degrad(lastdpsi/3600.);
	lastdeps = degrad(lastdeps/3600.);

	lastmjd = mjd;
	*deps = lastdeps;
	*dpsi = lastdpsi;
}