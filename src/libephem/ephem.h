/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	ephem.h
 * Desc:	Main header file (including all necessary prototypes,
 *			shortcuts, defines etc.)
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef _EPHEM_H
#define _EPHEM_H


/* Print Warning message - you should define 'TI' at the compiler command
 * line to enable some TI specific features which would improve performance
 * and decrease size of ephem68k.
 */
#if defined(__TIGCC_ENV__) && !defined(TI)
#warning "Compiling with TIGCC, but not in TI mode"
#endif


/* Defines instead of 'helper functions' - this will gain some speed and
 * (hopefully!?) save a few bytes because no function calls have to be done.
 */
#define refractex(pr,tr,ta,aa) \
		if ((ta) > degrad(-5.)) refract ((pr),(tr),(ta),(aa))
#define unrefractex(pr,tr,ta,aa) \
		if ((ta) > degrad(-5.)) unrefract ((pr),(tr),(ta),(aa))
#define range(v,r)		(v) -= (r)*floor((v)/(r))
#define range2(v,r)		((v) - (r)*floor((v)/(r)))

#define NEG(b)			((b) ? -1 : 1)


/* Menu (=current screen) identifiers */
#define M_MAIN		0
#define M_DATA1		1
#define M_DATA2		2
#define M_RISET		3
#define M_SKYDOME	4
#define M_HORIZON	5

#define MAXARGS 	20


/* Enable some TI specific features... */
#if defined(TI)

#define double		float
/* timath.h contains no round function (???) - so this is a good way */
#define round(n)	(n - (long)(n) < 0.5 ? floor(n) : ceil(n))
/* Turn on optimizations in other parts of ephem68k */
#define _TI_OPTIMIZED

/* This is very important for using DLLs - we have to recompile all parts
 * of ephem68k if we change one of it (e.g., if we change the DLL version
 * number, we would have to recompile the main app, too).
 */
#define DLL_1_IDENT		"zephem"
#define DLL_1_ID		0x6870657A
#define DLL_1_VERSION	1,1

/* Shortcuts for using functions from a DLL - only enabled if NOT compiling
 * a DLL (which is specified by the DLL define given at the command line
 * of TIGCC) - otherwise the DLL functions would be interpreted as one of
 * these macros...
 */
#if !defined(DLL)

#include <dll.h>

/**** Shortcut defines for DLL calls ****/
#define dll_getinfo		_DLL_call(void, (DLL_INFO *), 0)

/* anomaly.c @DLL */
#define anomaly			_DLL_call(void, (double, double, double *, double *), 1)
/* cal_mjd.c @DLL */
#define cal_mjd			_DLL_call(void, (short, double, short, double *), 2)
#define mjd_cal			_DLL_call(void, (double, short *, double *, short *), 3)
#define year_mjd		_DLL_call(void, (double, double *), 4)
#define mjd_dow			_DLL_call(void, (double, short *), 5)
#define mjd_dpm			_DLL_call(void, (double, short *), 6)
#define mjd_year		_DLL_call(void, (double, double *), 7)
#define mjd_day			_DLL_call(double, (double), 8)
#define mjd_hr			_DLL_call(double, (double), 9)
/* comet.c @DLL */
#define comet			_DLL_call(void, (double, double, double, double, double, double, double, \
								double, double *, double *, double *, double *, double *, \
								double *), 10)
/* ecl_eq.c @DLL */
#define ecleq_aux		_DLL_call(void, (short, Now *, double, double, double *, double *), 11)
/* moon.c @DLL */
#define moon			_DLL_call(void, (double, double *, double *, double *), 12)
/* moonnf.c @DLL */
#define moonnf			_DLL_call(void, (double, double *, double *), 13)
/* nutation.c @DLL */
#define nutation		_DLL_call(void, (double, double *, double *), 14)
/* parallax.c @DLL */
#define ta_par			_DLL_call(void, (double, double, double, double, double, double *, \
								double *), 15)
/* plans.c @DLL */
#define plans			_DLL_call(void, (double, short, double, double, double *, double *, \
								double *, double *, double *, double *, double *, double *), 16)
/* precess.c @DLL */
#define precess			_DLL_call(void, (double, double, double *, double *), 17)
/* objx.c @DLL */
#define obj_cir			_DLL_call(void, (Now *, Obj *, double *, double *, double *, double *, \
								double *, double *, double *, double *), 18)
/* obliq.c @DLL */
#define obliquity		_DLL_call(void, (double, double *), 19)
/* objx.c @DLL */
#define reduce_elements	_DLL_call(void, (double, double, double, double, double, double *, \
								double *, double *), 20)
/* refract.c @DLL */
#define refract			_DLL_call(void, (double, double, double, double *), 21)
#define unrefract		_DLL_call(void, (double, double, double, double *), 22)
/* sun.c @DLL */
#define sunpos			_DLL_call(void, (double, double *, double *), 23)

#endif
#endif


/* Structure used by getdllinfo() */
typedef struct {
	long id;
	char ver[8],
		name[16],
		date[16],
		cver[16];
} DLL_INFO;


/**** Function prototypes from all parts of ephem68k */
/* From aa_hadec.c */
void aaha_aux (double, double, double, double *, double *);
#if defined(TI)
#define aa_hadec(lat, alt, az, ha, dec)	aaha_aux (lat, az, alt, ha, dec)
#define hadec_aa(lat, ha, dec, alt, az)	aaha_aux (lat, ha, dec, az, alt)
#else
void aa_hadec (double, double, double, double *, double *);
void hadec_aa (double, double, double, double *, double *);
#endif

/* From anomaly.c */
#if !defined(TI) || (defined(TI) && defined(DLL))
void anomaly (double, double, double *, double *);
#endif

/* From cal_mjd.c */
#if !defined(TI) || (defined(TI) && defined(DLL))
void cal_mjd (short, double, short, double *);
void mjd_cal (double, short *, double *, short *);
void year_mjd (double, double *);
void mjd_dow (double, short *);
void mjd_dpm (double, short *);
void mjd_year (double, double *);
double mjd_day(double);
double mjd_hr(double);
#endif

/* From circum.c */
#ifdef _CIRCUM_H
void update_const(Now *);
short body_cir (short, double, Now *, Obj *, Sky *);
short twilight_cir (Now *, double *, double *, short *, short);
short sun_cir (double, Now *, Sky *);
short moon_cir (double, Now *, Sky *);
short same_cir (Now *, Now *);
short same_lday (Now *, Now *);
void now_lst (Now *, double *);
#endif
void elongation (double, double, double, double *);
void rnd_second (double *);

/* From comet.c */
#if !defined(TI) || (defined(TI) && defined(DLL))
void comet (double, double, double, double, double, double, double, double, double *, double *, \
		double *, double *, double *, double *);
#endif

/* From eq_ecl.c */
#if !defined(TI) || (defined(TI) && defined(DLL))
#ifdef _CIRCUM_H
void ecleq_aux(short, Now *, double, double, double *, double *);
#endif
#endif
#if defined(TI)
#define eq_ecl(np, ra, dec, lat, lng)	ecleq_aux (1, np, ra, dec, lng, lat)
#define ecl_eq(np, lat, lng, ra, dec)	ecleq_aux (-1, np, lng, lat, ra, dec)
#else
#ifdef _CIRCUM_H
void eq_ecl (Now *, double, double, double *, double *);
void ecl_eq (Now *, double, double, double *, double *);
#endif
#endif

/* From moon.c */
#if !defined(TI) || (defined(TI) && defined(DLL))
void moon (double, double *, double *, double *);
#endif

/* From moonnf.c */
#if !defined(TI) || (defined(TI) && defined(DLL))
void moonnf (double, double *, double *);
#endif

/* From nutation.c */
#if !defined(TI) || (defined(TI) && defined(DLL))
void nutation (double, double *, double *);
#endif

/* From objx.c */
#if !defined(TI) || (defined(TI) && defined(DLL))
#ifdef _CIRCUM_H
void obj_cir (Now *, Obj *, double *, double *, double *, double *, double *, double *, double *, \
		double *);
#endif
#endif

/* From obliq.c */
#if !defined(TI) || (defined(TI) && defined(DLL))
void obliquity (double, double *);
#endif

/* From parallax.c */
#if !defined(TI) || (defined(TI) && defined(DLL))
void ta_par (double, double, double, double, double, double *, double *);
#endif
#ifdef NEEDIT
void at_par (double, double, double, double, double, double *, double *);
#endif

/* From pelement.c */
#if !defined(TI) || (defined(TI) && defined(DLL))
void pelement (double, double[7][9]);
#endif

/* From plans.c */
#if !defined(TI) || (defined(TI) && defined(DLL))
void plans (double, short, double, double, double *, double *, double *, double *, double *, \
		double *, double *, double *);
#endif

/* From precess.c */
#if !defined(TI) || (defined(TI) && defined(DLL))
void precess (double, double, double *, double *);
#endif

/* From reduce.c */
#if !defined(TI) || (defined(TI) && defined(DLL))
void reduce_elements (double, double, double, double, double, double *, double *, double *);
#endif

/* From refract.c */
#if !defined(TI) || (defined(TI) && defined(DLL))
void refract (double, double, double, double *);
void unrefract (double, double, double, double *);
#endif

/* From riset.c */
void riset (double, double, double, double, double *, double *, double *, double *, short *);

/* From riset_c.c */
#ifdef _CIRCUM_H
short riset_cir (short, Now *, Obj *, short, short, double *, double *, double *, double *, \
		double *, double *, short *);
#endif

/* From sex_dec */
void sex_dec (short, short, short, double *);
void dec_sex (double, short *, short *, short *, short *);

/* From sun.c */
#if !defined(TI) || (defined(TI) && defined(DLL))
void sunpos (double, double *, double *);
#endif

/* From utc_gst.c */
void gst_utc (double, double, double *);
void utc_gst (double, double, double *);

/* From mainmenu.c */
void mm_labels(void);
#ifdef _CIRCUM_H
void mm_now(Now *, short);
void mm_twilight (Now *, short, short);
#endif

/* From altmenus.c */
short nxtbody(short, short);
void alt_labels(short);
#ifdef _CIRCUM_H
void alt_body(short, short, short, Now *, Obj *, short);
#endif

/* From watch.c */
void watch_labels(const short, const short, const short);
#ifdef _CIRCUM_H
void watch_body(const short, const short, const short, const Now *, const Obj *);
void draw_wbody(Watch *, const short, const short, const short, const Obj *);
#endif
void show_wbody(const short, const unsigned short);
void sel_wbody(const short);

/* From shared.c */
void bye(void);
short cfg_write(void);
short cfg_read(void);
#ifdef _CIRCUM_H
short db_read(char [], short, Obj *);
short obj_load(char [], char [], Obj *);
void obj_setfixed(ObjF *, char *[MAXARGS]);
void obj_setelliptical(ObjE *, char *[MAXARGS]);
void obj_sethyperbolic(ObjH *, char *[MAXARGS]);
void obj_setparabolic(ObjP *, char *[MAXARGS]);
#endif
void crack_year(char *, double *);
const char *objectname(short);
void load_dll(void);
short nxtbody(short, short);

/* From version.c */
void splash(void);

#endif
