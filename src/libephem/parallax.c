/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	parallax.c
 * Desc:	Routines to correct for parallax.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 * This file was taken from original ephem source code
 * Copyright (c) 1990,1991,1992 by Elwood Charles Downey
 * (Code cleanup by Jonathan Woithe, March 2000)
 *
 * It has been modified to run properly on TI calculators.
 */


#ifdef __TIGCC_ENV__
#include <timath.h>
#else
#include <math.h>
#endif
#include "astro.h"
#include "ephem.h"


/* given true ha and dec, tha and tdec, the geographical latitude, phi, the
 * height above sea-level (as a fraction of the earths radius, 6378.16km),
 * ht, and the equatorial horizontal parallax, ehp, find the apparent
 * ha and dec, aha and adec allowing for parallax.
 * all angles in radians. ehp is the angle subtended at the body by the
 * earth's equator.
 */
void ta_par (double tha, double tdec, double phi, double ht, double ehp, double *aha, double *adec)
{
	static double last_phi = -1000., last_ht = -1000., rsp = -100., rcp = -100.;
	double rp;	/* distance to object in Earth radii */
	double stha, ctha;
	double stdec, ctdec;
	double tdtha, dtha;
	double caha;

	/* avoid calcs involving the same phi and ht */
	if (phi != last_phi || ht != last_ht) {
	  double cphi, sphi, su, cu;
	  double u = atan(9.96647e-1*sphi/cphi);
#ifdef _TI_OPTIMIZED
	  sincos(phi, 0, &sphi, &cphi);
	  sincos(u, 0, &su, &cu);
#else
	  sphi = sin(phi);
	  cphi = cos(phi);
	  su = sin(u);
	  cu = cos(u);
#endif
	  rsp = (9.96647e-1*su)+(ht*sphi);
	  rcp = cu+(ht*cphi);
	  last_phi  =  phi;
	  last_ht   =  ht;
	}

	rp = 1./sin(ehp);
#ifdef _TI_OPTIMIZED
	sincos(tdec, 0, &stdec, &ctdec);
	sincos(tha, 0, &stha, &ctha);
#else
  stha = sin(tha);
  ctha = cos(tha);
	stdec = sin(tdec);
	ctdec = cos(tdec);
#endif
  tdtha = (rcp*stha)/((rp*ctdec)-(rcp*ctha));
  dtha = atan(tdtha);
	*aha = tha+dtha;
	caha = cos(*aha);
	range (*aha, 2.*PI);
  *adec = atan(caha*(rp*stdec-rsp)/(rp*ctdec*ctha-rcp));
}

#ifdef NEEDIT
/* given the apparent ha and dec, aha and adec, the geographical latitude, phi,
 * the height above sea-level (as a fraction of the earths radius, 6378.16km),
 * ht, and the equatorial horizontal parallax, ehp, find the true ha and dec,
 * tha and tdec allowing for parallax.
 * all angles in radians. ehp is the angle subtended at the body by the
 * earth's equator.
 * uses ta_par() iteratively: find a set of true ha/dec that converts back
  *  to the given apparent ha/dec.
 */
void at_par (double aha, double adec, double phi, double ht, double ehp, double *tha, double *tdec)
{
	double nha, ndec;	/* ha/dec corres. to current true guesses */
	double eha, edec;	/* error in ha/dec */

	/* first guess for true is just the apparent */
	*tha = aha;
	*tdec = adec;

	while (1) {
	    ta_par (*tha, *tdec, phi, ht, ehp, &nha, &ndec);
	    eha = aha - nha;
	    edec = adec - ndec;
	    if (fabs(eha)<1e-6 && fabs(edec)<1e-6)
		break;
	    *tha += eha;
	    *tdec += edec;
	}
}

#endif
