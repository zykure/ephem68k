/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	cal_mjd.c
 * Desc:	Routines for handling the MJD date/time format.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 * This file was taken from original ephem source code
 * Copyright (c) 1990,1991,1992 by Elwood Charles Downey
 * (Code cleanup by Jonathan Woithe, March 2000)
 *
 * It has been modified to run properly on TI calculators.
 */


#ifdef __TIGCC_ENV__
#include <timath.h>
#else
#include <math.h>
#endif
#include "astro.h"
#include "ephem.h"


/* given a date in months, mn, days, dy, years, yr,
 * return the modified Julian date (number of days elapsed since 1900 jan 0.5),
 * *mjd.
 */
void cal_mjd (short mn, double dy, short yr, double *mjd)
{
	short b, d, m, y;
	long c;

	m = mn;
	y = (yr < 0) ? yr + 1 : yr;
	if (mn < 3) {
	  m += 12;
	  y -= 1;
	}

	if (yr < 1582 || (yr == 1582 && (mn < 10 || (mn == 10 && dy < 15.))))
	    b = 0;
	else {
	    int a;
	    a = y/100;
	    b = 2 - a + a/4;
	}

	if (y < 0)
	    c = (long)((365.25*(double)y) - 0.75) - 694025.L;
	else
	    c = (long)(365.25*(double)y) - 694025.L;

	d = 30.6001*(m+1);

	*mjd = b + c + d + dy - 0.5;
}

/* given the modified Julian date (number of days elapsed since 1900 jan 0.5,),
 * mjd, return the calendar date in months, *mn, days, *dy, and years, *yr.
 */
void mjd_cal (double mjd, short *mn, double *dy, short *yr)
{
	double d, f;
	double i, a, b, ce, g;

	d = mjd + 0.5;
	i = floor(d);
	f = d - i;
	if (f == 1.) {
	  f = 0.;
	  i += 1.;
	}

	if (i > -115860.) {
	    a = floor((i/36524.25)+.9983573)+14.;
	    i += 1. + a - floor(a/4.);
	}

	b = floor((i/365.25)+.802601);
	ce = i - floor((365.25*b)+.750001)+416.;
	g = floor(ce/30.6001);
	*mn = g - 1.;
	*dy = ce - floor(30.6001*g)+f;
	*yr = b + 1899.;

	if (g > 13.5)
	    *mn = (short) (g - 13.);
	if (*mn < 2.5)
	    *yr = (short) (b + 1900.);
	if (*yr < 1)
	    *yr -= 1;
}

/* given an mjd, set *dow to 0..6 according to which dayof the week it falls
 * on (0=sunday) or set it to -1 if can't figure it out.
 */
void mjd_dow (double mjd, short *dow)
{
	/* cal_mjd() uses Gregorian dates on or after Oct 15, 1582.
	 * (Pope Gregory XIII dropped 10 days, Oct 5..14, and improved the leap-
	 * year algorithm). however, Great Britian and the colonies did not
	 * adopt it until Sept 14, 1752 (they dropped 11 days, Sept 3-13,
	 * due to additional accumulated error). leap years before 1752 thus
	 * can not easily be accounted for from the cal_mjd() number...
	 */
	if (mjd < -53798.5) {
	    /* pre sept 14, 1752 too hard to correct */
	    *dow = -1;
	    return;
	}
	*dow = ((long)floor(mjd-.5)) % 7;/* 1/1/1900 (mjd 0.5) is a Monday -> dow = 0*/
	if (*dow < 0)
	    *dow += 7;
}

/* given a mjd, return the the number of days in the month.  */
void mjd_dpm (double mjd, short *ndays)
{
	static const short dpm[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	short m, y;
	double d;

	mjd_cal (mjd, &m, &d, &y);
	*ndays = (m==2 && ((y%4==0 && y%100!=0)||y%400==0)) ? 29 : dpm[m-1];
}

/* given a mjd, return the year as a double. */
void mjd_year (double mjd, double *yr)
{
	short m, y;
	double d;
	double e0, e1;	/* mjd of start of this year, start of next year */

	mjd_cal (mjd, &m, &d, &y);
	if (y == -1) y = -2;
	cal_mjd (1, 1.0, y, &e0);
	cal_mjd (1, 1.0, y+1, &e1);
	*yr = y + (mjd - e0)/(e1 - e0);
}

double mjd_day(double jd)
{
	return (floor(jd-0.5)+0.5);
}

double mjd_hr(double jd)
{
	return (jd-mjd_day(jd))*24.0 + EPS;
}

/* given a decimal year, return mjd */
void year_mjd (double y, double *mjd)
{
	double e0, e1;	/* mjd of start of this year, start of next year */
	int yf = floor(y);
	if (yf == -1) yf = -2;

	cal_mjd (1, 1., yf, &e0);
	cal_mjd (1, 1., yf+1, &e1);
	*mjd = e0 + (y - yf)*(e1-e0);
}
