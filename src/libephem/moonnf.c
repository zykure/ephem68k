/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	moonnf.c
 * Desc:	Find new moon and full moon dates of one month.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 * This file was taken from original ephem source code
 * Copyright (c) 1990,1991,1992 by Elwood Charles Downey
 * (Code cleanup by Jonathan Woithe, March 2000)
 *
 * It has been modified to run properly on TI calculators.
 */


#ifdef __TIGCC_ENV__
#include <timath.h>
#else
#include <math.h>
#endif
#include "astro.h"
#include "ephem.h"

#define	unw(w,z)	((w)-floor((w)/(z))*(z))

void m(double t, double k, double *mjd);


/* given a modified Julian date, mjd, return the mjd of the new
 * and full moons about then, mjdn and mjdf.
 * TODO: exactly which ones does it find? eg:
 *   5/28/1988 yields 5/15 and 5/31
 *   5/29             6/14     6/29
 */
void moonnf (double mjd, double *mjdn, double *mjdf)
{
	short mo, yr;
	double dy;
	double mjd0;
	double k, tn, tf, t;

	mjd_cal (mjd, &mo, &dy, &yr);
	cal_mjd (1, 0.0, yr, &mjd0);
	k = (yr-1900.+((mjd-mjd0)/365.))*12.3685;
	k = floor(k+0.5);
	tn = k/1236.85;
	tf = (k+0.5)/1236.85;
	t = tn;
	m (t, k, mjdn);
	t = tf;
	k += 0.5;
	m (t, k, mjdf);
}

void m(double t, double k, double *mjd)
{
	double t2, a, a1, b, b1, c, ms, mm, f, ddjd;

	t2 = t*t;
	a = 29.53*k;
	c = degrad(166.56+(132.87-9.173e-3*t)*t);
	b = 5.8868e-4*k+(1.178e-4-1.55e-7*t)*t2+3.3e-4*sin(c)+7.5933E-1;
	ms = 359.2242+360*unw(k/1.236886e1,1)-(3.33e-5+3.47e-6*t)*t2;
	mm = 306.0253+360*unw(k/9.330851e-1,1)+(1.07306e-2+1.236e-5*t)*t2;
	f = 21.2964+360*unw(k/9.214926e-1,1)-(1.6528e-3+2.39e-6*t)*t2;
	ms = degrad(unw(ms,360.));
	mm = degrad(unw(mm,360.));
	f = degrad(unw(f,360.));
	ddjd = (1.734e-1-3.93e-4*t)*sin(ms)
			+2.1e-3*sin(2*ms)	-4.068e-1*sin(mm)	+1.61e-2*sin(2*mm)
			-4.e-4*sin(3*mm)	+1.04e-2*sin(2*f)	-5.1e-3*sin(ms+mm)
			-7.4e-3*sin(ms-mm)	+4.e-4*sin(2*f+ms)	-4.e-4*sin(2*f-ms)
			-6.e-4*sin(2*f+mm)	+1.e-3*sin(2*f-mm)	+5.e-4*sin(ms+2*mm);
	a1 = (long)a;
	b = b+ddjd+(a-a1);
	b1 = (long)b;
	a = a1+b1;
	b = b-b1;
	*mjd = a + b;
}
