/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	circum.c
 * Desc:	Compute all circumstances for an object.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 * This file was taken from original ephem source code
 * Copyright (c) 1990,1991,1992 by Elwood Charles Downey
 * (Code cleanup by Jonathan Woithe, March 2000)
 *
 * It has been modified to run properly on TI calculators.
 */


#ifdef __TIGCC_ENV__
#include <timath.h>
#else
#include <math.h>
#endif
#include "astro.h"
#include "circum.h"
#include "ephem.h"

inline short about_now (Now *n1, Now *n2, double dt);

/* update pseudo-constants inside the Now structure
 * this has to be done only once for one mjd
 */
void update_const(Now *np)
{
	now_lst(np, (double*) &np->n_const.c_lst);
	obliquity(np->n_mjd, (double*) &np->n_const.c_eps);
	sunpos(np->n_mjd, (double*) &np->n_const.c_lsn,
			(double*) &np->n_const.c_rsn);
	nutation(np->n_mjd, (double*) &np->n_const.c_deps,
			(double*) &np->n_const.c_dpsi);
}

/* find body p's circumstances now.
 * to save some time the caller may specify a desired accuracy, in arc seconds.
 * if, based on its mean motion, it would not have moved this much since the
 * last time we were called we only recompute altitude and azimuth and avoid
 * recomputing the planet's heliocentric position. use 0.0 for best possible.
 * we always recompute the user-defined objects' position regardless.
 * return 0 if only alt/az changes, else 1 if all other stuff updated too.
 * N.B: values are for opposition, ie, at fastest retrograde.
 */
short body_cir (short p, double as, Now *np, Obj *op, Sky *sp)
{
	typedef struct {
	    double l_dpas;	/* mean days per arc second */
	    Now l_now;		/* when l_sky was found */
	    double l_ra, l_dec;	/* the eod, ie, unprecessed, ra/dec values */
	    Sky l_sky;
	} Last;
	/* must be in same order as the astro.h object #define's */
	static Last last[8] = {
	    {.000068, NOW_NULL, 0., 0., SKY_NULL},	/* mercury */
	    { .00017, NOW_NULL, 0., 0., SKY_NULL},	/* venus */
	    { .00015, NOW_NULL, 0., 0., SKY_NULL},	/* mars */
	    {  .0012, NOW_NULL, 0., 0., SKY_NULL},	/* jupiter */
	    {  .0024, NOW_NULL, 0., 0., SKY_NULL},	/* saturn */
	    {  .0051, NOW_NULL, 0., 0., SKY_NULL},	/* uranus */
	    {  .0081, NOW_NULL, 0., 0., SKY_NULL}		/* neptune */
	};
	static Last objlast[NOBJ-OBJ1] = {
		  {0., NOW_NULL, 0., 0., SKY_NULL},
		  {0., NOW_NULL, 0., 0., SKY_NULL},
		  {0., NOW_NULL, 0., 0., SKY_NULL},
		  {0., NOW_NULL, 0., 0., SKY_NULL},
	};
	double alt, az;
	double ehp, ha, dec;	/* ehp: angular dia of earth from body */
	Last *lp;
	short new;

	if (p == SUN)
		return (sun_cir (as, np, sp));
	else if (p == MOON)
		return (moon_cir (as, np, sp));
	else if (p >= OBJ1 && p <= OBJ4)
		lp = &objlast[p-OBJ1];
	else
		lp = last + p;

	/* if less than l_every days from last time for this planet
	 * just redo alt/az.
	 * ALWAYS redo objects x and y.
	 */
	if ((p <= OBJ1 || p >= OBJ4) && same_cir (np, &lp->l_now)
		      && about_now (np, &lp->l_now, as*lp->l_dpas)) {
	    *sp = lp->l_sky;
	    new = 0;
	} else {
	    double lpd0, psi0;	/* heliocentric ecliptic long and lat */
	    double rp0;		/* dist from sun */
	    double rho0;	/* dist from earth */
	    double lam, bet;	/* geocentric ecliptic long and lat */
	    double dia, mag;	/* angular diameter at 1 AU and magnitude */
	    double el;	/* elongation */
	    double f;   /* phase from earth */

	    lp->l_now = *np;
	    if (p >= OBJ1 && p <= OBJ4) {
				obj_cir(np, op, &lpd0, &psi0, &rp0, &rho0, &lam, &bet,
						(double*) &sp->s_size, (double*) &sp->s_mag);
	    } else {
				double a;
				plans(np->n_mjd, p, np->n_const.c_lsn, np->n_const.c_rsn,
				  	&lpd0, &psi0, &rp0, &rho0, &lam, &bet, &dia, &mag);
				lam += np->n_const.c_dpsi;
				a = np->n_const.c_lsn-lam;			/* and 20.4" aberation */
				lam -= degrad(20.4/3600.)*cos(a)/cos(bet);
				bet -= degrad(20.4/3600.)*sin(a)*sin(bet);
	    }

	    ecl_eq (np, bet, lam, &lp->l_ra, &lp->l_dec);

	    sp->s_ra = lp->l_ra;
	    sp->s_dec = lp->l_dec;
	    if (np->n_epoch != EOD)
			precess (np->n_mjd, np->n_epoch, (double*) &sp->s_ra, (double*) &sp->s_dec);
	    sp->s_edist = rho0;
	    sp->s_sdist = rp0;
	    elongation (lam, bet, np->n_const.c_lsn, &el);
	    el = raddeg(el);
	    sp->s_elong = el;
	    f = (rp0 > 0.) ? 0.25 * (((rp0+rho0)*(rp0+rho0)
	    	- np->n_const.c_rsn*np->n_const.c_rsn)/(rp0*rho0)) : 0.;
	    sp->s_phase = f*100.; /* percent */
	    if (p != OBJ1 && p != OBJ2 && p != OBJ3 && p != OBJ4) {
				sp->s_size = dia/rho0;
				sp->s_mag = mag + 5.*log(rp0*rho0/sqrt(f))/log(10.);
	    }
	    sp->s_hlong = lpd0;
	    sp->s_hlat = psi0;
	    new = 1;
	}

	/* alt, az; correct for parallax and refraction; use eod ra/dec */
	ha = hrrad(np->n_const.c_lst) - lp->l_ra;
	if (sp->s_edist > 0.) {
	  ehp = (2.*6378./146.e6) / sp->s_edist;
	  ta_par (ha, lp->l_dec, np->n_lat, np->n_height, ehp, &ha, &dec);
	} else
	  dec = lp->l_dec;
	hadec_aa (np->n_lat, ha, dec, &alt, &az);
	refractex (np->n_pressure, np->n_temp, alt, &alt);
	sp->s_alt = alt;
	sp->s_az = az;
	lp->l_sky = *sp;

	return (new);
}

/* find local times when sun is 18 degrees below horizon.
 * return 0 if just returned same stuff as previous call, else 1 if new.
 */
short twilight_cir (Now *np, double *dawn, double *dusk, short *status, short mode)
{
	static Now last_now = NOW_NULL;
	static double last_dawn = -100., last_dusk = -100.;
	static int last_status = -10, last_mode = -10;
	short new;

	if (same_cir (np, &last_now) && same_lday (np, &last_now) && (mode == last_mode)) {
	    *dawn = last_dawn;
	    *dusk = last_dusk;
	    *status = last_status;
	    new = 0;
	} else {
	    double x;
	    riset_cir (SUN,np,(Obj *) 0, 0., mode == TWILIGHTCIV ? TWILIGHTCIV : TWILIGHT,
	    	dawn, dusk, &x, &x, &x, &x, status);
	    last_dawn = *dawn;
	    last_dusk = *dusk;
	    last_status = *status;
	    last_now = *np;
	    last_mode = mode;
	    new = 1;
	}

	return (new);
}

/* find sun's circumstances now.
 * as is the desired accuracy, in arc seconds; use 0.0 for best possible.
 * return 0 if only alt/az changes, else 1 if all other stuff updated too.
 */
short sun_cir (double as, Now *np, Sky *sp)
{
	static Sky last_sky = SKY_NULL;
	static Now last_now = NOW_NULL;
	static double last_ra = -100., last_dec = -100.;	/* unprecessed ra/dec */
	double alt, az;
	double ehp, ha, dec;	/* ehp: angular dia of earth from body */
	short new;

	if (same_cir (np, &last_now) && about_now (np, &last_now, as*.00028)) {
	  *sp = last_sky;
	  new = 0;
	} else {
		double lsn = np->n_const.c_lsn;
	  last_now = *np;
	  lsn += np->n_const.c_dpsi;	/* correct for nutation */
	  lsn -= degrad(20.4/3600.);		/* and light travel time */

	  sp->s_edist = np->n_const.c_rsn;
	  sp->s_sdist = 0.;
	  sp->s_elong = 0.;
	  sp->s_size = raddeg(4.65242e-3/np->n_const.c_rsn)*3600.*2.;
	  sp->s_mag = -26.8;
	  sp->s_hlong = lsn-PI;	/* geo- to helio- centric */
	  range (sp->s_hlong, TWOPI);
	  sp->s_hlat = 0.;

	  ecl_eq (np, 0., lsn, &last_ra, &last_dec);
	  sp->s_ra = last_ra;
	  sp->s_dec = last_dec;
	  if (np->n_epoch != EOD)
			precess (np->n_mjd, np->n_epoch, (double*) &sp->s_ra, (double*) &sp->s_dec);
    new = 1;
	}

	ha = hrrad(np->n_const.c_lst) - last_ra;
	ehp = (2. * 6378. / 146.e6) / sp->s_edist;
	ta_par (ha, last_dec, np->n_lat, np->n_height, ehp, &ha, &dec);
	hadec_aa (np->n_lat, ha, dec, &alt, &az);
	refractex (np->n_pressure, np->n_temp, alt, &alt);
	sp->s_alt = alt;
	sp->s_az = az;
	last_sky = *sp;

	return (new);
}

/* find moon's circumstances now.
 * as is the desired accuracy, in arc seconds; use 0.0 for best possible.
 * return 0 if only alt/az changes, else 1 if all other stuff updated too.
 */
short moon_cir (double as, Now *np, Sky *sp)
{
	static Sky last_sky = SKY_NULL;
	static Now last_now = NOW_NULL;
	static double ehp = -100.;
	static double last_ra = -100., last_dec = -100.;	/* unprecessed */
	double alt, az;
	double ha, dec;
	short new;

	if (same_cir (np, &last_now) && about_now (np, &last_now, as*.000021)) {
	  *sp = last_sky;
	  new = 0;
	} else {
	  double lam, bet;
	  double edistau;	/* earth-moon dist, in au */
	  double el;		/* elongation, rads east */

	  last_now = *np;
	  moon (np->n_mjd, &lam, &bet, &ehp);	/* moon's true ecliptic loc */
	  lam += np->n_const.c_dpsi;
		range (lam, TWOPI);

	  sp->s_edist = 6378.14/sin(ehp);	/* earth-moon dist, want km */
	  sp->s_size = 3600.*31.22512*sin(ehp);/* moon angular dia, seconds */
	  sp->s_hlong = lam;			/* save geo in helio fields */
	  sp->s_hlat = bet;

	  ecl_eq (np, bet, lam, &last_ra, &last_dec);
	  sp->s_ra = last_ra;
	  sp->s_dec = last_dec;
	  if (np->n_epoch != EOD)
			precess (np->n_mjd, np->n_epoch, (double*) &sp->s_ra, (double*) &sp->s_dec);

	  elongation (lam, bet, np->n_const.c_lsn, &el);

	  /* solve triangle of earth, sun, and elongation for moon-sun dist */
	  edistau = sp->s_edist/1.495979e8; /* km -> au */
	  sp->s_sdist =
		sqrt (edistau*edistau + np->n_const.c_rsn*np->n_const.c_rsn
				- 2.*edistau*np->n_const.c_rsn*cos(el));

	  /* TODO: improve mag; this is based on a flat moon model. */
	  sp->s_mag = -12.7 + 2.5*(log10(PI) - log10(PI/2*(1+1.e-6-cos(el))));

	  sp->s_elong = raddeg(el);	/* want degrees */
	  sp->s_phase = fabs(el)/PI*100.;	/* want non-negative % */
	  new = 1;
	}

	/* show topocentric alt/az by correcting ra/dec for parallax
	 * as well as refraction.
	 */
	ha = hrrad(np->n_const.c_lst) - last_ra;
	ta_par (ha, last_dec, np->n_lat, np->n_height, ehp, &ha, &dec);
	hadec_aa (np->n_lat, ha, dec, &alt, &az);
	refractex (np->n_pressure, np->n_temp, alt, &alt);
	sp->s_alt = alt;
	sp->s_az = az;
	last_sky = *sp;

	return (new);
}

/* given geocentric ecliptic longitude and latitude, lam and bet, of some object
 * and the longitude of the sun, lsn, find the elongation, el. this is the
 * actual angular separation of the object from the sun, not just the difference
 * in the longitude. the sign, however, IS set simply as a test on longitude
 * such that el will be >0 for an evening object <0 for a morning object.
 * to understand the test for el sign, draw a graph with lam going from 0-2*PI
 *   down the vertical axis, lsn going from 0-2*PI across the hor axis. then
 *   define the diagonal regions bounded by the lines lam=lsn+PI, lam=lsn and
 *   lam=lsn-PI. the "morning" regions are any values to the lower left of the
 *   first line and bounded within the second pair of lines.
 * all angles in radians.
 */
void elongation (double lam, double bet, double lsn, double *el)
{
	*el = acos(cos(bet)*cos(lam-lsn));
	if (lam>lsn+PI || (lam>lsn-PI && lam<lsn))
		*el = - *el;
}

/* return whether the two Nows are for the same observing circumstances. */
short same_cir (Now *n1, Now *n2)
{
	return (n1->n_lat == n2->n_lat
		&& n1->n_lng == n2->n_lng
		&& n1->n_temp == n2->n_temp
		&& n1->n_pressure == n2->n_pressure
		&& n1->n_height == n2->n_height
		&& n1->n_tz == n2->n_tz
		&& n1->n_epoch == n2->n_epoch);
}

/* return whether the two Nows are for the same LOCAL day */
short same_lday (Now *n1, Now *n2)
{
	return (mjd_day(n1->n_mjd - n1->n_tz/24.) ==
		       mjd_day(n2->n_mjd - n2->n_tz/24.));
}

/* return whether the mjd of the two Nows are within dt */
inline short about_now (Now *n1, Now *n2, double dt)
{
	return (fabs (n1->n_mjd - n2->n_mjd) <= dt/2.);
}

void now_lst (Now *np, double *lst)
{
	static double last_mjd = -10000., last_lng = -1000., last_lst = -100.;

	if (np->n_mjd != last_mjd || np->n_lng != last_lng) {
		utc_gst (mjd_day(np->n_mjd), mjd_hr(np->n_mjd), &last_lst);
		last_lst += radhr(np->n_lng);
		range (last_lst, 24.);
		last_mjd = np->n_mjd;
		last_lng = np->n_lng;
	}
	*lst = last_lst;
}

/* round a time in days, *t, to the nearest second, IN PLACE. */
void rnd_second (double *t)
{
	*t = floor(*t*SPD+0.5)/SPD;
}
