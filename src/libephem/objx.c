/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	objx.c
 * Desc:	Compute current values of the user defined objects.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 * This file was taken from original ephem source code
 * Copyright (c) 1990,1991,1992 by Elwood Charles Downey
 * (Code cleanup by Jonathan Woithe, March 2000)
 *
 * It has been modified to run properly on TI calculators.
 */


/* functions to save the user-definable objects, referred to as "x" and "y".
 * this way, once defined, the objects can be quieried for position just like
 * the other bodies, with obj_cir().
 */


#ifdef __TIGCC_ENV__
#include <timath.h>
#else
#include <math.h>
#endif
#include "astro.h"
#include "circum.h"
#include "ephem.h"

#define	DY	0		/* decimal year flag for set_year() */
#define	YMD	1		/* year/mon/day flag for set_year() */


/* fill in info about object x or y.
 * most arguments and conditions are the same as for plans().
 * only difference is that mag is already apparent, not absolute magnitude.
 * this is called by body_cir() for object x and y just like plans() is called
 * for the planets.
 */
void obj_cir (Now *np, Obj *op, double *lpd0, double *psi0, double *rp0,
	double *rho0, double *lam, double *bet, double *siz, double *mag)
{
	if (!op) {
		return;
	}

	switch (op->o_type) {
	case FIXED: {
	    double xr, xd;
	    xr = op->o_data.o_f.f_ra;
	    xd = op->o_data.o_f.f_dec;
	    if (op->o_data.o_f.f_epoch != np->n_mjd)
				precess (op->o_data.o_f.f_epoch, np->n_mjd, &xr, &xd);
	    eq_ecl (np, xr, xd, bet, lam);

	    *lpd0 = NOHELIO;
	    *psi0 = *rp0 = *rho0 = 0.;
	    *mag = op->o_data.o_f.f_mag;
	    *siz = op->o_data.o_f.f_siz;
	    }
	    break;

	case ELLIPTICAL: {
	    /* this is basically the same code as pelement() and plans()
	     * combined and simplified for the special case of osculating
	     * (unperturbed) elements.
	     * inputs have been changed to match the Astronomical Almanac.
	     * we have added reduction of elements using reduce_elements().
	     */
	    double dt, lg;
	    double nu, ea;
	    double ma, rp, lo, slo,clo, sinc,cinc;
	    double inc, psi, spsi, cpsi;
	    double y, lpd, rpd, ll, rho, sll,cll;
	    double om;		/* arg of perihelion */
	    double Om;		/* long of ascending node. */
	    double e;
	    int pass;

	    dt = 0.;
	    lg = np->n_const.c_lsn + PI;
	    e = op->o_data.o_e.e_e;

	    for (pass = 0; pass < 2; pass++) {
				reduce_elements (op->o_data.o_e.e_epoch, np->n_mjd-dt,
						degrad(op->o_data.o_e.e_inc), degrad (op->o_data.o_e.e_om),
						degrad (op->o_data.o_e.e_Om), &inc, &om, &Om);

				ma = degrad (op->o_data.o_e.e_M
						+ (np->n_mjd - op->o_data.o_e.e_cepoch - dt) * op->o_data.o_e.e_n);
				anomaly (ma, e, &nu, &ea);
				rp = op->o_data.o_e.e_a * (1-e*e) / (1+e*cos(nu));
				lo = nu + om;
#ifdef _TI_OPTIMIZED
				sincos(lo, 0, &slo, &clo);
				sincos(inc, 0, &sinc, &cinc);
#else
				slo = sin(lo);
				clo = cos(lo);
				sinc = sin(inc);
				cinc = cos(inc);
#endif
				spsi = slo*sinc;
				y = slo*cinc;
				psi = asin(spsi);
				lpd = atan(y/clo)+Om;
				if (clo<0) lpd += PI;
				range (lpd, TWOPI);
				cpsi = cos(psi);
				rpd = rp*cpsi;
				ll = lpd - lg;
				rho = sqrt(np->n_const.c_rsn*np->n_const.c_rsn
						+ rp*rp-2*np->n_const.c_rsn*rp*cpsi*cos(ll));
				dt = rho*5.775518e-3;	/* light travel time, in days */
				if (pass == 0) {
			    *lpd0 = lpd;
			    *psi0 = psi;
			    *rp0 = rp;
			    *rho0 = rho;
				}
	    }

#ifdef _TI_OPTIMIZED
			sincos(ll, 0, &sll, &cll);
#else
	    sll = sin(ll);
	    cll = cos(ll);
 #endif
	    if (rpd < np->n_const.c_rsn)
				*lam = atan(-1*rpd*sll/(np->n_const.c_rsn-rpd*cll))+lg+PI;
	    else
				*lam = atan(np->n_const.c_rsn*sll/(rpd-np->n_const.c_rsn*cll))+lpd;
	    range (*lam, TWOPI);
	    *bet = atan(rpd*spsi*sin(*lam-lpd)/(cpsi*np->n_const.c_rsn*sll));

	    if (op->o_data.o_e.e_mag.m_whichm == MAG_HG) {
				// the H and G parameters from the Astro. Almanac.
				double psi_t, Psi_1, Psi_2, beta;
				beta = acos((rp*rp + rho*rho - np->n_const.c_rsn*np->n_const.c_rsn)
						/ (2*rp*rho));
				psi_t = exp(log(tan(beta/2.0))*0.63);
				Psi_1 = exp(-3.33*psi_t);
				psi_t = exp(log(tan(beta/2.0))*1.22);
				Psi_2 = exp(-1.87*psi_t);
				*mag = op->o_data.o_e.e_mag.m_m1 + 5.0*log10(rp*rho)
		  	  	- 2.5*log10((1-op->o_data.o_e.e_mag.m_m2)*Psi_1
		    		+ op->o_data.o_e.e_mag.m_m2*Psi_2);
		    } else {
				// the g/k model of comets
				*mag = op->o_data.o_e.e_mag.m_m1 + 5*log10(rho)
						+ 2.5*op->o_data.o_e.e_mag.m_m2*log10(rp);
	  	  }
	    *siz = op->o_data.o_e.e_siz / rho;
	    }
	    break;

	case HYPERBOLIC: {
	    double dt, lg;
	    double nu, ea;
	    double ma, rp, lo, slo,clo, sinc,cinc;
	    double inc, psi, spsi, cpsi;
	    double y, lpd, rpd, ll, rho, sll,cll;
	    double om;		/* arg of perihelion */
	    double Om;		/* long of ascending node. */
	    double e;
	    double a, n;	/* semi-major axis, mean daily motion */
	    int pass;

	    dt = 0.;
	    lg = np->n_const.c_lsn + PI;
	    e = op->o_data.o_h.h_e;
	    a = op->o_data.o_h.h_qp/(e - 1.0);
	    n = .98563/sqrt(a*a*a);

	    for (pass = 0; pass < 2; pass++) {
				reduce_elements (op->o_data.o_h.h_epoch, np->n_mjd-dt,
						degrad(op->o_data.o_h.h_inc), degrad (op->o_data.o_h.h_om),
						degrad (op->o_data.o_h.h_Om), &inc, &om, &Om);

				ma = degrad ((np->n_mjd - op->o_data.o_h.h_ep - dt) * n);
				anomaly (ma, e, &nu, &ea);
				rp = a * (e*e-1.0) / (1.0+e*cos(nu));
				lo = nu + om;
#ifdef _TI_OPTIMIZED
				sincos(lo, 0, &slo, &clo);
				sincos(inc, 0, &sinc, &cinc);
#else
				slo = sin(lo);
				clo = cos(lo);
				sinc = sin(inc);
				cinc = cos(inc);
#endif
				spsi = slo*sinc;
				y = slo*cinc;
				psi = asin(spsi);
				lpd = atan(y/clo)+Om;
				if (clo<0) lpd += PI;
				range (lpd, TWOPI);
				cpsi = cos(psi);
				rpd = rp*cpsi;
				ll = lpd-lg;
				rho = sqrt(np->n_const.c_rsn*np->n_const.c_rsn
					+rp*rp-2*np->n_const.c_rsn*rp*cpsi*cos(ll));
				dt = rho*5.775518e-3;	/* light travel time, in days */
				if (pass == 0) {
			    *lpd0 = lpd;
			    *psi0 = psi;
			    *rp0 = rp;
			    *rho0 = rho;
				}
	    }

#ifdef _TI_OPTIMIZED
			sincos(ll, 0, &sll, &cll);
#else
			sll = sin(ll);
			cll = cos(ll);
#endif
	    if (rpd < np->n_const.c_rsn)
				*lam = atan(-1*rpd*sll/(np->n_const.c_rsn-rpd*cll))+lg+PI;
	    else
				*lam = atan(np->n_const.c_rsn*sll/(rpd-np->n_const.c_rsn*cll))+lpd;
	    range (*lam, TWOPI);
	    *bet = atan(rpd*spsi*sin(*lam-lpd)/(cpsi*np->n_const.c_rsn*sll));

	    *mag = op->o_data.o_h.h_g + 5*log10(rho) + 2.5*op->o_data.o_h.h_k*log10(rp);
	    *siz = op->o_data.o_h.h_siz / rho;
	  }
	  break;

	case PARABOLIC: {
	  double inc, om, Om;
	  double lpd, psi, rp, rho;
	  double dt;
	  int pass;

	  /* two passes to correct lam and bet for light travel time. */
	  dt = 0.;
	  for (pass = 0; pass < 2; pass++) {
			reduce_elements (op->o_data.o_p.p_epoch, np->n_mjd-dt,
					degrad(op->o_data.o_p.p_inc), degrad(op->o_data.o_p.p_om),
					degrad(op->o_data.o_p.p_Om), &inc, &om, &Om);
			comet (np->n_mjd-dt, op->o_data.o_p.p_ep, inc, om, op->o_data.o_p.p_qp,
					Om, np->n_const.c_lsn, np->n_const.c_rsn, &lpd, &psi, &rp, &rho, lam, bet);
				if (pass == 0) {
		    	*lpd0 = lpd;
		    	*psi0 = psi;
		    	*rp0 = rp;
		    	*rho0 = rho;
				}
			dt = rho*5.775518e-3;	/* au to light-days */
	    }
	    *mag = op->o_data.o_p.p_g + 5*log10(rho) + 2.5*op->o_data.o_p.p_k*log10(rp);
	    *siz = op->o_data.o_p.p_siz / rho;
	  }
	  break;

	default:
	  break;
	}
}
