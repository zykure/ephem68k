/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	comet.c
 * Desc:	Special handling for comets.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *
 * This file was taken from original ephem source code
 * Copyright (c) 1990,1991,1992 by Elwood Charles Downey
 * (Code cleanup by Jonathan Woithe, March 2000)
 *
 * It has been modified to run properly on TI calculators.
 */


#ifdef __TIGCC_ENV__
#include <timath.h>
#else
#include <math.h>
#endif
#include "astro.h"
#include "ephem.h"

#ifdef _TI_OPTIMIZED
#define ERRLMT	0.001			/* speed up! */
#else
#define	ERRLMT	0.0001
#endif


/* given a modified Julian date, mjd, and a set of heliocentric parabolic
 * orbital elements referred to the epoch of date (mjd):
 *   ep:   epoch of perihelion,
 *   inc:  inclination,
 *   ap:   argument of perihelion (equals the longitude of perihelion minus the
 *	   longitude of ascending node)
 *   qp:   perihelion distance,
 *   om:   longitude of ascending node;
 * find:
 *   lpd:  heliocentric longitude,
 *   psi:  heliocentric latitude,
 *   rp:   distance from the sun to the planet,
 *   rho:  distance from the Earth to the planet,
 *   lam:  geocentric ecliptic longitude,
 *   bet:  geocentric ecliptic latitude,
 *         none are corrected for light time, ie, they are the true values for
 *	   the given instant.
 *
 * all angles are in radians, all distances in AU.
 * mutual perturbation corrections with other solar system objects are not
 * applied. corrections for nutation and abberation must be made by the caller.
 * The RA and DEC calculated from the fully-corrected ecliptic coordinates are
 * then the apparent geocentric coordinates. Further corrections can be made,
 * if required, for atmospheric refraction and geocentric parallax.
 */
void comet (double mjd, double ep, double inc, double ap, double qp, double om, double lsn, \
	double rsn, double *lpd, double *psi, double *rp, double *rho, double *lam, double *bet)
{
	double w, s, s2;
	double l, y, sl,cl, sinc,cinc;
	double spsi, cpsi;
	double rd;
	double lg, re, ll;
	double cll, sll;
	double nu;

    w = ((mjd-ep)*3.649116e-02)/(qp*sqrt(qp));
    s = w/3;
	while (1) {
	    double d;
	    s2 = s*s;
	    d = (s2+3)*s-w;
	    if (fabs(d) <= ERRLMT)
		break;
	    s = ((2*s*s2)+w)/(3*(s2+1));
	}

    nu = 2*atan(s);
	*rp = qp*(1+s2);
	l = nu+ap;
	#ifdef _TI_OPTIMIZED
	sincos(l, 0, &sl, &cl);
	sincos(inc, 0, &sinc, &cinc);
	#else
  sl = sin(l);
	cl = cos(l);
  sinc = sin(inc);
	cinc = cos(inc);
	#endif
	spsi = sl*sinc;
  *psi = asin(spsi);
	y = sl*cinc;
  *lpd = atan(y/cl)+om;
	cpsi = cos(*psi);
  if (cl<0)
		*lpd += PI;
	range (*lpd, TWOPI);
  rd = *rp * cpsi;
	lg = lsn+PI;
  re = rsn;
	ll = *lpd - lg;
	#ifdef _TI_OPTIMIZED
	sincos(ll, 0, &sll, &cll);
	#else
	sll = sin(ll);
    cll = cos(ll);
	#endif
    *rho = sqrt((re * re)+(*rp * *rp)-(2*re*rd*cll));
    if (rd<re)
    	*lam = atan((-1*rd*sll)/(re-(rd*cll)))+lg+PI;
	else
	    *lam = atan((re*sll)/(rd-(re*cll)))+*lpd;
	range (*lam, TWOPI);
    *bet = atan((rd*spsi*sin(*lam-*lpd))/(cpsi*re*sll));
}
