/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	shared.c
 * Desc:	Some functions used both in ephem68k and aphem68k.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <tigcclib.h>
#include "astro.h"
#include "circum.h"
#include "ephem.h"
#include "shared.h"
#include "info.h"
#include "formats.h"
#include "i18n.h"


/* declared in main.c */
extern LCD_BUFFER lcdbuf;
extern Obj objects[3];
extern Now now;
extern unsigned short flags;
extern const char *obj_names[NOBJ];

/* DLL error messages */
const char *dllerrors[6] = {
	LOC_DLL_ERRORS
};

/* Restore screen and display exit message */
void bye(void)
{
	LCD_restore(lcdbuf);
	DrawLine(0, 121, 239, 121, A_NORMAL);
	ST_helpMsg(INFO_MESSAGE);
}

/* Write configuration to file */
short cfg_write(void)
{
	short i;
	FILE *f = fopen("ephemcfg", "wb");
	if (!f)	return -1;

	/* Save data to file */
	if ( fprintf(f, "%s,%hu,%.8f,%.8f,%.8f,%.8f,%.8f,%.8f,%.8f,%.8f",
			COMMENT_VERSION_SHORT, flags, now.n_mjd + MJD,
			raddeg(now.n_lat), raddeg(now.n_lng), -now.n_tz,
			now.n_height * REARTH, now.n_temp, now.n_pressure, now.n_epoch ) == EOF)
		return 1;

	for (i = 0; i < NOBJ-OBJ1; i++)
    	if (fprintf(f, ",%s", objects[i].o_type != OBJ_NONE ? objectname(i+OBJ1) : "") == EOF)
    		return 1;

	/* Add custom file extension */
	if (fputc(0, f) == EOF) return 1;
	if (fputs("conf", f) == EOF) return 1;
	if (fputc(0, f) == EOF) return 1;
	if (fputc(OTH_TAG, f) == EOF) return 1;

	fclose (f);

	return 0;
}

/* Read configuration from file, if it exists */
short cfg_read(void)
{

#define ARGS 14

	unsigned char tmp[128], *s;
	unsigned char *args[ARGS], c;
	short i, ac = 0;

	FILE *f = fopen("ephemcfg", "rb");
	if (!f)	return -1;

	if (feof (f))
		return 1;

	fgets (tmp, 128, f);
	s = tmp;

	fclose (f);
	
	/* Split and convert file contents */
	args[ac] = s;
	do {
	    c = *s++;
	    if (c == ',' || c == '\n') {
			s[-1] = '\0';
			args[++ac] = s;
	    } else if (ac > 4) {
			if (c == '-')
				s[-1] = '\xAD';
			else if ((c | 32) == 'e')
				s[-1] = '\x95';
		}
	} while (c);

	if (ac != ARGS-1)
		return 1;

	if (strncmp(args[0], COMMENT_VERSION_SHORT, 16) != 0) return 2;

	flags = (unsigned short) atoi(args[1]);

	/* Read values */
	now.n_mjd = 	atof(args[2]) - MJD;
	now.n_lat = 	degrad(atof(args[3]));
	now.n_lng = 	degrad(atof(args[4]));
	now.n_tz = 		-atof(args[5]);
	now.n_height = 	atof(args[6]) / REARTH;
	now.n_temp = 	atof(args[7]);
	now.n_pressure = atof(args[8]);
	now.n_epoch = 	atof(args[9]);

	/* Verify user-object names */
	for (i = 0; i < NOBJ-OBJ1; i++)
		if (!db_read (args[i+10], 0, &objects[i]))
			DlgMessage(LOC_ERR, LOC_ERR_OBJECT, BT_OK, BT_NONE);

	return 0;
}

/* Search database for object and load it if found */
short db_read(char ident[], short test, Obj *op)
{
	/* test=1 means search database, but don't load anything
	 * (used to verify object name) */

	unsigned char tmp[128], name[MAXNM], *s;
	short found = 0;

	/* DLL takes too much RAM, so we have to unload it first */
	UnloadDLL ();

	FILE *f = fopen("ephemdb", "rb");
	if (!f)	{
		DlgMessage(LOC_ERR, LOC_ERR_DB, BT_OK, BT_NONE);
		return 0;
	}

	/* Empty identifier? */
	if (strlen(ident) == 0) {
		op->o_type = OBJ_NONE;
		return -1;
	}

	/* Convert to lowercase */
	s = ident;
	do {
		if (isupper(*s))
			*s = tolower(*s);
	} while (*s++);

	/* Search every single line of the database */
	while (!feof(f) && !found) {
		fgets(tmp, 128, f);
		s = tmp;

		if (!isalpha(*s))
			continue;

		do {
	    	if (*s == ',')
				*s = '\0';
		} while (*s++);

		strncpy(name, tmp, MAXNM);

		s = tmp;
		do {
			if (isupper(*s))
				*s = tolower(*s);
		} while (*s++);

		if ((found = !strncmp(ident, tmp, MAXNM)))
			break;
	}

	fclose(f);

	/* Reload the DLL we've unloaded before */
	load_dll();

	/* Load object into RAM if found */
	if (!test && found)
		found &= obj_load(name, s, op);

	return found;
}

/* Load object into RAM and update descriptor */
short obj_load(char name[], char s[], Obj *op)
{
	char *args[MAXARGS], c;
	short ac = 0, err = 0;

	/* Split and convert object data */
	args[0] = s++;
	do {
	  c = *s++;
		if (c == '-')
			s[-1] = '\xAD';
		else if ((c | 32) == 'e')
			s[-1] = '\x95';
	    else if (c == ',' || c == '\0' || c == '\n') {
			s[-1] = '\0';
			args[++ac] = s;
	    }
	} while (c);

	op->o_class = args[0][1];

	/* Every object type got another function to update the object descriptor */
	switch (args[0][0]) {
		case 'f':
    	if (ac != 8) {
				err = 1;
			} else {
				op->o_type = FIXED;
				strncpy(op->o_data.o_f.f_name, name, MAXNM);
				op->o_data.o_f.f_name[MAXNM] = '\0';
				obj_setfixed(&op->o_data.o_f, args);
			}
			break;

		case 'e':
	    if (ac != 16) {
				err = 1;
			} else {
				op->o_type = ELLIPTICAL;
				strncpy(op->o_data.o_e.e_name, name, MAXNM);
				op->o_data.o_e.e_name[MAXNM] = '\0';
				obj_setelliptical(&op->o_data.o_e, args);
			}
			break;

		case 'h':
 	    if (ac != 16) {
				err = 1;
			} else {
				op->o_type = HYPERBOLIC;
				strncpy(op->o_data.o_h.h_name, name, MAXNM);
				op->o_data.o_h.h_name[MAXNM] = '\0';
				obj_sethyperbolic(&op->o_data.o_h, args);
			}
			break;

		case 'p':
    	if (ac != 16) {
				err = 1;
			} else {
				op->o_type = PARABOLIC;
				strncpy(op->o_data.o_p.p_name, name, MAXNM);
				op->o_data.o_p.p_name[MAXNM] = '\0';
				obj_setparabolic(&op->o_data.o_p, args);
			}
			break;

		default:
			err = 1;
	}

	if (err)
		DlgMessage(LOC_ERR, LOC_ERR_DBREAD, BT_OK, BT_NONE);

	return !err;
}

/**** NOT USED FOR NOW  --  this function does not work properly! ****/
/* Create a list of all objects contained in the database */
/*
short db_getnames(char* names)
{
	unsigned char tmp[128], *s;
	unsigned short n = 0;

	names = malloc(MAXNM);

	FILE *f = fopen("ephemdb", "rb");
	if (!f)	{
		DlgMessage("ERROR", "Could not open database!", BT_OK, BT_NONE);
		return 0;
	}

	while (!feof(f)) {
		if (!names)	{
			DlgMessage("ERROR", "Not enough memory!", BT_OK, BT_NONE);
			break;
		}

		fgets (tmp, 128, f);
		s = tmp;

		if (!isalpha (*s))
			continue;

		do {
	    	if (*s == ',')
				*s = '\0';
		} while (*s++);

		strncpy(&names[MAXNM*n], tmp, MAXNM);
		names[MAXNM*n+15] = '\0';
		n++;
		names = realloc(names, MAXNM*n);
	}

	fclose(f);
	return n;
}
*/

/* Update descriptor for FIXED type object */
void obj_setfixed(ObjF *op, char *args[MAXARGS])
{
	short d, h, m, s;

	f_dec_sexsign(raddeg(op->f_dec), &h, &m, &s);
	f_sscansex((char *) args[1], &h, &m, &s);
	sex_dec(h, m, s, (double*)&op->f_ra);
	op->f_ra = hrrad(op->f_ra);			// right ascension

	f_dec_sexsign(raddeg(op->f_dec), &d, &m, &s);
	f_sscansex((char *) args[2], &d, &m, &s);
	sex_dec(d, m, s, (double*)&op->f_dec);
	op->f_dec = degrad(op->f_dec);		// declination

	op->f_mag = atof(args[3]);			// apparent magnitude

	crack_year((char *) args[4], (double*)&op->f_epoch);	// reference epoch

	op->f_dist =						// earth distance
		(args[5][0] == 'M') ? 1000000.0 * atof(args[5]+1) :
			((args[5][0] == 'k') ? 1000.0 * atof(args[5]+1) :
			atof(args[5]));

	strncpy(op->f_class, args[6], 15);	// other info
	s = strlen(op->f_class);
	op->f_class[s-1] = 0;
	if (op->f_class[s-3] == '\xAD')
		op->f_class[s-3] = '-';
}

/* Update descriptor for ELLIPTICAL type object */
void obj_setelliptical(ObjE *op, char *args[MAXARGS])
{
	op->e_inc = atof(args[1]);		// inclination
	op->e_Om = atof(args[2]);		// longitude of ascending node
	op->e_om = atof(args[3]);		// argument of perihelion
	op->e_a = atof(args[4]);		// mean distance
	op->e_n = atof(args[5]);		// daily motion
	op->e_e = atof(args[6]);		// eccentricity
	op->e_M = atof(args[7]);		// mean anomaly

	crack_year((char *) args[8], (double*)&op->e_cepoch);	// epoch date
	crack_year((char *) args[9], (double*)&op->e_epoch);	// equinox date

	op->e_mag.m_whichm = 			// magnitude (g/k or H/G model)
		(args[10][0] == 'H') ? MAG_HG : MAG_gk;
	op->e_mag.m_m1 = atof(args[10]+1);
	op->e_mag.m_m2 = atof(args[11]);
	op->e_siz = atof(args[12]);		// angular size
	op->e_radius = atof(args[13]);	//radius
	op->e_daylen = 		 			// day length
		(args[14][0] == 'd') ? 24 * atof (args[14]+1) :
		atof(args[14]);
}

/* Update descriptor for HYPERBOLICAL type object */
void obj_sethyperbolic(ObjH *op, char *args[MAXARGS])
{
	op->h_ep = atof(args[1]);		// epoch of perihelion
	op->h_inc = atof(args[2]);		// inclination
	op->h_Om = atof(args[3]);		// longitude of ascending node
	op->h_om = atof(args[4]);		// argument of perihelion
	op->h_e = atof(args[5]);		// eccentricity
	op->h_qp = atof(args[6]);		// perihelion distance

	crack_year((char *) args[7], (double*)&op->h_epoch);	// equinox date

	op->h_g = atof(args[8]);		// magnitude
	op->h_k = atof(args[9]);
	op->h_siz = atof(args[10]);		// angular size
	op->h_radius = atof(args[11]);	// radius
	op->h_daylen = 		 			// day length
		(args[12][0] == 'd') ? 24 * atof(args[12]+1) :
		atof(args[12]);
}

/* Update descriptor for PARABOLICAL type object */
void obj_setparabolic(ObjP *op, char *args[MAXARGS])
{
	op->p_ep = atof(args[1]);		// epoch of perihelion
	op->p_inc = atof(args[2]);		// inclination
	op->p_Om = atof(args[3]);		// longitude of ascending node
	op->p_qp = atof(args[4]);		// perihelion distance
	op->p_om = atof(args[5]);		// argument of perihelion

	crack_year((char *) args[6], (double*)&op->p_epoch);	// equinox date

	op->p_g = atof(args[7]);		// magnitude
	op->p_k = atof(args[8]);
	op->p_siz = atof(args[9]);		// angular size
	op->p_radius = atof(args[10]);	// radius
	op->p_daylen = 		 			// day length
		(args[11][0] == 'd') ? 24 * atof (&args[11][1]) :
		atof(args[11]);
}

/* Convert string like "2006.0123" or "23.01.2006" to MJD */
void crack_year(char *bp, double *p)
{
	/* Decimal format "yyyy.nnnn"? */
	if (decimal_year(bp)) {
	    double y = atof(bp);
	    year_mjd(y, p);
	}
	/* Standard format "yyyy.mm.dd"? */
	else {
	    short d, m, y;
	    float dd;
	    mjd_cal(*p, &m, &dd, &y);
	    d = (short) dd;
	    f_sscandate(bp, &m, &d, &y);
	    cal_mjd(m, d, y, p);
	}
}

/* Get name of object */
const char *objectname(short p)
{
	Obj *op;

	/* User-objects descriptor contains name */
	if (p >= OBJ1 && p <= OBJ4) {
		op = &objects[p-OBJ1];

		switch (op->o_type) {
			case FIXED:
				return op->o_data.o_f.f_name;
				break;
			case ELLIPTICAL:
				return op->o_data.o_e.e_name;
				break;
			case HYPERBOLIC:
				return op->o_data.o_h.h_name;
				break;
			case PARABOLIC:
				return op->o_data.o_p.p_name;
				break;
		}
	}

	/* Other objects names */
	return obj_names[p];
}


/* Load DLL, display error message and exit if it fails (version, memory etc.) */
void load_dll(void) {
	short status = LoadDLL(DLL_1_IDENT, DLL_1_ID, DLL_1_VERSION);
	if (status != DLL_OK) {
		char tmp[32];
		sprintf(tmp, LOC_ERR_DLL, status, dllerrors[status-1]);
		DlgMessage(LOC_ERR, tmp, BT_OK, BT_NONE);
		bye();
		exit(1);
	}
}

/* handy function to return the next planet in the order in which they are
 * displayed in the lower half of the screen.
 * input is a given planet, return is the next planet.
 * if input is not legal, then first planet is returned; when input is the
 * last planet, then -1 is returned.
 */
short nxtbody(short p, short d)
{
	static const short nxtpl[NOBJ] = {
		VENUS, MARS, JUPITER, SATURN, URANUS, NEPTUNE,
		OBJ1, MOON, MERCURY, OBJ2, OBJ3, OBJ4, -1
	};
	static const short prvpl[NOBJ] = {
		MOON, MERCURY, VENUS, MARS, JUPITER, SATURN,
		URANUS, -1, SUN, NEPTUNE, OBJ1, OBJ2, OBJ3
	};

	if (p < MERCURY || p >= NOBJ)
		return (d>0 ? SUN : OBJ4);

  return (d>0 ? nxtpl[p] : prvpl[p]);
}
