/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	mainmenu.c
 * Desc:	Show the main menu (date and time info, calendar...).
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include <tigcclib.h>
#include "astro.h"
#include "circum.h"
#include "ephem.h"
#include "formats.h"
#include "screen.h"
#include "i18n.h"


void mm_calendar(Now *, short);
void mm_nfmoon(double, double, short, short);


/* Labels have only to be redrawn if we switch from another menu */
void mm_labels(void)
{
	f_string(R_LD,		C_LD,		LOC_MM_DATE);
	f_string(R_LT,		C_LT,		LOC_MM_TIME);
	f_string(R_UD,		C_UD,		LOC_MM_UDATE);
	f_string(R_UT,		C_UT,		LOC_MM_UTIME);
	f_string(R_JD,		C_JD,		LOC_MM_JD);

	f_string(R_GST,		C_GST,		LOC_MM_GST);
	f_string(R_LST,		C_LST,		LOC_MM_LST);
	f_string(R_DAWN,	C_DAWN,		LOC_MM_DAWN);
	f_string(R_DUSK,	C_DUSK,		LOC_MM_DUSK);
	f_string(R_NITE,	C_NITE,		LOC_MM_NITELN);

	f_string(R_LON,		C_LON,		LOC_MM_LON);
	f_string(R_LAT,		C_LAT,		LOC_MM_LAT);
	f_string(R_HEIGHT,C_HEIGHT,		LOC_MM_HEIGHT);
	f_string(R_TEMP,	C_TEMP,		LOC_MM_TEMP);
	f_string(R_PRES,	C_PRES,		LOC_MM_PRESS);
	f_string(R_EPOCH,	C_EPOCH,	LOC_MM_EPOCH);

	DrawLine(MAIN_LINEV, A_NORMAL);
	DrawLine(MAIN_LINEH, A_NORMAL);
}

/* Print all the time/date/where related stuff */
void mm_now(Now *np, short all)
{
	double lmjd = np->n_mjd - np->n_tz/24.0;

	f_time(R_LT, C_LTV, mjd_hr(lmjd));
	f_date(R_LD, C_LDV, lmjd);

	f_time(R_UT, C_UTV, mjd_hr(np->n_mjd));
	f_date(R_UD, C_UDV, np->n_mjd);

	f_double(R_JD, C_JDV, "%#10.2f", np->n_mjd + MJD);

	f_time(R_GST, C_GSTV, np->n_const.c_lst - radhr(np->n_lng));
	f_time(R_LST, C_LSTV, np->n_const.c_lst);

	if (all) {
	    f_gangle(R_LON, C_LONV, np->n_lng);	// + east
	    f_gangle(R_LAT, C_LATV, np->n_lat);

			f_double(R_HEIGHT, C_HEIGHTV, "%7.1f m", np->n_height * REARTH);

			f_double(R_TEMP, C_TEMPV, "%6.1f " "\xB0" "C", np->n_temp);

			f_double(R_PRES, C_PRESV, "%4.0f hPa", np->n_pressure);

	    if (np->n_epoch == EOD) {
				f_string(R_EPOCH, C_EPOCHV, LOC_EOD);
	    } else {
	    	double tmp;
				mjd_year(np->n_epoch, &tmp);
				f_double(R_EPOCH, C_EPOCHV, "%8.1f", tmp);
	    }
	}

	/* Print the calendar for local day */
	mm_calendar(np, all > 1);
}

/* Display twilight times (Dawn, Dusk, NiteLen, DayLen) */
void mm_twilight(Now *np, short force, short mode)
{
	double dusk, dawn;
	double tmp;
	short status;

	if (!twilight_cir(np, &dawn, &dusk, &status, mode) && !force)
	    return;

	if (status != 0) {
	    f_string(R_DAWN, C_DAWNV, 	"-----");
	    f_string(R_DUSK, C_DUSKV, 	"-----");
	    f_string(R_NITE, C_NITEV, 	"-----");
	    return;
	}

	f_mtime(R_DAWN, C_DAWNV, dawn);
	f_mtime(R_DUSK, C_DUSKV, dusk);
	tmp = dawn - dusk; range(tmp, 24.0);
	f_mtime(R_NITE, C_NITEV, tmp);
}


/* Print the calendar, including new/full moons if enabled */
void mm_calendar(Now *np, short force)
{
	static const char *mnames[] = {
		LOC_MONTH_NAMES
	};
	static int last_m = -100, last_y = -100;
	char str[64];
	short m, y;
	double d;
	short f, nd;
	short i, r, c;
	double jd0;

	/* Get local m/d/y. Do nothing if still same month and not forced. */
	mjd_cal(np->n_mjd-np->n_tz/24.0, &m, &d, &y);
	if (m == last_m && y == last_y && !force)
	  return;

	last_m = m;
	last_y = y;

	/* Find day of week of first day of month */
	cal_mjd(m, 1.0, y, &jd0);
	mjd_dow(jd0, &f);
	if (f < 0) {
	  /* Can't figure it out - too hard before Gregorian */
	  int i;
	  for (i = 8; --i >= 0; )
			f_string(R_CAL+i, C_CAL, "                    ");
	  return;
	}

	/* Print header */
	f_blanks(R_CAL, C_CAL, 20);
	sprintf(str, "%s %4d", mnames[m-1], y);
	f_string(R_CAL, C_CAL + (20 - (strlen(mnames[m-1]) + 5))/2, str);
	f_string(R_CAL+1, C_CAL, LOC_DAY_ABBRV);

	/* Find number of days in this month */
	mjd_dpm(jd0, &nd);

	/* Print the calendar */
	for (r = 0; r < 6; r++) {
	    char row[7*3+1], *rp = row;
	    for (c = 0; c < 7; c++) {
			i = r*7+c;
			if (i < f || i >= f + nd)
			    sprintf(rp, "   ");
			else
			    sprintf(rp, "%2d ", i-f+1);
			rp += 3;
	    }
	    f_string(R_CAL+2+r, C_CAL, row);
	}

	/* Over print the new and full moons for this month
	 * Use two dates to make sure all dates are computed */
	mm_nfmoon(jd0-3, np->n_tz, m, f);
	mm_nfmoon(jd0+15, np->n_tz, m, f);
}

/* Compute new and full moon dates */
void mm_nfmoon(double jd, double tzone, short m, short f)
{
	double dm;
	short mm, ym;
	double jdn, jdf;
	short di;

	moonnf(jd, &jdn, &jdf);
	mjd_cal(jdn-tzone/24.0, &mm, &dm, &ym);
	if (m == mm) {
	    di = dm + f - 1;
	    f_string(R_CAL+2+di/7, C_CAL+3*(di%7), LOC_CAL_NM);
	}
	mjd_cal(jdf-tzone/24.0, &mm, &dm, &ym);
	if (m == mm) {
	    di = dm + f - 1;
	    f_string(R_CAL+2+di/7, C_CAL+3*(di%7), LOC_CAL_FM);
	}
}
