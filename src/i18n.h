/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	i18n.h
 * Desc:	ephem68k i18n-package
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef _I18N_H
#define _I18N_H

/*
 * The ephem68k i18n-package adds support for l10n into other languages than english.
 * Simply define your (included) target language at the compiler command line or
 * inside this file (below). If nothing is specified, english l13n will be used.
 * Currently supported languages include: english, german.
 */

/* Uncomment to use german l10n */
//#define GERMAN

#endif


#ifdef GERMAN
#include "l10n_de.h"
#endif

#ifndef _LOCAL_
#include "l10n_en.h"
#endif
