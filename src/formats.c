/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	formats.c
 * Desc:	Basic formatting routines used to print the data in various
 *			formats on the screen.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include "io.h"
#include "astro.h"
#include "ephem.h"
#include "formats.h"


/* draw n blanks at the given cursor position.  */
void f_blanks(short r, short c, short n)
{
	c_pos(r, c);
	while (--n >= 0)
	    putchar(' ');
}

/* print the given value, v, in "sexadecimal" format at [r,c]
 * ie, in the form A:m.P, where A is a digits wide, P is p digits.
 * if p == 0, then no decimal point either.
 */
void f_sexad(short r, short c, short a, short p, short mod, double v)
{
	char astr[32], str[32];
	long dec;
	double frac;
	char visneg;

	if (v >= 0.0)
	    visneg = 0;
	else {
	    if (v <= -0.5/60.0*pow(10.0,-1.0*p)) {
			v = -v;
			visneg = 1;
	    } else {
			/* don't show as negative if less than the precision showing */
			v = 0.0;
			visneg = 0;
	    }
	}

	dec = (long)v;
	frac = (v - dec)*60.0;
	sprintf(str, "59.%.*s5", p, "999999999");
	if (frac >= atof (str)) {
	    dec += 1;
	    frac = 0.0;
	}
	dec %= mod;
	if (dec == 0 && visneg)
	    strcpy(str, "-0");
	else
	    sprintf(str, "%ld", visneg ? -dec : dec);

	if (p == 0)
	    sprintf(astr, "%*s:%02d", a, str, (short)(frac+0.5));
	else
	    sprintf(astr, "%*s:%#0*.*f", a, str, p+3, p, frac);

	f_string(r, c, astr);
}

/* print the given value, t, in sexagesimal format at [r,c]
 * ie, in the form T:mm, where T is nd digits wide.
 * N.B. we assume nd >= 2.
 */
void f_sexag(short r, short c, short nd, double t)
{
	char tstr[32];
	short h, m, s;
	short tisneg;

	dec_sex(t, &h, &m, &s, &tisneg);
	if (h == 0 && tisneg)
	    sprintf(tstr, "%*s-0:%02d:%02d", nd-2, "", m, s);
	else
	    sprintf(tstr, "%*d:%02d:%02d", nd, tisneg ? -h : h, m, s);

	f_string(r, c, tstr);
}

/* print the given modified Julian date, jd, as the starting date at [r,c]
 * in the form yyyy.mm.dd.
 */
void f_date(short r, short c, double jd)
{
	char dstr[32];
	short m, y;
	double d;

	mjd_cal(jd, &m, &d, &y);
	sprintf(dstr, "%-4d.%02d.%02d", y, m, (short) floor(d));

	f_string(r, c, dstr);
}

/* print the given double as a rounded int, with the given format.
 * this is used to plot full precision, but display far less.
 * N.B. caller beware that we really do expect fmt to refer to an int, not
 *   a long for example. also beware of range that implies.
 */
void f_int(short r, short c, const char *fmt, double f)
{
	char str[80];
	short i;

	i = (f < 0) ? (int)(f-0.5) : (int)(f+0.5);
	sprintf(str, fmt, i);

	f_string(r, c, str);
}

void f_char(short r, short c, char chr)
{
	c_pos(r, c);
	putchar(chr);
}

void f_string(short r, short c, const char *str)
{
	c_pos(r, c);
	puts(str);
}

void f_double(short r, short c, const char *fmt, double f)
{
	char str[80];
	sprintf(str, fmt, f);

	f_string(r, c, str);
}

/* clear from [r,c] to end of line, if we are drawing now. */
void f_eol(short r, short c)
{
    f_blanks(r, c, NC-c);
}

/* crack a line of the form X?X?X into its components,
 *   where X is an integer and ? can be any character except '0-9' or '-',
 *   such as ':' or '/'.
 * only change those fields that are specified:
 *   eg:  ::10	only changes *s
 *        10    only changes *d
 *        10:0  changes *d and *m
 * if see '-' anywhere, first non-zero component will be made negative.
 */
void f_sscansex(char *bp, short *d, short *m, short *s)
{
	char c;
	short *p = d;
	short *nonzp = 0;
	short sawneg = 0;
	short innum = 0;
	*d = *m = *s = 0;

	if ((unsigned char)*bp == 0xAD)
		*bp = '-';

	while ((c = *bp++))
	    if (isdigit(c)) {
			if (!innum) {
			    *p = 0;
			    innum = 1;
			}
			*p = *p*10 + (c - '0');
			if (*p && !nonzp)
			    nonzp = p;
	    } else if (c == '-') {
			sawneg = 1;
	    } else if (c != ' ') {
			/* advance to next component */
			p = (p == d) ? m : s;
			innum = 0;
	    }

	if (sawneg && nonzp)
	    *nonzp = -*nonzp;
}

/* crack a floating date string, bp, of the form d.m.y, into its components.
 * leave any component unspecified unchanged.
 * actually, the slashes may be anything but digits or a decimal point.
 * this is functionally the same as f_sscansex() exept we allow for
 *   the day portion to be real, and we don't handle negative numbers.
 *   maybe someday we could make a combined one and use it everywhere.
 */
void f_sscandate(char *bp, short *m, short *d, short *y)
{
	char *bp0, c;
	*d = *m = *y =0;

	bp0 = bp;
	while ((c = *bp++) && isdigit(c))
	    continue;
	if (bp > bp0+1)
	    *y = atoi (bp0);
	if (c == '\0')
	    return;

	bp0 = bp;
	while ((c = *bp++) && isdigit(c))
	    continue;
	if (bp > bp0+1)
	    *m = atoi (bp0);
	if (c == '\0')
	    return;

	bp0 = bp;
	while ((c = *bp++))
	    continue;
	if (bp > bp0+1)
	    *d = atoi (bp0);
}

/* just like dec_sex() but makes the first non-zero element negative if
 * x is negative (instead of returning a sign flag).
 */
void f_dec_sexsign(double x, short *h, short *m, short *s)
{
	short n;
	dec_sex(x, h, m, s, &n);
	if (n) {
	    if (*h)
			*h = -*h;
	    else if (*m)
			*m = -*m;
	    else
			*s = -*s;
	}
}

/* return 1 if bp looks like a decimal year; else 0.
 * any number greater than 12 or less than 0 is assumed to be a year, or any
 * string with exactly one decimal point, an optional minus sign, and nothing
 * else but digits.
 */
short decimal_year(char *bp)
{
	char c;
	int ndig = 0, ndp = 0, nneg = 0, nchar = 0;
	double y = atof(bp);	/* does return NAN on parse error (NOT '0'!) */

	while ((c = *bp++)) {
	    nchar++;
	    if (isdigit(c))
			ndig++;
	    else if (c == '.')
			ndp++;
	    else if (c == '-')
			nneg++;
	}

	return (y == NAN && (y > 12 || y < 0
		    || (ndp == 2 && nneg <= 1 && nchar == ndig+ndp+nneg)));
}
