/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	l10n_en.h
 * Desc:	Part of the ephem68k i18n-package -- english locals (default)
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef _L10N_EN_H
#define _L10N_EN_H

/* This prevents using more than one l10n */
#define _LOCAL_


/* Settings editor help - var. width */
#define LOC_SETTINGS_HELP	\
				"Set julian date (MJD.nn)", \
				"Set local time (HH:MM:SS)", \
				"Set local date (YYYY.MM.DD or YYYY.nn)", \
				"Set timezone (hours before UT)", \
				"Set geographic longitude (degrees east)", \
				"Set geographic latitude (degrees north))", \
				"Set height (meters above NN)", \
				"Set current temperature (" "\xB0" "C)", \
				"Set current atmosph. pressure (hPa)", \
				"Set target epoch ('EOD' for epoch of date)", \
				"Other options (Press \x18 to toggle)"

/* Flags editor help - var. width */
#define LOC_FLAGS_HELP		\
				"Compute and show twilight", \
				"Use adaptive horizon for rise/set times", \
				"Use civil instead of astronomical twilight"

/* Flag names */
#define LOC_FLAGS			'e','v','m','j','s','n','u','S','M','A','B','C','D','T','H','C'

/* DLL error messages - max. 20 chars */
#define LOC_DLL_ERRORS		\
				"Internal",				/* #1 NOTINGHOSTSPACE */	\
				"File not found",		/* #2 NOTFOUND*/			\
				"Internal",				/* #3 LOCKFAILED */ 		\
				"Not enough memory",	/* #4 OUTOFMEM */			\
				"Internal",				/* #5 ALREADYLOADED */		\
				"Wrong version"			/* #6 WRONGVERSION	*/

/* Calendar month names - max. 8 chars */
#define LOC_MONTH_NAMES		\
			    "January", "February", "March", "April", "May", "June", \
	    		"July", "August", "September", "October", "November", "December"

/* Object names - max. 10 chars */
#define LOC_OBJECT_NAMES 	\
				"Mercury", "Venus", "Mars", "Jupiter", "Saturn", "Uranus", \
				"Neptune", "Sun", "Moon", "Object A", "Object B", "Object C", \
				"Object D"

/* Calendar week days - fixed width */
#define LOC_DAY_ABBRV 		"Mo Tu We Th Fr Sa Su"

/* Cardinal points for watch screens - 1 char */
#define LOC_CARDINALS		'N','E','S','W'
#define LOC_ZENITH			'Z'

/* 'Any key' message - var. width */
#define LOC_ANYKEY			"Press any key to continue..."

/* Dialog titles - var. width */
#define LOC_ERR				"ERROR"
#define LOC_CONFIRM			"CONFIRMATION"
#define LOC_NEW_SETTINGS	"SETTINGS CHANGED"

/* Dialog title - fixed width */
#define LOC_SETTINGS		"EPHEM68K  SETTINGS"
#define LOC_HELP			"Help"

/* Error messages and confirmations - var. width */
#define LOC_ERR_CONFIG		"Error reading config file!"
#define LOC_ERR_CFGVERSION	"Config file has wrong version!\nUsing default values."
#define LOC_ERR_CFGSAVE		"Could not save settings!"
#define LOC_ERR_DB			"Could not open database!"
#define LOC_ERR_DBREAD		"Error reading database!"
#define LOC_ERR_OBJECT		"Could not find object!"
#define LOC_ERR_DLL			"Error loading DLL!\nCode #%2.2d (%s)"
#define LOC_QUIT			"Do you really want to quit?"
#define LOC_SETTINGS_SAVE	"Save changes and apply?"

/* Status info text - var. width */
#define LOC_KEY_STOP		"[Press ESC to stop]"
#define LOC_KEY_HELP		"[Press F8 for help]"

/* Status messages - var. width */
#define LOC_UPDATE_CORE		"Updating: Core..."
#define LOC_UPDATE_MAIN		"Updating: Main..."
#define LOC_UPDATE_TWIL		"Updating: Twilight..."
#define LOC_UPDATE_OBJ		"Updating: %s..."		/* Don't forget the '%s'! */

/* Main screen title - var. width */
#define LOC_MAIN			"Main"

/* EpochOfDate identifier - 8 chars */
#define LOC_EOD				"(OfDate)"

/* Settings editor items - max. 12 chars */
#define LOC_SET_JD			"Julian Date:"
#define LOC_SET_TIME		"Local Time:"
#define LOC_SET_DATE		"Local Date:"
#define LOC_SET_TZ			"Timezone:"
#define LOC_SET_LONG		"Longitude:"
#define LOC_SET_LAT			"Latitude:"
#define LOC_SET_HEIGHT		"Height:"
#define LOC_SET_TEMP		"Temperature:"
#define LOC_SET_PRESS		"Pressure:"
#define LOC_SET_EPOCH		"Epoch:"
#define LOC_SET_FLAGS		"Flags:"

/* Flags editor help - var. width */
#define LOC_SET_OBJ_HELP	"Compute and show %s" 		/* Don't forget the '%s'! */

/* NoObject identifier - max. 8 chars */
#define LOC_EDIT_NONE		"(NONE)"

/* Object editor help */
#define LOC_EDIT_KEY		"Press ENTER to set %s"		/* Don't forget the '%s'! */
#define LOC_EDIT_HELP		"Enter new name for %s (leave blank to clear)"	/* Don't forget the '%s'! */

/* Object types for info screen -  max. 14 chars*/
#define LOC_OBJ_STAR		"Star"
#define LOC_OBJ_DBLSTAR		"Double Star"
#define LOC_OBJ_VARSTAR		"Variable Star"
#define LOC_OBJ_GALAXY		"Galaxy"
#define LOC_OBJ_CLUSTER		"Cluster"
#define LOC_OBJ_OPEN		"Open Cluster"
#define LOC_OBJ_PLNEB		"Plan. Nebula"
#define LOC_OBJ_NEB			"Refl. Nebula"
#define LOC_OBJ_EMNEB		"Emiss. Nebula"
#define LOC_OBJ_REST		"Supernova Rest"
#define LOC_OBJ_MILKYWAY	"Milkyway"
#define LOC_OBJ_ASTERISM	"Asterism"
#define LOC_OBJ_ASTEROID	"Asteroid"
#define LOC_OBJ_PLANETOID	"Planetoid"
#define LOC_OBJ_COMET		"Comet"

/* Info screen items - max. 12 chars */
#define LOC_INFO_DIAM		"Diameter:"
#define LOC_INFO_DAYLEN		"Day Length:"
#define LOC_INFO_PERIOD		"Orb. Period:"
#define LOC_INFO_MAG		"App. Mag:"
#define LOC_INFO_DIST		"Distance:"
#define LOC_INFO_CLASS		"Class:"
#define LOC_INFO_HYPERB		"Hyperbolic"
#define LOC_INFO_PARAB		"Parabolic"

/* Help screen items - max. 16 chars */
#define LOC_HELP_MAIN		"F1   Main"
#define LOC_HELP_DATA1		"F2   Data (1)"
#define LOC_HELP_DATA2		"F3   Data (2)"
#define LOC_HELP_RISET		"F4   Rise/Set"
#define LOC_HELP_SKYDOME	"F5   Sky Dome"
#define LOC_HELP_HORIZON	"F6   Horizon"
#define LOC_HELP_INFO		"F7   Object Info"
#define LOC_HELP_HELP		"F8   Help"
#define LOC_HELP_TOGGLE		"ENT  Toggle item"
#define LOC_HELP_UPDATE		"SPC  Update"
#define LOC_HELP_MAPINFO	"MODE Map tooltips"
#define LOC_HELP_SELITEM	"\x13/\x14  Select item"
#define LOC_HELP_ROTATE		"\x11/\x12  Rotate view"
#define LOC_HELP_TIME		"+/-  Change Time"
#define LOC_HELP_EDIT		"CLR  Edit object"
#define LOC_HELP_SETTINGS	"APPS Settings"
#define LOC_HELP_QUIT		"ESC  Quit"
#define LOC_HELP_SLEEP		"OFF  Sleep"

/* Splash screen first rows - var. width */
#define LOC_SPLASH_VERSION	"ephem68k version " COMMENT_VERSION_STRING
#define LOC_SPLASH_COPY		"Copyright " COMMENT_STRING
#define LOC_SPLASH_COMPILE	"Compiled on " __DATE__ " with " __TIGCC_VERSION_STRING__

/* Main menu - 8 chars max */
#define LOC_MM_DATE			"Loc Date"
#define LOC_MM_TIME			"Loc Time"
#define LOC_MM_UDATE		"UTC Date"
#define LOC_MM_UTIME		"UTC Time"
#define LOC_MM_JD			"JD"
#define LOC_MM_GST			"GST"
#define LOC_MM_LST			"LST"
#define LOC_MM_DAWN			"Dawn"
#define LOC_MM_DUSK			"Dusk"
#define LOC_MM_NITELN		"NiteLn"
#define LOC_MM_LON			"Lon"
#define LOC_MM_LAT			"Lat"
#define LOC_MM_HEIGHT		"Height"
#define LOC_MM_TEMP			"Temp"
#define LOC_MM_PRESS		"Pressure"
#define LOC_MM_EPOCH		"Epoch"

/* Calendar moon dates - 2 chars */
#define LOC_CAL_NM			"NM"
#define LOC_CAL_FM			"FM"

/* Alt menu rows - 3 chars max */
#define LOC_ALT_OBJECT		"Obj"
#define LOC_ALT_SUN			"Su"
#define LOC_ALT_MOON		"Mo"
#define LOC_ALT_MERCURY		"Me"
#define LOC_ALT_VENUS		"Ve"
#define LOC_ALT_MARS		"Ma"
#define LOC_ALT_JUPITER		"Ju"
#define LOC_ALT_SATURN		"Sa"
#define LOC_ALT_URANUS		"Ur"
#define LOC_ALT_NEPTUNE		"Ne"
#define LOC_ALT_OBJ_A		" A"
#define LOC_ALT_OBJ_B		" B"
#define LOC_ALT_OBJ_C		" C"
#define LOC_ALT_OBJ_D		" D"

/* Alt menu cols - var length */
#define LOC_ALT_RA			" R.A."
#define LOC_ALT_DEC			" Dec"
#define LOC_ALT_AZ			" Az"
#define LOC_ALT_ALT			" Alt"
#define LOC_ALT_MAG			"VMag"
#define LOC_ALT_PHASE		"Phs"
#define LOC_ALT_HLON		" H-Lon"
#define LOC_ALT_HLAT		" H-Lat"
#define LOC_ALT_EDIST		"Ea-Dst"
#define LOC_ALT_SDIST		"Sn-Dst"
#define LOC_ALT_ELONG		"Elong"
#define LOC_ALT_SIZE		"  Sz"
#define LOC_ALT_RISE		"Rise"
#define LOC_ALT_TRANSIT		"Transit"
#define LOC_ALT_SET			"Set"

/* Alt menu status info - max. 12 chars */
#define LOC_ALT_ERROR		"?Error?"
#define LOC_ALT_CIRCUM		"Circumpolar"
#define LOC_ALT_NOTRANS		"No transit"
#define LOC_ALT_NEVERUP		"Never up"
#define LOC_ALT_NORISE		"No rise"
#define LOC_ALT_NOSET		"No set"

#endif

