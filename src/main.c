/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	main.c
 * Desc:	The core of ephem68k.
 *
 * Version:	ephem68k 1.1, 2008 Oct 26 (uses DLLs v1.1)
 *
 * Usage: Simply run ephem68k() on your TI calculator to get an
 * user interface.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

/*
 * IMPORTANT: ephem68k _must_ be archived to run properly (the caching system
 * uses static vars, so if you don't archive all parts of the program
 * it will re-use the old values since they aren't cleared when the app
 * is stored in flash memory - that will produce very strange results.
 */


/* Since we're using a DLL, we have to make sure the program runs in the
 * so-called 'ghost space' to run properly.
 */
#define EXECUTE_IN_GHOST_SPACE


#include <tigcclib.h>
#include "astro.h"
#include "circum.h"
#include "ephem.h"
#include "shared.h"
#include "io.h"
#include "screen.h"
#include "formats.h"
#include "info.h"
#include "i18n.h"


/* Defines for 'settings' menu */
#define S_JD		  0
#define S_TIME		1
#define S_DATE		2
#define S_TZ		  3
#define S_LON		  4
#define S_LAT 		5
#define S_HEIGHT	6
#define S_TEMP		7
#define S_PRES		8
#define S_EPOCH		9
#define S_FLAGS		10
#define S_NULL		S_JD
#define S_NUM		  (S_FLAGS+1)
#define FLAG(x)		((f & (1<<x)) ? flag_names[x] : '-')
#define FLAGP(x)	((*f & (1<<x)) ? flag_names[x] : '-')


/* flag titles (for settings editor) */
const char flag_names[16] = {
	LOC_FLAGS
};

/* object names (see astro.h) */
const char *obj_names[NOBJ] = {
	LOC_OBJECT_NAMES
};

/* all objects (must be same order as pnames) */
const short bodyrow[NOBJ] = {
	R_MERCURY, R_VENUS, R_MARS, R_JUPITER, R_SATURN, R_URANUS,
	R_NEPTUNE, R_SUN, R_MOON, R_OBJ1, R_OBJ2, R_OBJ3, R_OBJ4
};

/* tooltips for settings menu */
const char *settings_help[S_NUM] = {
	LOC_SETTINGS_HELP
};

/* tooltips for extended flags */
const char *flags_help[3] = {
	LOC_FLAGS_HELP
};

/* global vars */
LCD_BUFFER lcdbuf;
Obj objects[NOBJ-OBJ1];
Now now = NOW_NULL;
short active_dll = 0;
unsigned short flags;


/* functions only used here */
void sel_item(short, short);
void redraw_screen (short, short, short, short);
void update_statline (short, short);
short settings_dlg(void);
void edit_flags (unsigned short *);
void set_object(short);
void show_info (short);
void show_help (void);


/* Main */
void _main(void)
{
	short key,
		exec = 1,							/* mainloop runs until exec is set to false */
		active_menu = M_MAIN,	/* active menu (screen), one of the M_x defines */
		active_item = -1,			/* index of the selected item */
		select = 1,						/* set to 1 if the item highlight should be redrawn */
		redraw = 2,						/* 0 = do nothing, 1 = update, 2 = full redraw */
		show_grid = 1,				/* Enable/disable grid in watch screens */
		watch_rotation = 0;		/* current rotation of watch screen (degrees) */

	/* Initialize variables with some default values in case there's no config file */
	flags = F_SUN | F_MOON | F_MERCURY | F_VENUS | F_MARS |
		F_JUPITER | F_SATURN;

	now.n_mjd		= 2453652.5 - MJD;	// 01.01.2000, 00:00:00 UT

	objects[0].o_type = OBJ_NONE;
	objects[1].o_type = OBJ_NONE;
	objects[2].o_type = OBJ_NONE;

	/* We got no DLL yet */
	active_dll = 0;

	LCD_save(lcdbuf);

	/* Hide those nasty status line symbols */
	ST_busy(ST_NORMAL);

	/* Display credits ;) */
	splash();
	ST_helpMsg(LOC_ANYKEY);
	wait();
	if (ngetchx() == K_QUIT) {
  		bye();
		return;
	}

	FontSetSys(F_6x8);

	/* Try to read the config file, if one exists */
	switch (cfg_read()) {
		case 1:
			DlgMessage(LOC_ERR, LOC_ERR_CONFIG, BT_OK, BT_NONE);
			break;
		case 2:
			DlgMessage(LOC_ERR, LOC_ERR_CFGVERSION, \
					BT_OK, BT_NONE);
			break;
	}

	/* Select the first object */
	active_item = nxtbody(-1,1);

	/* Try to load the right DLL (version number is hardcoded and must be the right one!) */
	load_dll();


	/* This TRY..FINALLY forces DLL unloading in case something happens */
	TRY

	/* The main loop */
	while (exec) {

		/* Redraw screen if something has changed */
		if (redraw) {
			if (redraw < 2 && active_menu != M_MAIN)
				sel_item(active_item, active_menu); // unselect the active item
			redraw_screen(redraw, active_menu, watch_rotation, show_grid);
			select = 1;
			redraw = 0;
		}

		/* Highlight the active item (if valid) */
		if (select && (flags & F_OBJECTS)) {
			sel_item(active_item, active_menu);
			if (active_menu >= M_SKYDOME && active_menu <= M_HORIZON)
				show_wbody(active_item, flags);
			select = 0;
		}

		/* Redraw the status line with new values (and clear the previous contents,
		 * e.g. some status indicators (we are using ngetchx() to get the keys ...) */
		update_statline(active_menu, active_item);

		/* Go into IDLE mode until a key is pressed to save battery power;
		 * then check how to handle the pressed key */
		wait();
		switch (key = read_char()) {

			/* Wanna get out? */
			case K_QUIT:
			case KEY_QUIT:
			case KEY_ON:
				if (DlgMessage(LOC_CONFIRM, LOC_QUIT, BT_YES, BT_NO) == KEY_ENTER)
					exec = 0;
				break;

			/* This should be clear */
			case KEY_OFF:
				off();
				break;

			/* Show settings dialog */
			case K_SETTINGS:
				if (settings_dlg()) {

					/* config is saved immediately */
					if (cfg_write())
						DlgMessage(LOC_ERR, LOC_ERR_CFGSAVE, BT_OK, BT_NONE);
					redraw = 2;
				}
				break;

			/* Enable/disable selected object or edit user-defined object */
			case KEY_ENTER:
			case K_EDITITEM:
				if (active_menu != M_MAIN) {

					/* user-defined objects need special handling */
					if (active_item >= OBJ1 && active_item <= OBJ4) {

						if (objects[active_item-OBJ1].o_type == OBJ_NONE || key == K_EDITITEM) {

							/* edit object -- only allowed in data screens */
							if (active_menu <= M_SKYDOME) {
								sel_item(active_item, active_menu);
								set_object(active_item-OBJ1);
								sel_item(active_item, active_menu);
							}

							/* Toggle flag to wrong state - it will be xored below */
							if (objects[active_item-OBJ1].o_type != OBJ_NONE)
								flags |= (1<<active_item);
							else
								flags &= ~(1<<active_item);

							break;
						}
					}

					if (key != K_EDITITEM)
						flags ^= (1<<active_item);
				}
				break;

			/* Redraw the screen now! */
			case K_UPDATE:
				redraw = 2;
				break;

			case K_MAIN:
			case K_DATA1:
			case K_DATA2:
			case K_RISET:
			case K_SKYDOME:
			case K_HORIZON:
				active_menu = key - K_MAIN + M_MAIN;
				redraw = 2;
				break;

			/* Change mjd by one hour */
			case K_TIMEDOWN:
			case K_TIMEUP:
				now.n_mjd += 1.0/24.0 * NEG(key == K_TIMEDOWN);
				redraw = 1;
				break;

			/* Change mjd by one day */
			case KEY_2ND + K_TIMEDOWN:
			case KEY_2ND + K_TIMEUP:
				now.n_mjd += NEG(key == KEY_2ND + K_TIMEDOWN);
				redraw = 1;
				break;

			/* Rotate skyview */
			case K_ROTL:
			case K_ROTR:
				if (active_menu == M_SKYDOME || active_menu == M_HORIZON) {
					watch_rotation = (watch_rotation + 90 * NEG(key == K_ROTL)) % 360;
					redraw = 2;
				}
				break;

			/* Select item (if there are any items enabled) */
			case K_PREVITEM:
			case K_NEXTITEM:
				if (active_menu != M_MAIN) {
					sel_item(active_item, active_menu);
					active_item = nxtbody(active_item, NEG(key == K_PREVITEM));
					if (active_item == -1)
						active_item = nxtbody(-1, NEG(key == K_PREVITEM));
					select = 1;
				}
				break;

			/* Toggle labels in Watch (Skydome & Horizon) mode */
			case K_TOGGLE:
				if (active_menu >= M_SKYDOME && active_menu <= M_HORIZON) {
					sel_wbody(active_item);
					show_grid = !show_grid;
					redraw = 2;
				}
				break;

			/* Show object info */
			case K_INFO:
				if (active_menu != M_MAIN)
					show_info(active_item);
				break;

			/* Show help */
			case K_HELP:
				show_help();
				break;

			default:
				break;
		}
	}
	FINALLY
		UnloadDLL();
	ENDFINAL

	cfg_write();
	bye();
}


/* Select and highlight item */
void sel_item(short p, short menu)
{
	if (p == -1)
		return;

	if (menu >= M_DATA1 && menu <= M_RISET) {
		short r = (CH * bodyrow[p]) - 1;
		ScrRectFill(&(SCR_RECT) {{0,r,239,r+CH}},
			&(SCR_RECT) {{0,15,239,119}}, A_XOR);
	} else if (menu >= M_SKYDOME && menu <= M_HORIZON) {
		sel_wbody(p);
	}
}

/* Redraw screen corresponding to how_much */
void redraw_screen(short how_much, short menu,	short watch_rot, short show_grid)
{
	short p;
	char buf[48];
	short i;

	/* Verify that empty user-objects are disabled */
	for (i = 0; i < NOBJ-OBJ1; i++)
		if (objects[i].o_type == OBJ_NONE)
			flags &= ~(1<<(OBJ1+i));

	/* Clear the screen for a full redraw */
	if (how_much == 2)
	    c_erase();

	/* Update global circumstances */
	if (how_much) {
		ST_helpMsg(LOC_UPDATE_CORE);
		update_const(&now);
	}

	/* Update other values for specific menu screen */
	switch (menu) {

		case M_MAIN:
			if (how_much == 2)
		    	mm_labels();

			/* Print all the time-related fields */
			ST_helpMsg(LOC_UPDATE_MAIN);
			mm_now(&now, how_much);

			/* Compute and print twilight */
			if (flags & F_TWILIGHT) {
				ST_helpMsg(LOC_UPDATE_TWIL);
		    	mm_twilight(&now, how_much, flags & (F_CIVILTWI ? TWILIGHTCIV : TWILIGHT));
		    } else {
	    		f_string(R_DAWN, C_DAWNV, 	"-----");
		    	f_string(R_DUSK, C_DUSKV, 	"-----");
	    		f_string(R_NITE, C_NITEV, 	"-----");
	    	}

			chk_char();
			break;

		case M_DATA1:
		case M_DATA2:
		case M_RISET:
			if (how_much == 2)
		    	alt_labels(menu);

			/* Update every enabled object */
	    	for (p = nxtbody(-1, 1); p != -1; p = nxtbody(p, 1))
	    		if (flags & (1<<p)) { /* enabled? */
		    		sprintf(buf, LOC_UPDATE_OBJ "  " LOC_KEY_STOP, objectname(p));
						ST_helpMsg(buf);

	    			alt_body(menu, p, how_much, &now,
	    				((p>=OBJ1 && p<=OBJ4) ? &objects[p-OBJ1] : (Obj*)0),
	    				((flags & F_ADPHZN) ? ADPHZN : STDHZN));

					/* User want to abort processing? */
					if (chk_char() == KEY_ESC) break;
		    	}

			chk_char ();
			break;

		case M_SKYDOME:
		case M_HORIZON:
			if (how_much == 2)
				c_erase();

			watch_labels(menu, watch_rot, show_grid);

			if (how_much == 1)
				break;

	    	for (p = nxtbody(-1, 1); p != -1; p = nxtbody (p, 1))
	    		if (flags & (1<<p)) {
		    		sprintf(buf, LOC_UPDATE_OBJ "  " LOC_KEY_STOP, objectname(p));
						ST_helpMsg(buf);

	    			watch_body(menu, p, watch_rot, &now,
	    				((p>=OBJ1 && p<=OBJ4) ? &objects[p-OBJ1] : (Obj*)0));

					/* User want to abort processing? */
					if (chk_char() == KEY_ESC) break;
	    		}

			chk_char();
			break;

		default:
			break;
	}
}

/* Update status line with some info */
void update_statline(short menu, short item)
{
	char buf[48];
	short dm, dy, th, tm, ts, tneg;
	double dd, lmjd = now.n_mjd - now.n_tz/24.0;

	FontSetSys(F_4x6);

	ST_clear();
	DrawLine(STAT_SPLIT1, A_NORMAL);
	DrawLine(STAT_SPLIT2, A_NORMAL);
	DrawLine(STAT_SPLIT3, A_NORMAL);
	DrawLine(STAT_SPLIT4, A_NORMAL);

	/* ... local date */
	mjd_cal(lmjd, &dm, &dd, &dy);
	sprintf(buf, "%02d.%02d.%-4d", (short)(dd), dm, dy);
	DrawStr(X_STAT2, Y_STATLINE, buf, A_NORMAL);

	/* ... local time */
	dec_sex(mjd_hr(lmjd), &th, &tm, &ts, &tneg);
	sprintf(buf, "%*d:%02d:%02d", (th>=10 ? 2 : 3), tneg ? -th : th, tm, ts);
	DrawStr(X_STAT3, Y_STATLINE, buf, A_NORMAL);

	/* ... observer's longitude */
	dec_sex(raddeg(now.n_lng), &th, &tm, &ts, &tneg);
	if (th == 0 && tneg) sprintf(buf, "    -0:%02d:%02d", tm, ts);
	else sprintf(buf, "%*s%d:%02d:%02d", (th>=100 ? 0 : (th>=10 ? 2 : 4))
		+ (tneg ? 0 : 2), "", tneg ? -th : th, tm, ts);
	DrawStr(X_STAT4, Y_STATLINE, buf, A_NORMAL);

	/* ... observer's latitude */
	dec_sex (raddeg(now.n_lat), &th, &tm, &ts, &tneg);
	if (th == 0 && tneg) sprintf(buf, "    -0:%02d:%02d", tm, ts);
	else sprintf(buf, "%*s%d:%02d:%02d", (th>=10 ? 0 : 2)
		+ (tneg ? 0 : 2), "", tneg ? -th : th, tm, ts);
	DrawStr(X_STAT5, Y_STATLINE, buf, A_NORMAL);

	/* ... tooltip */
	switch (menu) {
		case M_MAIN:
			DrawStr(X_STAT1, Y_STATLINE, LOC_MAIN "  " LOC_KEY_HELP, A_NORMAL);
			break;

		case M_DATA1:
		case M_DATA2:
		case M_RISET:
		case M_SKYDOME:
		case M_HORIZON:
			sprintf(buf,
				(flags & (1<<item) ? "%s" : "(%s)"),
				objectname(item));
			DrawStr(X_STAT1, Y_STATLINE, buf, A_NORMAL);
			break;

		default:
			break;
	}

	FontSetSys (F_6x8);
}

/* Display settings editor and let user change preferences */
short settings_dlg(void)
{
	LCD_BUFFER lcd;
	Now n;
	unsigned short f;
	char buf[32];
	short exec = 1, new = 1, changed = 0, sel_item = S_NULL, select = 1;
	double v;
	unsigned short i;
	short d, m, s, y;
	double lmjd = 0.;

	LCD_save(lcd);

	ScrRectFill(&(SCR_RECT){{7,3,231,117}},
		&(SCR_RECT){{0,0,239,120}}, A_REVERSE);
	DrawClipRect(&(WIN_RECT){8,4,230,116},
		&(SCR_RECT){{0,0,239,120}}, A_NORMAL | B_DOUBLE);

	f_string(R_STOP-2, S_COLL+8, LOC_SETTINGS);
	ScrRectFill(&(SCR_RECT){{11,7,227,15}},
		&(SCR_RECT){{0,0,239,120}}, A_XOR);

	f_string(R_STOP+S_JD,		S_COL1,		LOC_SET_JD);
	f_string(R_STOP+S_TIME,		S_COL1,		LOC_SET_TIME);
	f_string(R_STOP+S_DATE,		S_COL1,		LOC_SET_DATE);
	f_string(R_STOP+S_TZ,		S_COL1,		LOC_SET_TZ);
	f_string(R_STOP+S_LON,		S_COL1,		LOC_SET_LONG);
	f_string(R_STOP+S_LAT,		S_COL1,		LOC_SET_LAT);
	f_string(R_STOP+S_HEIGHT,	S_COL1,		LOC_SET_HEIGHT);
	f_string(R_STOP+S_TEMP,		S_COL1,		LOC_SET_TEMP);
	f_string(R_STOP+S_PRES,		S_COL1,		LOC_SET_PRESS);
	f_string(R_STOP+S_EPOCH,	S_COL1,		LOC_SET_EPOCH);
	f_string(R_STOP+S_FLAGS,	S_COL1,		LOC_SET_FLAGS);

	n = now;
	f = flags;
	while (exec) {
		if (new) {
			/* Print current settings */
			lmjd  = n.n_mjd - n.n_tz/24.0;
			f_double(R_STOP+S_JD, S_COL2-2, "%#12.4f", n.n_mjd + MJD);
			f_time(R_STOP+S_TIME, S_COL2+2, mjd_hr(lmjd));
			f_date(R_STOP+S_DATE, S_COL2, lmjd);
			f_time(R_STOP+S_TZ, S_COL2+2, -n.n_tz);
			f_gangle(R_STOP+S_LON, S_COL2, n.n_lng);
			f_gangle(R_STOP+S_LAT, S_COL2, n.n_lat);
			f_double(R_STOP+S_HEIGHT, S_COL2+3, "%7.1f",
				n.n_height * REARTH);
			f_double(R_STOP+S_TEMP, S_COL2+4, "%6.1f",
				n.n_temp);
			f_double(R_STOP+S_PRES, S_COL2+6, "%4.0f", n.n_pressure);
		    if (n.n_epoch == EOD) {
					f_string(R_STOP+S_EPOCH, S_COL2+2, LOC_EOD);
		    } else {
		    	double tmp;
					mjd_year(n.n_epoch, &tmp);
					f_double(R_STOP+S_EPOCH, S_COL2+2, "%8.1f", tmp);
		    }
		    for (i = 0; i < 16; i++)
		    	f_char(R_STOP+S_FLAGS, S_COL2+i-6, FLAG(i));
		    new = 0;
		}

		if (select) {
			f_char(R_STOP+sel_item, S_COLL, 0x12);
			f_char(R_STOP+sel_item, S_COLR, 0x11);
			ST_helpMsg(settings_help[sel_item]);
			select = 0;
		}

		wait();
		switch (i = read_char()) {
			case K_SETTINGS:
			case KEY_ESC:
			case KEY_QUIT:
			case KEY_ON:
				exec = 0;
				if (changed)
					if (DlgMessage(LOC_NEW_SETTINGS, LOC_SETTINGS_SAVE,
							BT_YES, BT_NO) == KEY_ESC)
						changed = 0;
					else {
						now = n;
						flags = f;
					}
				break;

			case KEY_ENTER:
				if (sel_item == S_FLAGS) {
					edit_flags(&f);
					select = 1;
				} else {
					f_blanks(R_STOP+sel_item, S_COL2-2, 12);
					c_pos(R_STOP+sel_item, S_COL2-2);
					getsn(buf, 12);
					f_blanks(R_STOP+sel_item, S_COL2-2, 12);
					buf[strlen(buf)] = 0;

					if (sel_item == S_DATE) {
						f_sscandate(buf, &m, &d, &y);
						cal_mjd (m, d, y, &v);
					} else if (sel_item == S_EPOCH) {
						if (cmpstri(buf, "eod") == 0) {
							v = EOD;
						} else if (decimal_year(buf)) {
							year_mjd(atof(buf), &v);
						}
					} else if ((sel_item >= S_TIME) && (sel_item <= S_LAT)) {
						f_sscansex(buf, &d, &m, &s);
						sex_dec(d, m, s, &v);
					} else {
						v = atof(buf);
					}

					if (is_nan(v))
						v = 0.0;

					switch (sel_item) {
						case S_JD:
							n.n_mjd = v - MJD;
							break;
						case S_TIME:
							lmjd = mjd_day(lmjd) + v/24.0;
							n.n_mjd = lmjd + n.n_tz/24.0;
							break;
						case S_DATE:
							lmjd = mjd_day(v) + mjd_hr(lmjd)/24.0;
							n.n_mjd = lmjd + n.n_tz/24.0;
							break;
						case S_TZ:
							n.n_mjd -= (v + n.n_tz)/24.0;
							n.n_tz = -v;
							break;
						case S_LON:
							n.n_lng = degrad(v);
							break;
						case S_LAT:
							n.n_lat = degrad(v);
							break;
						case S_HEIGHT:
							n.n_height = v / REARTH;
							break;
						case S_TEMP:
							n.n_temp = v;
							break;
						case S_PRES:
							n.n_pressure = v;
							break;
						case S_EPOCH:
							n.n_epoch = v;
							break;
					}
				}

				changed = 1;
				new = 1;
				break;

			case KEY_UP:
			case KEY_DOWN:
				f_char(R_STOP+sel_item, S_COLL, ' ');
				f_char(R_STOP+sel_item, S_COLR, ' ');

				sel_item += NEG(i == KEY_UP);
				if (sel_item < S_NULL)
					sel_item = S_NUM-1;
				else if (sel_item > S_NUM-1)
					sel_item = S_NULL;

				select = 1;
				break;

			default:
				break;
		}
	}

	LCD_restore(lcd);

	return changed;
}

/* Edit flags inside settings dialog */
void edit_flags(unsigned short *f)
{
	short exec = 1, select = 1, idx = 0, key;
	unsigned short bak = *f;
	char buf[32];

	while (exec) {
		if (select) {
			f_char(R_STOP+S_FLAGS, S_COL2+idx-6, 0x18);
			if (idx >= NOBJ) {
				ST_helpMsg(flags_help[idx-NOBJ]);
			} else {
				sprintf(buf, LOC_SET_OBJ_HELP, obj_names[idx]);
				ST_helpMsg(buf);
			}
			select = 0;
		}

		wait();
		switch (key = read_char()) {
			case KEY_ENTER:
				exec = 0;
				break;

			case KEY_ESC:
			case KEY_QUIT:
				exec = 0;
				*f = bak;
				break;
			case KEY_DOWN:
			case KEY_UP:
				*f ^= (1<<idx);
				break;

			case KEY_LEFT:
			case KEY_RIGHT:
				f_char (R_STOP+S_FLAGS, S_COL2+idx-6, FLAGP(idx));

				idx += NEG(key == KEY_LEFT);
				if (idx < 0)
					idx = 15;
				else if (idx > 15)
					idx = 0;
				select = 1;
				break;
		}
	}
}

/* Let user enter an user-defined object */
void set_object(short p)
{
	short r = R_OBJ1+p;
	char buf[64];

	sprintf(buf, LOC_EDIT_KEY, obj_names[OBJ1+p]);
	ST_helpMsg(buf);

	f_blanks(r, C_OBJ+3, 37);
	f_string(r, C_OBJ+3,
			(objects[p].o_type != OBJ_NONE ? objectname(OBJ1+p) : LOC_EDIT_NONE));

	wait();
	if (read_char() == KEY_ENTER) {
		sprintf(buf, LOC_EDIT_HELP, obj_names[OBJ1+p]);
		ST_helpMsg(buf);

		f_blanks(r, C_OBJ+3, 37);
		f_char(r, C_OBJ+3, '>');
		c_pos(r, C_OBJ+5);
		getsn(buf, MAXNM);

		/* Search database for object */
		if (buf[0] == '\0')
			objects[p].o_type = OBJ_NONE;
		else if (!db_read(buf, 0, &objects[p]))
			DlgMessage(LOC_ERR, LOC_ERR_OBJECT, BT_OK, BT_NONE);
	}

	f_blanks(r, C_OBJ+3, 37);
}

/* Display information about selected object */
void show_info(short p) {
	LCD_BUFFER lcd;
	Obj *op = NULL;
	double tmp;
	const char *s = NULL;

	if (objects[p-OBJ1].o_type == OBJ_NONE)
		return;

	LCD_save(lcd);

	ScrRectFill(&(SCR_RECT){{25,44,213,91}},
		&(SCR_RECT){{0,0,239,120}}, A_REVERSE);
	DrawClipRect(&(WIN_RECT){26,45,212,90},
		&(SCR_RECT){{0,0,239,120}}, A_NORMAL);

	f_string(R_ITOP, I_COL1, objectname (p));

	if (p >= OBJ1 && p <= OBJ4) {
		op = &objects[p-OBJ1];

		/* Sub-types for fixed objects */
		if (op->o_type == FIXED) {
			switch (op->o_class) {
				case 'S':
					s = LOC_OBJ_STAR;
					break;
				case 'D':
					s = LOC_OBJ_DBLSTAR;
					break;
				case 'V':
					s = LOC_OBJ_VARSTAR;
					break;
				case 'G':
					s = LOC_OBJ_GALAXY;
					break;
				case 'C':
					s = LOC_OBJ_CLUSTER;
					break;
				case 'O':
					s = LOC_OBJ_OPEN;
					break;
				case 'P':
					s = LOC_OBJ_PLNEB;
					break;
				case 'N':
					s = LOC_OBJ_NEB;
					break;
				case 'E':
					s = LOC_OBJ_EMNEB;
					break;
				case 'R':
					s = LOC_OBJ_REST;
					break;
				case 'M':
					s = LOC_OBJ_MILKYWAY;
					break;
				case 'A':
					s = LOC_OBJ_ASTERISM;
					break;
			}
		} else {
			/* Sub-types for other (ellipt., parab., hyperb.) objects */
			switch (op->o_class) {
				case 'A':
					s = LOC_OBJ_ASTEROID;
					break;
				case 'P':
					s = LOC_OBJ_PLANETOID;
					break;
				case 'C':
					s = LOC_OBJ_COMET;
					break;
			}
		}

		f_string(R_ITOP, I_COL2, s);

		/* Print info */
		if ((op->o_type == ELLIPTICAL) ||
				(op->o_type == HYPERBOLIC) ||
				(op->o_type == PARABOLIC)) {
			f_string(R_ITOP+2, I_COL1, LOC_INFO_DIAM);
			f_string(R_ITOP+3, I_COL1, LOC_INFO_DAYLEN);
			f_string(R_ITOP+4, I_COL1, LOC_INFO_PERIOD);
		}

		switch (op->o_type) {
			case FIXED:
				f_string(R_ITOP+2, I_COL1, LOC_INFO_MAG);
				f_string(R_ITOP+3, I_COL1, LOC_INFO_DIST);
				f_string(R_ITOP+4, I_COL1, LOC_INFO_CLASS);

				f_double(R_ITOP+2, I_COL3, "%#.2f", op->o_data.o_f.f_mag);
				tmp = op->o_data.o_f.f_dist;
				if (tmp >= MPARSEC)
				 	f_double(R_ITOP+3, I_COL3, "%#.2f Mpc", tmp /MPARSEC);
				else if (tmp >= KPARSEC)
				 	f_double(R_ITOP+3, I_COL3, "%#.2f kpc", tmp /KPARSEC);
				else
				 	f_double(R_ITOP+3, I_COL3, "%#.2f pc", tmp /PARSEC);
			 	f_string(R_ITOP+4, I_COL3, op->o_data.o_f.f_class);
				break;

			case ELLIPTICAL:
			 	f_double(R_ITOP+2, I_COL3, "%#.1f km", op->o_data.o_e.e_radius);
			 	f_double(R_ITOP+3, I_COL3, "%#.2f h", op->o_data.o_e.e_daylen);
			 	f_double(R_ITOP+4, I_COL3, "%#.2f a", pow (op->o_data.o_e.e_a, 1.5));
				break;

			case HYPERBOLIC:
			 	f_double(R_ITOP+2, I_COL3, "%#.1f km", op->o_data.o_e.e_radius);
			 	f_double(R_ITOP+3, I_COL3, "%#.2f h", op->o_data.o_e.e_daylen);
				f_string(R_ITOP+4, I_COL3, LOC_INFO_HYPERB);
				break;

			case PARABOLIC:
			 	f_double(R_ITOP+2, I_COL3, "%#.2f km", op->o_data.o_e.e_radius);
			 	f_double(R_ITOP+3, I_COL3, "%#.2f h", op->o_data.o_e.e_daylen);
				f_string(R_ITOP+4, I_COL3, LOC_INFO_PARAB);
				break;
		}
	} else {
		/* Hardcoded values for internal objects like Sun, Moon etc. */
		if (p == SUN) {
			f_string(R_ITOP+2, I_COL1, LOC_INFO_MAG);
			f_string(R_ITOP+3, I_COL1, LOC_INFO_DIAM);
			f_string(R_ITOP+4, I_COL1, LOC_INFO_CLASS);
			f_string(R_ITOP+2, I_COL2, "-26.77");
			f_string(R_ITOP+3, I_COL2, "1.3925 Gm");
			f_string(R_ITOP+4, I_COL2, "G2 V");
		} else {
			f_string(R_ITOP+2, I_COL1, LOC_INFO_DIAM);
			f_string(R_ITOP+3, I_COL1, LOC_INFO_DAYLEN);
			f_string(R_ITOP+4, I_COL1, LOC_INFO_PERIOD);
			if (p == MOON) {
			 	f_string(R_ITOP+2, I_COL3, "1738.0 km");
			 	f_string(R_ITOP+3, I_COL3, "29.53 d");
			 	f_string(R_ITOP+4, I_COL3, "29.53 d");
			} else {
				double t[3];
				switch (p) {
					case MERCURY:
						t[0] = 4878.0; t[1] = 176.0; t[2] = 87.97;	break;
					case VENUS:
						t[0] = 12104.0; t[1] = 117.0; t[2] = 224.7;	break;
					case MARS:
						t[0] = 6794.1; t[1] = 24.66; t[2] = 686.98;	break;
					case JUPITER:
						t[0] = 142796.0; t[1] = 9.84; t[2] = 11.87;	break;
					case SATURN:
						t[0] = 120000.0; t[1] = 10.23; t[2] = 29.46;	break;
					case URANUS:
						t[0] = 51118.0; t[1] = 17.23; t[2] = 84.67;	break;
					case NEPTUNE:
						t[0] = 49424.0; t[1] = 16.05; t[2] = 165.49;	break;
				}
		 		f_double(R_ITOP+2, I_COL3, "%#.1f km", t[0]);
		 		f_double(R_ITOP+3, I_COL3,
		 			p < MARS ? "%#.2f d" : "%#.2f h", t[1]);
		 		f_double(R_ITOP+4, I_COL3,
		 			p < MARS ? "%#.2f d" : "%#.2f a", t[2]);
		 	}
		}
	}

	wait();
	read_char();

	LCD_restore(lcd);
}

/* Display keymap */
void show_help() {
	LCD_BUFFER lcd;

	LCD_save(lcd);

	ScrRectFill(&(SCR_RECT){{7,11,231,115}},
		&(SCR_RECT){{0,0,239,120}}, A_REVERSE);
	DrawClipRect(&(WIN_RECT){8,12,230,114},
		&(SCR_RECT){{0,0,239,120}}, A_NORMAL);

	f_string(R_HTOP,   H_COL1+16, LOC_HELP);

	f_string(R_HTOP+2, H_COL1, LOC_HELP_MAIN);
	f_string(R_HTOP+3, H_COL1, LOC_HELP_DATA1);
	f_string(R_HTOP+4, H_COL1, LOC_HELP_DATA2);
	f_string(R_HTOP+5, H_COL1, LOC_HELP_RISET);
	f_string(R_HTOP+6, H_COL1, LOC_HELP_SKYDOME);
	f_string(R_HTOP+7, H_COL1, LOC_HELP_HORIZON);
	f_string(R_HTOP+8, H_COL1, LOC_HELP_INFO);
	f_string(R_HTOP+9, H_COL1, LOC_HELP_HELP);

	f_string(R_HTOP+2, H_COL2, LOC_HELP_TOGGLE);
	f_string(R_HTOP+3, H_COL2, LOC_HELP_UPDATE);
	f_string(R_HTOP+4, H_COL2, LOC_HELP_MAPINFO);
	f_string(R_HTOP+5, H_COL2, LOC_HELP_SELITEM);
	f_string(R_HTOP+6, H_COL2, LOC_HELP_ROTATE);
	f_string(R_HTOP+7, H_COL2, LOC_HELP_TIME);
	f_string(R_HTOP+8, H_COL2, LOC_HELP_EDIT);
	f_string(R_HTOP+9, H_COL2, LOC_HELP_SETTINGS);

	f_string(R_HTOP+11,H_COL1, LOC_HELP_QUIT);
	f_string(R_HTOP+11,H_COL2, LOC_HELP_SLEEP);

	wait();
	read_char();

	LCD_restore(lcd);
}
