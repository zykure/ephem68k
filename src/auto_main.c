/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	auto_main.c
 * Desc:	Core of the ephem68k Auto Processor (aphem68k).
 *
 * Version:	aphem68k 1.1, 2008 Oct 26 (uses DLLs v1.1)
 *
 * Usage: aphem68k needs five parameters:
 *   start_date:	Date to start calculation (MJD.NN format)
 *   stop_date:		Date to stop (format like above)
 *   time_step:   Step between each calculation (format like above)
 *   objects:     Integer flag field to specify which objects to compute
 *                See the F_* defines in 'shared.h' to find out which values
 *                to use. Example: "0b0000000010000000" will compute sun.
 *   items:       Integer flag field (see above) to specify which values to
 *                store (output filtering). See the I_xx defines below to
 *                find out which values to use. Attention: Values with bits
 *                higher than 11 (e.g. I_MJD) are computed only once while
 *                other values will be computed for each object.
 *   Values are computed for each object and for each date. So you will get
 *   num_objects * (stop_date - start_date) / time_step
 *   data values when running aphem68k.
 *
 *   aphem68k returns a 2-dimensional matrix containign all values. The 1st
 *   dimension (row coordinates) specifies time as specified when executing
 *   the program; the 2nd dimension (column coordinates) specifies the field
 *   index of each value.
 *   To get the field index of a specific value, you'll have to count which
 *   objects and items are enabled.
 *   Example: objects = 384 (Sun and Moon) and items = 24579 (MJD, LST, RA, Dec).
 *   The matrix (1st row) will look like this:
 *     [ MJD, LST, (Sun RA, Sun Dec, Moon RA, Moon Dec ]
 *   Every row corresponds to a date as specified, you can calculate this by
 *   mjd = start_date + i * time_step (beginning with row 0 for i) or look
 *   at the first element of each row if you enabled I_MJD in the items field.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

/* Since we're using a DLL, we have to make sure the program runs in the
 * so-called 'ghost space'.
 * Also, ephem68k MUST be archived to run properly (the caching system
 * uses static vars, so if you don't archive all parts of the program
 * it will re-use the old values since they aren't cleared when the app
 * is stored in flash memory - that will produce very strange results.
 */
#define EXECUTE_IN_GHOST_SPACE
//#define RETURN_VALUE
#define RETURN_VALUE ephdata

#include <tigcclib.h>
#include "astro.h"
#include "circum.h"
#include "ephem.h"
#include "shared.h"
#include "io.h"
#include "screen.h"
#include "formats.h"
#include "info.h"
#include "i18n.h"


/* Flags for the 'data items' item (16-bit-field).
 * The 'data items' field is used to filter output of
 * the program, e.g. which values to return. */
#define I_RA			1		// bit  0
#define I_DEC			2		// bit  1
#define I_AZ			4		// bit  2
#define I_ALT			8		// bit  3
#define I_VMAG			16		// bit  4
#define I_PHASE			32		// bit  5
#define I_HLON			64		// bit  6
#define I_HLAT			128		// bit  7
#define I_EDIST			256		// bit  8
#define I_SDIST			512		// bit  9
#define I_ELONG			1024	// bit  10
#define I_SIZE			2048	// bit  11
#define I_OBJECTS		4095
#define I_MJD			8192	// bit  13
#define I_LST			16384	// bit  14
#define I_OBLIQ			32768	// bit  15
#define I_CONST			57344

/* object names (see astro.h) */
const char *obj_names[NOBJ] = {
	LOC_OBJECT_NAMES
};

short active_dll;
AutoData auto_data;
LCD_BUFFER lcdbuf;
Obj objects[NOBJ-OBJ1];
Now now;
unsigned short flags, items;
short progress;

void comp_obj(short, Obj *);


/* Main */
void _main(void)
{
  	short p;
	char buf[64];

	/* Initialize variables with some default values in case there's no config file */
	flags = F_SUN | F_MOON | F_MERCURY | F_VENUS | F_MARS |
		F_JUPITER | F_SATURN;

	now.n_mjd		= 2453652.5 - MJD;	// 01.01.2000, 00:00:00 UT
	now.n_temp		= 20;
	now.n_pressure	= 1013;

	objects[0].o_type = OBJ_NONE;
	objects[1].o_type = OBJ_NONE;
	objects[2].o_type = OBJ_NONE;

	active_dll = 0;				// We got no DLL loaded

	LCD_save(lcdbuf);			// Save screen contents to restore them later

	ST_busy(ST_NORMAL);		// Hide those nasty status symbols

	ESI argptr = top_estack;
	if (ArgCount() < 5) {
  		bye();
  		ER_throw(ER_TOO_FEW_ARGS);
  	}
  	if (ArgCount() > 5) {
  		bye();
  		ER_throw(ER_TOO_MANY_ARGS);
  	}

  	if (GetArgType(argptr) == FLOAT_TAG)
    	auto_data.start = GetFloatArg(argptr);
  	else if (GetArgType(argptr) == POSINT_TAG || GetArgType(argptr) == NEGINT_TAG )
    	auto_data.start = (double) GetIntArg(argptr);
  	else {
  		bye();
  		ER_throw(ER_ARG_MUST_BE_DECIMAL);
  	}

  	if (GetArgType(argptr) == FLOAT_TAG)
    	auto_data.stop = GetFloatArg(argptr);
  	else if (GetArgType(argptr) == POSINT_TAG || GetArgType(argptr) == NEGINT_TAG )
    	auto_data.stop = (double) GetIntArg(argptr);
  	else {
  		bye();
  		ER_throw(ER_ARG_MUST_BE_DECIMAL);
  	}

  	if (GetArgType(argptr) == FLOAT_TAG)
    	auto_data.step = GetFloatArg(argptr);
  	else if (GetArgType(argptr) == POSINT_TAG)
    	auto_data.step = (double) GetIntArg(argptr);
  	else {
  		bye();
  		ER_throw(ER_ARG_MUST_BE_DECIMAL);
  	}

  	if (GetArgType(argptr) == POSINT_TAG)
    	flags = (double) GetIntArg(argptr);
  	else {
  		bye();
  		ER_throw(ER_ARG_MUST_BE_DECIMAL);
  	}

  	if (GetArgType(argptr) == POSINT_TAG)
    	items = (double) GetIntArg(argptr);
  	else {
  		bye();
  		ER_throw(ER_ARG_MUST_BE_DECIMAL);
  	}

  	while (GetArgType (top_estack) != END_TAG)  // Clean up arguments
    	top_estack = next_expression_index (top_estack);
  	top_estack--;

	/* Display credits ;) */
	splash();

	FontSetSys(F_6x8);

	/* Try to read the config file, if one exists */
	if (cfg_read() > 0)
		DlgMessage(LOC_ERR, LOC_ERR_CONFIG, BT_OK, BT_NONE);

 	if (auto_data.step <= 0. || auto_data.stop - auto_data.start <= 0) {
  		bye();
  		ER_throw(ER_DOMAIN);
  	}

	/* Try to load the right DLL (version number is hardcoded in ephem68k) */
	load_dll();

  	TRY

    push_END_TAG(); // 1st Dimension: Time
    for (now.n_mjd = auto_data.stop; now.n_mjd >= auto_data.start; now.n_mjd -= auto_data.step) {
		push_END_TAG(); // 2nd Dimension: Objects

		progress = floor(100.0 * (1.0 - (now.n_mjd / auto_data.stop)));

		sprintf(buf, "[%d%%] MJD %.2f - " LOC_UPDATE_CORE, progress, now.n_mjd);
		update_const(&now);

		if ((items & I_OBJECTS) && (flags & F_OBJECTS)) {

  	    	for (p = nxtbody(-1, -1); p != -1; p = nxtbody(p, -1)) {
	      		if (flags & (1<<p))
        			comp_obj(p, (p>=OBJ1 && p<=OBJ4) ? &objects[p-OBJ1] : (Obj*)0);
        		if (kbhit() == K_QUIT)
        			break;
      		}
      	}

 		/* Obliquity of the Ecliptic, degrees */
     	if (items & I_OBLIQ)	push_Float(raddeg(now.n_const.c_eps));

 		/* Local sidereal time, hours */
     	if (items & I_LST)		push_Float(now.n_const.c_lst);

 		/* Modified Julian Date, mjd */
     	if (items & I_MJD)		push_Float(now.n_mjd);

		push_LIST_TAG();
    	if (kbhit() == K_QUIT)
    	break;
    }
    push_LIST_TAG();

	FINALLY
		UnloadDLL();

	ENDFINAL

  GKeyFlush();
  bye();
}

void comp_obj(short p, Obj *obj) {
	Sky sky;
	char buf[64];

  	sprintf(buf, "[%d%%] MJD %.2f - " LOC_UPDATE_OBJ, progress, now.n_mjd, objectname(p));
  	ST_helpMsg(buf);

	if (p>=OBJ1 && p<=OBJ4) {
		if (!obj)
			return;
		if (obj->o_type == OBJ_NONE)
			return;
	}
	body_cir(p, ((flags & F_ADPHZN) ? ADPHZN : STDHZN), &now, obj, &sky);

	/* Angular size, arcsec */
	if (items & I_SIZE)		push_Float(sky.s_size);
	/* Elonagtion, degrees */
	if (items & I_ELONG)	push_Float(p == SUN ? 0.0 : sky.s_elong);
	/* Sun Distance, AU */
	if (items & I_SDIST)	push_Float(sky.s_sdist <= 0.0 ? 0.0 : sky.s_sdist);
	/* Earth Distance, AU (Moon: km) */
	if (items & I_EDIST)	push_Float(sky.s_edist <= 0.0 ? 0.0 : sky.s_edist);
	/* Heliocentric (Ecliptical) Latitude, degrees */
	if (items & I_HLAT)		push_Float(p == SUN ? 0.0 : raddeg(sky.s_hlat));
	/* Heliocentric (Ecliptical) Longitude, degrees */
	if (items & I_HLON)		push_Float(sky.s_hlong == NOHELIO ? 0.0 : raddeg(sky.s_hlong));
	/* Phase (Illumination), percent */
	if (items & I_PHASE)	push_Float(sky.s_sdist <= 0.0 ? 0.0 : sky.s_phase);
	/* Visible Magnitude, mag */
	if (items & I_VMAG)		push_Float(sky.s_mag);
	/* Topocentric Altitude, degrees */
	if (items & I_ALT)		push_Float(raddeg(sky.s_alt));
	/* Topocentric Azimuth, degrees */
	if (items & I_AZ)		push_Float(raddeg(sky.s_az));
	/* Topocentric Declination, degrees */
	if (items & I_DEC)		push_Float(raddeg(sky.s_dec));
	/* Topocentric Right Ascension, hours */
	if (items & I_RA)		push_Float(radhr(sky.s_ra));
}
