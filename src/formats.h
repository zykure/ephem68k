/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	formats.h
 * Desc:	Header file to use output formatting routines
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef _FORMATS_H
#define _FORMATS_H

void f_blanks (short, short, short);
void f_sexad (short, short, short, short, short, double);
void f_sexag (short, short, short, double);
void f_date (short, short, double);
void f_int (short, short, const char *, double);
void f_char (short, short, const char);
void f_string (short, short, const char *);
void f_double (short, short, const char *, double);
void f_eol (short, short);
void f_msg (char *);
void f_sscansex (char *, short *, short *, short *);
void f_sscandate (char *, short *, short *, short *);
void f_dec_sexsign (double, short *, short *, short *);
short decimal_year (char *);

/* print angle ra, in radians, in ra hours as hh:mm.m at [r,c]
 * N.B. we assume ra is >= 0.
 */
#define f_ra(r,c,ra)		f_sexad (r, c, 2, 1, 24, radhr(ra))
/* print time, t, as hh:mm:ss */
#define f_time(r,c,t)		f_sexag (r, c, 2, t)
/* print time, t, as +/-hh:mm:ss (don't show leading +) */
#define f_signtime(r,c,t)	f_sexag (r, c, 3, t)
/* print time, t, as hh:mm */
#define f_mtime(r,c,t)		f_sexad (r, c, 2, 0, 24, t)
/* print angle, a, in rads, as degress at [r,c] in form ddd:mm */
#define f_angle(r,c,a)		f_sexad (r, c, 3, 0, 360, raddeg(a))
/* print angle, a, in rads, as degress at [r,c] in form dddd:mm:ss */
#define f_gangle(r,c,a)		f_sexag (r, c, 4, raddeg(a))


#endif
