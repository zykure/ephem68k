/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	version.c
 * Desc:	Displays the splash screen at the start of the program.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include <tigcclib.h>
#include "info.h"
#include "io.h"
#include "screen.h"
#include "i18n.h"


/* Set up graphics defined at info.h */
const BITMAP logo = { 24, 112, LOGO };
const ICON icon = { COMMENT_BW_ICON };

/* Credits to display */
const char *credits[] = {
	LOC_SPLASH_VERSION,
	LOC_SPLASH_COPY,
	LOC_SPLASH_COMPILE,
	"",
	"Based on ephem v4.29, March 9, 2000",
	"(c) 1990-1992 by Elwood Charles Downey",
	"(Code cleanup by Jonathan Woithe, 2000)",
	"",
	"E-mail: zykure@web.de",
	"",
	"Web: http://sourceforge.net/projects/ephem68k"
};


/* Show it! */
void splash(void)
{
	int nr, r, l;

	ClrScr();
	DrawLine(TOP_LINE, A_NORMAL);
	DrawLine(STAT_LINE, A_NORMAL);

	DrawIcon(52, 5, &icon, A_NORMAL);
	BitmapPut(77, 1, &logo, &(SCR_RECT){{0, 0, 239, 27}}, A_NORMAL);

	FontSetSys(F_4x6);
	nr = sizeof(credits)/sizeof(credits[0]);
	r = 24;
	for(l = 0; l < nr; l++)
	    DrawStr(44, r+=8, credits[l], A_NORMAL);
}
