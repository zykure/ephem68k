/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	screen.h
 * Desc:	This file contains most of the information how to display each
 *			item on the screen.
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef _SCREEN_H
#define _SCREEN_H

/* General (root) coordinates */
#define	COL1	0
#define	COL2	23

#define	R_TOP	0
#define	R_MID	(R_TOP+7)


/* Main screen items */
#define C_LT	COL1		/* Local Time */
#define	R_LT	(R_TOP+0)
#define	C_LTV	(C_LT+12)
#define C_LD	COL1		/* Local Date */
#define	R_LD	(R_TOP+1)
#define	C_LDV	(C_LD+10)

#define C_UT	COL1		/* UTC Time */
#define	R_UT	(R_TOP+2)
#define	C_UTV	(C_UT+12)
#define C_UD	COL1		/* UTC Date */
#define	R_UD	(R_TOP+3)
#define	C_UDV	(C_UD+10)

#define	R_JD	(R_TOP+5)	/* Julian Date */
#define	C_JD	COL1
#define	C_JDV	(C_JD+10)

#define	R_GST	(R_TOP)		/* Greenwich Sid. Time */
#define	C_GST	COL2
#define	C_GSTV	(C_GST+9)

#define	R_LST	(R_TOP+1)	/* Local Sid. Time */
#define	C_LST	COL2
#define	C_LSTV	(C_LST+9)

#define	R_DAWN	(R_TOP+3)	/* Dawn */
#define	C_DAWN	COL2
#define	C_DAWNV	(C_DAWN+12)

#define	R_DUSK	(R_TOP+4)	/* Dusk */
#define	C_DUSK	COL2
#define	C_DUSKV	(C_DUSK+12)

#define	R_NITE	(R_TOP+5)	/* Night Length */
#define	C_NITE	COL2
#define	C_NITEV	(C_NITE+12)

#define	R_CAL	(R_MID)		/* Calendar */
#define	C_CAL   COL1

#define	R_LON	(R_MID+0)	/* Observer Longitude */
#define	C_LON	COL2
#define	C_LONV	(C_LON+7)

#define	R_LAT	(R_MID+1)	/* Observer Latitude */
#define	C_LAT	COL2
#define	C_LATV	(C_LAT+7)

#define	R_HEIGHT (R_MID+2)	/* Observer Height above NN */
#define	C_HEIGHT COL2
#define	C_HEIGHTV (C_HEIGHT+8)

#define	R_TEMP	(R_MID+4)	/* Current Temperature */
#define	C_TEMP	COL2
#define	C_TEMPV	(C_TEMP+8)

#define	R_PRES	(R_MID+5)	/* Current Atmospheric Pressure */
#define	C_PRES	COL2
#define	C_PRESV	(C_PRES+9)

#define	R_EPOCH	(R_MID+7)	/* Epoch */
#define	C_EPOCH	COL2
#define	C_EPOCHV (C_EPOCH+9)


/* Defines for 'watch' screens */
#define SKY_X	119
#define SKY_Y	60
#define SKY_R	53
#define HOR_X	119
#define HOR_Y	113
#define HOR_R	(2*SKY_R)


#define R_WTOP	0
#define C_WCOL1	0
#define C_WCOL2	30


/* Defines for 'settings' screen */
#define R_STOP	3
#define S_COLL	3
#define S_COLR	36
#define S_COL1	(S_COLL+2)
#define S_COL2	(S_COLL+22)


/* Defines for 'info' screen */
#define R_ITOP	6
#define I_COL1	5
#define I_COL2	(S_COL1+14)
#define I_COL3	(S_COL1+14)

/* Defines for 'help' screen */
#define R_HTOP	2
#define H_COL1	2
#define H_COL2	21

/* Planet rows */
#define	R_PLANTAB	(R_TOP)			/* Start of planet/objetcs table */
#define	R_SUN		(R_PLANTAB+2)
#define	R_MOON		(R_PLANTAB+3)
#define	R_MERCURY	(R_PLANTAB+4)
#define	R_VENUS		(R_PLANTAB+5)
#define	R_MARS		(R_PLANTAB+6)
#define	R_JUPITER	(R_PLANTAB+7)
#define	R_SATURN	(R_PLANTAB+8)
#define	R_URANUS	(R_PLANTAB+9)
#define	R_NEPTUNE	(R_PLANTAB+10)
#define	R_OBJ1		(R_PLANTAB+11)
#define	R_OBJ2		(R_PLANTAB+12)
#define	R_OBJ3		(R_PLANTAB+13)
#define	R_OBJ4		(R_PLANTAB+14)

#define	C_OBJ		COL1	/* Start of planet/object data */

/* Menu 1a (Data 1) items */
#define	C_RA		(C_OBJ+3)
#define	C_DEC		(C_OBJ+11)
#define	C_AZ		(C_OBJ+18)
#define	C_ALT		(C_OBJ+25)
#define	C_MAG		(C_OBJ+32)
#define	C_PHASE		(C_OBJ+37)

/* Menu 1b (Data 2) items */
#define	C_HLONG		(C_OBJ+3)
#define	C_HLAT		(C_OBJ+10)
#define	C_EDIST		(C_OBJ+17)
#define C_SDIST 	(C_OBJ+24)
#define	C_ELONG		(C_OBJ+31)
#define	C_SIZE		(C_OBJ+36)

/* Menu 2 (Rise/Set) items */
#define	C_RISETM	(C_OBJ+3)
#define	C_RISEAZ	(C_OBJ+9)
#define	C_TRANSTM	(C_OBJ+16)
#define	C_TRANSALT	(C_OBJ+21)
#define	C_SETTM		(C_OBJ+28)
#define	C_SETAZ		(C_OBJ+34)


#define Y_STATLINE	123
#define X_STAT1		0
#define X_STAT2		95
#define X_STAT3		135
#define X_STAT4		167
#define X_STAT5		207

/* Definitions for DrawLine() */
#define TOP_LINE	0, 27, 239, 27
#define STAT_LINE	0, 121, 239, 121
#define MAIN_LINEH	0, 51, 239, 51
#define MAIN_LINEV	128, 0, 128, 120
#define TAB_LINE  	0, 11, 239, 11
#define STAT_SPLIT1	93, 122, 93, 127
#define STAT_SPLIT2	133, 122, 133, 127
#define STAT_SPLIT3	165, 122, 165, 127
#define STAT_SPLIT4	205, 122, 205, 127

#endif
