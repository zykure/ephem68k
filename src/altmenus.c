/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	altmenus.c
 * Desc:	Show one of the other menus (Data or Rise/Set screens).
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include <tigcclib.h>
#include "astro.h"
#include "circum.h"
#include "ephem.h"
#include "formats.h"
#include "screen.h"
#include "i18n.h"


void alt1a_body(short, short, Now *, Obj *);
void alt1b_body(short, short, Now *, Obj *);
void alt2_body(short, short, Now *, Obj *, short);

const short bodyrow[NOBJ] = {
	R_MERCURY, R_VENUS, R_MARS, R_JUPITER, R_SATURN, R_URANUS,
	R_NEPTUNE, R_SUN, R_MOON, R_OBJ1, R_OBJ2, R_OBJ3, R_OBJ4
};

void alt_plnames(void)
{
	f_string(R_PLANTAB,	C_OBJ,	LOC_ALT_OBJECT);
	f_string(R_SUN,		C_OBJ,	LOC_ALT_SUN);
	f_string(R_MOON,	C_OBJ,	LOC_ALT_MOON);
	f_string(R_MERCURY,	C_OBJ,	LOC_ALT_MERCURY);
	f_string(R_VENUS,	C_OBJ,	LOC_ALT_VENUS);
	f_string(R_MARS,	C_OBJ,	LOC_ALT_MARS);
	f_string(R_JUPITER,	C_OBJ,	LOC_ALT_JUPITER);
	f_string(R_SATURN,	C_OBJ,	LOC_ALT_SATURN);
	f_string(R_URANUS,	C_OBJ,	LOC_ALT_URANUS);
	f_string(R_NEPTUNE,	C_OBJ,	LOC_ALT_NEPTUNE);
	f_string(R_OBJ1,	C_OBJ,	LOC_ALT_OBJ_A);
	f_string(R_OBJ2,	C_OBJ,	LOC_ALT_OBJ_B);
	f_string(R_OBJ3,	C_OBJ,	LOC_ALT_OBJ_C);
	f_string(R_OBJ4,	C_OBJ,	LOC_ALT_OBJ_D);
}

void alt_labels(short menu)
{
	alt_plnames();

	switch (menu) {
		case M_DATA1:
			f_string(R_PLANTAB,	C_RA,		LOC_ALT_RA);
			f_string(R_PLANTAB,	C_DEC,		LOC_ALT_DEC);
			f_string(R_PLANTAB,	C_AZ,		LOC_ALT_AZ);
			f_string(R_PLANTAB,	C_ALT,		LOC_ALT_ALT);
			f_string(R_PLANTAB,	C_MAG,		LOC_ALT_MAG);
			f_string(R_PLANTAB,	C_PHASE,	LOC_ALT_PHASE);
			break;

		case M_DATA2:
			f_string(R_PLANTAB,	C_HLONG,	LOC_ALT_HLON);
			f_string(R_PLANTAB,	C_HLAT,		LOC_ALT_HLAT);
			f_string(R_PLANTAB,	C_EDIST,	LOC_ALT_EDIST);
			f_string(R_PLANTAB,	C_SDIST,	LOC_ALT_SDIST);
			f_string(R_PLANTAB,	C_ELONG,	LOC_ALT_ELONG);
			f_string(R_PLANTAB,	C_SIZE,		LOC_ALT_SIZE);
			break;

		case M_RISET:
			f_string(R_PLANTAB,	C_RISETM+2,	LOC_ALT_RISE);
			f_string(R_PLANTAB,	C_TRANSTM+2,LOC_ALT_TRANSIT);
			f_string(R_PLANTAB,	C_SETTM+2,	LOC_ALT_SET);
			break;

		default:
			break;
	}

	DrawLine(TAB_LINE, A_NORMAL);
}

/* 'Wrapper function' */
void alt_body(short menu, short p, short force, Now *np, Obj *obj, short hzn)
{
	switch (menu) {
		case M_DATA1:
			alt1a_body(p, force, np, obj);
			break;

		case M_DATA2:
			alt1b_body(p, force, np, obj);
			break;

		case M_RISET:
			alt2_body(p, force, np, obj, hzn);
			break;

		default:
			break;
	}
}

/* print body info in first menu format */
void alt1a_body(short p, short force, Now *np, Obj *obj)
{
	Sky sky;
	short row = bodyrow[p];

	if (p>=OBJ1 && p<=OBJ4) {
		if (!obj)
			return;
		if (obj->o_type == OBJ_NONE)
			return;
	}

	if (body_cir(p, AS, np, obj, &sky) || force) {
	    f_ra(row, C_RA, sky.s_ra);
    	f_angle(row, C_DEC, sky.s_dec);
    	f_double(row, C_MAG, sky.s_mag <= -9.95 ? "%4.0f" : "%#4.1f",
				sky.s_mag);
   		if (sky.s_sdist > 0.0)
				f_int(row, C_PHASE, "%3d", sky.s_phase);
	}
	f_angle(row, C_AZ, sky.s_az);
	f_angle(row, C_ALT, sky.s_alt);
}

void alt1b_body(short p, short force, Now *np, Obj *obj)
{
	Sky sky;
	short row = bodyrow[p];

	if (p>=OBJ1 && p<=OBJ4) {
		if (!obj)
			return;
		if (obj->o_type == OBJ_NONE)
			return;
	}

	if (body_cir(p, AS, np, obj, &sky) || force) {
		if (sky.s_hlong != NOHELIO) {
			f_angle(row, C_HLONG, sky.s_hlong);
		if (p != SUN)
		    f_angle(row, C_HLAT, sky.s_hlat);
	    }
		if (p == MOON) {
	 		f_double(R_MOON, C_EDIST, "%6.0f", sky.s_edist);
	  } else if (sky.s_edist > 0.0) {
			/* show distance in au */
			f_double(row, C_EDIST,(sky.s_edist>=10.0)?"%-6.3f":"%-6.4f",
					sky.s_edist);
	  }
	  if (sky.s_sdist > 0.0)
			f_double(row, C_SDIST, (sky.s_sdist>=9.99995)?"%-6.3f":"%-6.4f",
					sky.s_sdist);
	  if (p != SUN)
			f_double(row, C_ELONG, "%4.0f", sky.s_elong);
	  f_double(row, C_SIZE, "%#4.1f",
	    		(sky.s_size>=59.95)?sky.s_size/60.0:sky.s_size);
	}
}

void alt2_body(short p, short force, Now *np, Obj *obj, short hzn)
{
	double ltr, lts, ltt, azr, azs, altt;
	short row = bodyrow[p];
	short status;

	if (p>=OBJ1 && p<=OBJ4) {
		if (!obj)
			return;
		if (obj->o_type == OBJ_NONE)
			return;
	}

	/* always recalc user objects since we don't know it's the same object */
	if (!riset_cir(p, np, obj, (p>=OBJ1 && p<=OBJ4), hzn,
		&ltr, &lts, &ltt, &azr, &azs, &altt, &status) && !force)
	    return;

	f_blanks(row, C_OBJ+3, 37);

	if (status & RS_ERROR) {
	    /* can not find where body is! */
	    f_string(row, C_RISETM, LOC_ALT_ERROR);
	    return;
	}
	if (status & RS_CIRCUMPOLAR) {
	    /* body is up all day */
	    f_string(row, C_RISETM, LOC_ALT_CIRCUM);
	    if (status & RS_NOTRANS) {
			f_string(row, C_TRANSTM, LOC_ALT_NOTRANS);
	    } else {
			f_mtime(row, C_TRANSTM, ltt);
			if (status & RS_2TRANS)
		    	f_char(row, C_TRANSTM+5, '+');
			f_angle(row, C_TRANSALT, altt);
	    }
	    return;
	}
	if (status & RS_NEVERUP) {
	    /* body never up at all today */
	    f_string(row, C_RISETM, LOC_ALT_NEVERUP);
	    return;
	}

	if (status & RS_NORISE) {
	    /* object does not rise as such today */
	    f_string(row, C_RISETM, LOC_ALT_NORISE);
	} else {
	    f_mtime(row, C_RISETM, ltr);
	    if (status & RS_2RISES) {
			/* object rises more than once today */
			f_char(row, C_RISETM+5, '+');
	    }
	    f_angle(row, C_RISEAZ, azr);
	}

	if (status & RS_NOTRANS) {
	    f_string(row, C_TRANSTM, LOC_ALT_NOTRANS);
	} else {
	    f_mtime(row, C_TRANSTM, ltt);
	    if (status & RS_2TRANS)
			f_char(row, C_TRANSTM+5, '+');
	    f_angle(row, C_TRANSALT, altt);
	}

	if (status & RS_NOSET) {
	    /* object does not set as such today */
	    f_string(row, C_SETTM, LOC_ALT_NOSET);
	} else {
	    f_mtime(row, C_SETTM, lts);
	    if (status & RS_2SETS)
			f_char(row, C_SETTM+5, '+');
	    f_angle(row, C_SETAZ, azs);
	}
}