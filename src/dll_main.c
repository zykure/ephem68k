/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	dll_main.c
 * Desc:	Core of the ephem68k DLL (zephem).
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include <tigcclib.h>
#include "circum.h"
#include "ephem.h"

/* Change these values inside ephem.h */
#define LIB_IDENT			DLL_1_IDENT
#define LIB_ID				DLL_1_ID
#define LIB_VERSION			DLL_1_VERSION


/* Required definitions*/
DLL_INTERFACE

void dll_getinfo(DLL_INFO *);

DLL_ID 			LIB_ID
DLL_VERSION LIB_VERSION
/* The first 12 exports have to stay in this order (dll_getinfo, ..., precess)!
 * These functions are implemented in both DLLs, so they must have the same
 * indizes to run properly.
 * UPDATE: ephem68k uses only one DLL now, so this note is obsolete.
 */
DLL_EXPORTS /*  0 */	dll_getinfo,
						/*  1 */	anomaly,
						/*  2 */	cal_mjd, mjd_cal, year_mjd, mjd_dow, mjd_dpm, mjd_year,
									mjd_day, mjd_hr,
						/* 10 */	comet,
						/* 11 */	ecleq_aux,
						/* 12 */	moon,
						/* 13 */	moonnf,
						/* 14 */	nutation,
						/* 15 */	ta_par,
						/* 16 */	plans,
						/* 17 */	precess,
						/* 18 */	obj_cir,
						/* 19 */	obliquity,
						/* 20 */	reduce_elements,
						/* 21 */	refract, unrefract,
						/* 23 */	sunpos

DLL_IMPLEMENTATION

/* Common function to get info about the DLL */
void dll_getinfo(DLL_INFO *i)
{
	i->id = LIB_ID;
	sprintf(i->ver, "%d.%d", LIB_VERSION);
	strncpy(i->name, LIB_IDENT, 16);
	strncpy(i->date, __DATE__, 16);
	strncpy(i->cver, __VERSION__, 16);
	return;
}
