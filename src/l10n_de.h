/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	l10n_de.h
 * Desc:	Part of the ephem68k i18n-package -- german locals
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef _L10N_DE_H
#define _L10N_DE_H

/* This prevents using more than one l10n */
#define _LOCAL_


/* Settings editor help - var. width */
#define LOC_SETTINGS_HELP	\
				"Setze Julianisches Datum (MJD.nn)", \
				"Setze lokale Uhrzeit (HH:MM:SS)", \
				"Setze locales Datum (YYYY.MM.DD oder YYYY.nn)", \
				"Setze Zeitzone (Stunden vor UT)", \
				"Setze geogr. Laenge (Grad Ost)", \
				"Setze geogr. Breite (Grad Nord))", \
				"Setze Hoehe (Meter ueber NN)", \
				"Setze aktuelle Temperatur (" "\xB0" "C)", \
				"Setze aktuellen Luftdruck (hPa)", \
				"Setze Zielepoche ('EOD' fuer aktuelle Epoche)", \
				"Andere Optionen (Umschalten mit \x18)"

/* Flags editor help - var. width */
#define LOC_FLAGS_HELP		\
				"Daemmerungszeiten berechnen", \
				"Adaptiver Horizont (Refraktion)", \
				"Buergerliche statt astronomischer Daemmerung"

/* Flag names */
#define LOC_FLAGS			'e','v','m','j','s','n','u','S','M','A','B','C','D','D','H','B'

/* DLL error messages - max. 20 chars */
#define LOC_DLL_ERRORS		\
				"Intern",				/* #1 NOTINGHOSTSPACE */	\
				"Datei nicht gefunden",	/* #2 NOTFOUND*/			\
				"Intern",				/* #3 LOCKFAILED */ 		\
				"Nicht genug Speicher",	/* #4 OUTOFMEM */			\
				"Intern",				/* #5 ALREADYLOADED */		\
				"Falsche Version"		/* #6 WRONGVERSION	*/

/* Calendar month names - max. 8 chars */
#define LOC_MONTH_NAMES		\
			    "Januar", "Februar", "Maerz", "April", "Mai", "Juni", \
	    		"Juli", "August", "September", "Oktober", "November", "Dezember"

/* Object names - max. 12 chars */
#define LOC_OBJECT_NAMES 	\
				"Merkur", "Venus", "Mars", "Jupiter", "Saturn", "Uranus", \
				"Neptun", "Sonne", "Mond", "Objekt A", "Objekt B", "Objekt C", \
				"Objekt D"

/* Calendar week days - fixed width */
#define LOC_DAY_ABBRV 		"Mo Di Mi Do Fr Sa So"

/* Cardinal points for watch screens - 1 char */
#define LOC_CARDINALS		'N','O','S','W'
#define LOC_ZENITH			'Z'

/* 'Any key' message - var. width */
#define LOC_ANYKEY			"Taste druecken um fortzufahren..."

/* Dialog titles - var. width */
#define LOC_ERR				"FEHLER"
#define LOC_CONFIRM			"BESTAETIGUNG"
#define LOC_NEW_SETTINGS	"NEUE EINSTELLUNGEN"

/* Dialog title - fixed width */
#define LOC_SETTINGS		"  EINSTELLUNGEN   "
#define LOC_HELP			"Hilfe"

/* Error messages and confirmations - var. width */
#define LOC_ERR_CONFIG		"Konnte Konfiguration nicht lesen!"
#define LOC_ERR_CFGVERSION	"Konfigdatei hast falsche Version!\nBenutze Standardwerte."
#define LOC_ERR_CFGSAVE		"Konnte Einstellungen nicht speichern!"
#define LOC_ERR_DB			"Konnte Datenbank nicht oeffnen!"
#define LOC_ERR_DBREAD		"Fehler in Datenbank!"
#define LOC_ERR_OBJECT		"Objekt nicht gefunden!"
#define LOC_ERR_DLL			"Konnte DLL nicht laden!\nStatus #%2.2d (%s)"
#define LOC_QUIT			"Wirklich beenden?"
#define LOC_SETTINGS_SAVE	"Einstellungen uebernehmen?"

/* Status info text - var. width */
#define LOC_KEY_STOP		"[ESC zum Abbruch]"
#define LOC_KEY_HELP		"[Hilfe mit F8]"

/* Status messages - var. width */
#define LOC_UPDATE_CORE		"Aktualisiere Kernfunktionen..."
#define LOC_UPDATE_MAIN		"Aktualisiere Uebersicht..."
#define LOC_UPDATE_TWIL		"Aktualisiere Daemmerungszeiten..."
#define LOC_UPDATE_OBJ		"Aktualisiere %s..."	/* Don't forget the '%s'! */

/* Main screen title - var. width */
#define LOC_MAIN			"Uebersicht"

/* EpochOfDate identifier - 8 chars */
#define LOC_EOD				" (jetzt)"

/* Settings editor items - max. 13 chars */
#define LOC_SET_JD			"Jul. Datum:"
#define LOC_SET_TIME		"Lok. Uhrzeit:"
#define LOC_SET_DATE		"Lok. Datum:"
#define LOC_SET_TZ			"Zeitzone:"
#define LOC_SET_LONG		"Laenge:"
#define LOC_SET_LAT			"Breite:"
#define LOC_SET_HEIGHT		"Hoehe:"
#define LOC_SET_TEMP		"Temperatur:"
#define LOC_SET_PRESS		"Luftdruck:"
#define LOC_SET_EPOCH		"Epoche:"
#define LOC_SET_FLAGS		"Schalter:"

/* Flags editor help - var. width */
#define LOC_SET_OBJ_HELP	"Berechne %s" 		/* Don't forget the '%s'! */

/* NoObject identifier - max. 8 chars */
#define LOC_EDIT_NONE		"(KEIN)"

/* Object editor help */
#define LOC_EDIT_KEY		"ENTER druecken um %s zu aendern"	/* Don't forget the '%s'! */
#define LOC_EDIT_HELP		"Neuer Namen f�r %s (frei lassen zum Loeschen)"	/* Don't forget the '%s'! */

/* Object types for info screen -  max. 15 chars*/
#define LOC_OBJ_STAR		"Stern"
#define LOC_OBJ_DBLSTAR		"Doppelstern"
#define LOC_OBJ_VARSTAR		"Variabler Stern"
#define LOC_OBJ_GALAXY		"Galaxie"
#define LOC_OBJ_CLUSTER		"Sternhaufen"
#define LOC_OBJ_OPEN		"Offener Haufen"
#define LOC_OBJ_PLNEB		"Plan. Nebel"
#define LOC_OBJ_NEB			"Refl. Nebel"
#define LOC_OBJ_EMNEB		"Emiss. Nebel"
#define LOC_OBJ_REST		"Supernova-Rest"
#define LOC_OBJ_MILKYWAY	"Milchstrasse"
#define LOC_OBJ_ASTERISM	"Asterismus"
#define LOC_OBJ_ASTEROID	"Asteroid"
#define LOC_OBJ_PLANETOID	"Planetoid"
#define LOC_OBJ_COMET		"Komet"

/* Info screen items - max. 12 chars */
#define LOC_INFO_DIAM		"Durchmesser:"
#define LOC_INFO_DAYLEN		"Tageslaenge:"
#define LOC_INFO_PERIOD		"Orb. Periode:"
#define LOC_INFO_MAG		"Helligkeit:"
#define LOC_INFO_DIST		"Entfernung:"
#define LOC_INFO_CLASS		"Klasse:"
#define LOC_INFO_HYPERB		"Hyperbolisch"
#define LOC_INFO_PARAB		"Parabolisch"

/* Help screen items - max. 17 chars */
#define LOC_HELP_MAIN		"F1   Uebersicht"
#define LOC_HELP_DATA1		"F2   Daten (1)"
#define LOC_HELP_DATA2		"F3   Daten (2)"
#define LOC_HELP_RISET		"F4   Sichtbark."
#define LOC_HELP_SKYDOME	"F5   Himmel"
#define LOC_HELP_HORIZON	"F6   Horizont"
#define LOC_HELP_INFO		"F7   Objekt-Info"
#define LOC_HELP_HELP		"F8   Hilfe"
#define LOC_HELP_TOGGLE		"ENT  Umschalten"
#define LOC_HELP_UPDATE		"SPC  Neu rechnen"
#define LOC_HELP_MAPINFO	"MODE Koord.-Netz"
#define LOC_HELP_SELITEM	"\x13/\x14  Auswaehlen"
#define LOC_HELP_ROTATE		"\x11/\x12  Rotieren"
#define LOC_HELP_TIME		"+/-  Zeit aendern"
#define LOC_HELP_EDIT		"CLR  Obj. aendern"
#define LOC_HELP_SETTINGS	"APPS Einstellung"
#define LOC_HELP_QUIT		"ESC  Beenden"
#define LOC_HELP_SLEEP		"OFF  Schlafmodus"

/* Splash screen first rows - var. width */
#define LOC_SPLASH_VERSION	"ephem68k Version " COMMENT_VERSION_STRING " [deutsch]"
#define LOC_SPLASH_COPY		"Copyright " COMMENT_STRING
#define LOC_SPLASH_COMPILE	"Erstellt am " __DATE__ " mit " __TIGCC_VERSION_STRING__

/* Main menu - 8 chars max */
#define LOC_MM_DATE			"Lok Datum"
#define LOC_MM_TIME			"Lok Zeit"
#define LOC_MM_UDATE		"UTC Datum"
#define LOC_MM_UTIME		"UTC Zeit"
#define LOC_MM_JD			"JD"
#define LOC_MM_GST			"GST"
#define LOC_MM_LST			"LST"
#define LOC_MM_DAWN			"Morgen"
#define LOC_MM_DUSK			"Abend"
#define LOC_MM_NITELN		"Nacht"
#define LOC_MM_LON			"Laenge"
#define LOC_MM_LAT			"Breite"
#define LOC_MM_HEIGHT		"Hoehe"
#define LOC_MM_TEMP			"Temp"
#define LOC_MM_PRESS		"Druck"
#define LOC_MM_EPOCH		"Epoche"

/* Calendar moon dates - 2 chars */
#define LOC_CAL_NM			"NM"
#define LOC_CAL_FM			"VM"

/* Alt menu rows - 3 chars max */
#define LOC_ALT_OBJECT		"Obj"
#define LOC_ALT_SUN			"So"
#define LOC_ALT_MOON		"Mo"
#define LOC_ALT_MERCURY		"Me"
#define LOC_ALT_VENUS		"Ve"
#define LOC_ALT_MARS		"Ma"
#define LOC_ALT_JUPITER		"Ju"
#define LOC_ALT_SATURN		"Sa"
#define LOC_ALT_URANUS		"Ur"
#define LOC_ALT_NEPTUNE		"Ne"
#define LOC_ALT_OBJ_A		" A"
#define LOC_ALT_OBJ_B		" B"
#define LOC_ALT_OBJ_C		" C"
#define LOC_ALT_OBJ_D		" D"

/* Alt menu cols - var length */
#define LOC_ALT_RA			" R.A."
#define LOC_ALT_DEC			" Dek"
#define LOC_ALT_AZ			" Az"
#define LOC_ALT_ALT			" Alt"
#define LOC_ALT_MAG			"VMag"
#define LOC_ALT_PHASE		"Phs"
#define LOC_ALT_HLON		" H-Lng"
#define LOC_ALT_HLAT		" H-Brt"
#define LOC_ALT_EDIST		"E-Entf"
#define LOC_ALT_SDIST		"S-Entf"
#define LOC_ALT_ELONG		"Elong"
#define LOC_ALT_SIZE		" Gr"
#define LOC_ALT_RISE		"Aufgang"
#define LOC_ALT_TRANSIT		"Transit"
#define LOC_ALT_SET			"Untergang"

/* Alt menu status info - max. 12 chars */
#define LOC_ALT_ERROR		"?Fehler?"
#define LOC_ALT_CIRCUM		"Zirkumpolar"
#define LOC_ALT_NOTRANS		"Kein Transit"
#define LOC_ALT_NEVERUP		"Nie sichtbar"
#define LOC_ALT_NORISE		"Kein Aufg."
#define LOC_ALT_NOSET		"Kein Unterg."

#endif

