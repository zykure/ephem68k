/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	watch.c
 * Desc:	Start sky watching mode (zenith or horizon view).
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include <tigcclib.h>
#include "astro.h"
#include "circum.h"
#include "ephem.h"
#include "formats.h"
#include "screen.h"
#include "i18n.h"


/* Sprites for drawing objects and cursor */
const unsigned char sprite_sun[] = {
	0b00100000,
	0b01010000,
	0b10001000,
	0b01010000,
	0b00100000
};
const unsigned char sprite_moon[] = {
	0b11100000,
	0b11100000,
	0b11100000,
};
const unsigned char sprite_planet[] = {
	0b01000000,
	0b10100000,
	0b01000000,
};
static const unsigned char sprite_galaxy[] = {
	0b01100000,
	0b10100000,
	0b11000000,
};
static const unsigned char sprite_nebula[] = {
	0b01100000,
	0b11100000,
	0b11000000,
};
static const unsigned char sprite_cluster[] = {
	0b10100000,
	0b01000000,
	0b10100000,
};
static const unsigned char sprite_solar[] = {
	0b01000000,
	0b11100000,
	0b01000000,
};
static const unsigned char sprite_sel[] = {
	0b11000110,
	0b10000010,
	0b00000000,
	0b00000000,
	0b00000000,
	0b10000010,
	0b11000110,
};

#define COORD_BIAS 10000.0
/* grid coordinates */
const unsigned short coord_skydome[][2] = {
/* Format:	{ x, y } with
 * 					x = cos(alt)*sin(az)/(1 + sin(alt))
 * 					y = cos(alt)*cos(az)/(1 + sin(alt))
 * Also see file grid_coord.odt (OpenOffice document)
 */ 
	{     0, 10000 }, // alt = 0�, az = 0�
	{     0,  5774 }, // alt = 30�
	{     0,  2679 }, // alt = 60�

	{  5000,  8660 }, // az = 30�
	{  2887,  5000 },
	{  1340,  2321 },
};
const unsigned short coord_horizon[][2] = {
/* Format:	{ x, y } with
 * 					x = cos(alt)*sin(az)/(1 + cos(alt)*cos(az))
 * 					y = sin(alt)/(1 + cos(alt)*cos(az))
 * Also see file grid_coord.odt (OpenOffice document)
 */ 
	{     0,     0}, // alt = 0�, az = 0�
	{	    0,  1317}, // alt = 15�
	{	    0,  2679}, // alt = 30�
	{	    0,  4142}, // alt = 45�
	{	    0,  5774}, // alt = 60�
	{	    0,  7673}, // alt = 75�

	{	 2679,     0}, // az = 30�
	{	 2630,  1409},
	{	 2474,  2857},
	{	 2193,  4386},
	{	 1745,  6043},
	{	 1057,  7891},

	{	 5574,     0}, // az = 60�
	{	 5641,  1745},
	{	 5234,  3489},
	{	 4524,  5224},
	{	 3464,  6928},
	{	 1985,  8552},

	{	10000,     0}, // az = 90�
	{	 9659,  2588},
	{	 8660,  5000},
	{	 7071,  7071},
	{	 5000,  8660},
	{	 2588,  9659}
};

static Watch wbodies[NOBJ] = { WATCH_NULL, WATCH_NULL, WATCH_NULL, WATCH_NULL };

#define LBL(i)	lbl[((i)%4)]

/* Draw labels, interface etc. */
void watch_labels(short mode, short rot, short show_grid)
{
	static const char lbl[4] = { LOC_CARDINALS };
	short k = (rot/90) % 4;
	short i, x, y;

	if (mode == M_SKYDOME) {
	    /* Draw coordinate grid */
 	  	for (i = 0; i < 7; i++) {
   			  x = SKY_R*((double)coord_skydome[i][0]/COORD_BIAS);
   			  y = SKY_R*((double)coord_skydome[i][1]/COORD_BIAS);
   			  if (!show_grid)
   			  	if ((i != 0) && (i != 3))
   			  		continue;
    		  DrawPix(SKY_X + x, SKY_Y + y, A_NORMAL);
    	  	DrawPix(SKY_X - x, SKY_Y + y, A_NORMAL);
	    	  DrawPix(SKY_X + x, SKY_Y - y, A_NORMAL);
	    	  DrawPix(SKY_X - x, SKY_Y - y, A_NORMAL);
  	  	  DrawPix(SKY_X + y, SKY_Y + x, A_NORMAL);
    		  DrawPix(SKY_X - y, SKY_Y + x, A_NORMAL);
    		  DrawPix(SKY_X + y, SKY_Y - x, A_NORMAL);
    	  	DrawPix(SKY_X - y, SKY_Y - x, A_NORMAL);
	    }
	    /* Draw zenith point */
  		if (show_grid)
  			DrawPix(SKY_X, SKY_Y, A_NORMAL);

		/* Print cardinals */
  		FontSetSys(F_4x6);
	    DrawChar(SKY_X-2-(rot==270?1:0), 	SKY_Y-SKY_R-6, 				LBL(k),		A_NORMAL);
	    DrawChar(SKY_X-SKY_R-5-(rot==180?2:0)-(rot==270?1:0),SKY_Y-2, 	LBL(k+1),	A_NORMAL);
		DrawChar(SKY_X-2-(rot==90?1:0), 	SKY_Y+SKY_R+2, 				LBL(k+2), 	A_NORMAL);
	    DrawChar(SKY_X+SKY_R+1,				SKY_Y-2, 					LBL(k+3), 	A_NORMAL);
  		FontSetSys(F_6x8);
	} else {
	    /* Draw coordinate grid */
 	  	for (i = 0; i < 24; i++) {
   			  x = HOR_R*((double)coord_horizon[i][0]/COORD_BIAS);
   			  y = HOR_R*((double)coord_horizon[i][1]/COORD_BIAS);
   			  if (!show_grid)
   			  	if ((i != 0) && (i != 6) && (i != 12) && (i < 18))
   			  		continue;
   			  if (i >= 3)
   	    	  DrawPix(HOR_X - x, HOR_Y - y, A_NORMAL);
	    	  DrawPix(HOR_X + x, HOR_Y - y, A_NORMAL);
  	   }
    	/* Draw zenith point */
  		DrawPix(HOR_X, HOR_Y-HOR_R, A_NORMAL);

			/* Draw cardinals */
  		FontSetSys(F_4x6);
    	DrawChar(HOR_X-2-(rot==90?1:0), 		HOR_Y+2,	LBL(k+2),	A_NORMAL);
    	DrawChar(HOR_X-HOR_R-2-(rot==180?1:0), 	HOR_Y+2,	LBL(k+1),	A_NORMAL);
    	DrawChar(HOR_X+HOR_R-2-(rot==0?1:0), 	HOR_Y+2,	LBL(k+3),	A_NORMAL);
	    DrawChar(HOR_X-2, 						HOR_Y-HOR_R-6, LOC_ZENITH,	A_NORMAL);
  		FontSetSys(F_6x8);
	}
}

/* Compute position and draw object */
void watch_body(const short mode, const short p, const short rot, 
		const Now *np, const Obj *op)
{
	Sky s;
	Watch *w = &wbodies[p];

	/* Make sure we use up-to-date values */
  body_cir(p, SKYACC, (Now *) np, (Obj *) op, &s);
	w->w_az = s.s_az;
	w->w_alt = s.s_alt;
	w->w_ra = s.s_ra;
	w->w_dec = s.s_dec;
	w->w_x = -1;
	w->w_y = -1;

  draw_wbody(w, p, mode, rot, op);

}

/* Print labels */
void show_wbody(short p, unsigned short flags)
{
	Watch *w = &wbodies[p];

	f_string(R_WTOP,C_WCOL1+3,	"       ");
	f_string(R_WTOP,C_WCOL2+3,	"       ");
	f_string(R_WTOP+1,C_WCOL1,	"Az ");
	f_string(R_WTOP+1,C_WCOL2,	"Alt");
	f_string(R_WTOP,  C_WCOL1,	"RA ");
	f_string(R_WTOP,  C_WCOL2,	"Dec");

	if (flags & (1<<p)) {
		f_angle(R_WTOP+1, C_WCOL1+4,	w->w_az);
		f_angle(R_WTOP+1, C_WCOL2+4,	w->w_alt);
		f_ra(R_WTOP, 	    C_WCOL1+3,	w->w_ra);
		f_angle(R_WTOP,   C_WCOL2+4,	w->w_dec);
	} else {
		f_string(R_WTOP+1, C_WCOL1+4,	"      ");
		f_string(R_WTOP+1, C_WCOL2+5,	"      ");
		f_string(R_WTOP,   C_WCOL1+3,	"      ");
		f_string(R_WTOP,   C_WCOL2+4,	"      ");
	}
}

/* Draw cursor */
void sel_wbody(short p)
{
	Watch *w = &wbodies[p];

	if (w->w_x > -1 && w->w_y > -1)
		Sprite8(w->w_x-3, w->w_y-3, 7,
		  	(unsigned char *) sprite_sel, LCD_MEM, SPRT_XOR);
}

void draw_wbody(Watch *w, const short p, const short mode, const short rot, const Obj *op) {
  double raz, taz, saz, caz, salt, calt, tx, ty, tz;
  
  if ((w->w_alt >= 0.0) && (w->w_alt <= 90.0)) {
  	/* Compute x/y position */
    taz = degrad(mode == M_SKYDOME ? 0.0 : 180.0) - w->w_az + degrad((double)rot);
    range(taz, TWOPI);
    sincos(taz, 0, &saz, &caz);
    sincos(w->w_alt, 0, &salt, &calt);
    tx = calt * saz;
    ty = salt;
    tz = calt * caz;
    if (mode == M_SKYDOME) {
	    w->w_x = (short)((double)SKY_X + (double)SKY_R*(tx/(1+ty)));
	    w->w_y = (short)((double)SKY_Y - (double)SKY_R*(tz/(1+ty)));
  	} else {
  		raz = (double)rot - raddeg(w->w_az);
  		range(raz, 360.0);
  		if (raz >= 90.0 && raz <= 270.0) {
  	    w->w_x = (short)((double)HOR_X - (double)HOR_R*(tx/(1+tz)));
		    w->w_y = (short)((double)HOR_Y - (double)HOR_R*(ty/(1+tz)));
		  }
		}

		/* Draw object sprites */
		if (p == SUN)
			Sprite8(w->w_x-2, w->w_y-2, 5,
			  	(unsigned char *) sprite_sun, LCD_MEM, SPRT_OR);
		else if (p == MOON)
			Sprite8(w->w_x-1, w->w_y-1, 3,
			  	(unsigned char *) sprite_moon, LCD_MEM, SPRT_OR);
		else if (p >= MERCURY && p <= NEPTUNE)
			Sprite8(w->w_x-1, w->w_y-1, 3,
			  	(unsigned char *) sprite_planet, LCD_MEM, SPRT_OR);
		else {
		  /* user defined objects */
		  if (op->o_type == FIXED) {
		  	switch (op->o_class) {
		  		case 'S':		// stars
		  		case 'D':
		  		case 'V':
						DrawPix(w->w_x, w->w_y, A_NORMAL);
						break;
					case 'G':		// galaxies
		 				Sprite8(w->w_x-1, w->w_y-1, 3,
				  			(unsigned char *) sprite_galaxy, LCD_MEM, SPRT_OR);
						break;
				  case 'R':		// nebulae etc.
				  case 'P':
				  case 'E':
				  case 'N':
		 				Sprite8(w->w_x-1, w->w_y-1, 3,
					  		(unsigned char *) sprite_nebula, LCD_MEM, SPRT_OR);
						break;
					case 'C':  // clusters and other objects
					case 'O':
					case 'M':
					case 'A':
		 				Sprite8(w->w_x-1, w->w_y-1, 3,
					  		(unsigned char *) sprite_cluster, LCD_MEM, SPRT_OR);
						break;
				}
			} else {	// solar system objects like comets
				Sprite8(w->w_x-1, w->w_y-1, 3,
				 		(unsigned char *) sprite_solar, LCD_MEM, SPRT_OR);
			}
		}
	}
}
