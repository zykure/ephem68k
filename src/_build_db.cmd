@rem This batch file is used to convert the object database
@rem (ephem.db) from plain text to TI file format (ephemdb.9xz)

call ttbin2oth -92 db ephem.db ephemdb
pause