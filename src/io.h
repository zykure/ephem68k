/*
 * ephem68k - interactive astronomical program for Texas Instruments
 * TI-92/TI-92+/Voyage200 calculators / CAS systems.
 * Official homepage: http://www.sourceforge.net/projects/ephem68k
 *
 * File:	io.h
 * Desc:	Some device-dependent macros to read/write data
 *
 * Copyright (c) 2005-2008 Jan D. Behrens, <zykure@web.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef _IO_H
#define _IO_H

#include <tigcclib.h>

/* Defines for F_6x8 font */
#define CW	6	/* Character width (in pixels) */
#define CH	8	/* Character height (in pixels) */
#define	NR	16	/* number of rows */
#define	NC	40	/* number of columns */


/* Defnes for input/output (instead of functions!) */
#define wait()			while (!kbhit ()) idle ()
#define read_char()		ngetchx ()
#define c_pos(r,c)		MoveTo (CW*(c)-1, CH*(r))
#define c_erase()		ClrScr (); DrawLine (STAT_LINE, A_NORMAL)
#define chk_char()		(kbhit () ? read_char () : 0)
#define ST_clear()		ScrRectFill(&(SCR_RECT){{0, 122, 239, 127}}, \
						&(SCR_RECT){{0, 122, 239, 127}}, A_REVERSE);


/* Add-on to compat.h */
#define KEY_2ND		4096

#endif
